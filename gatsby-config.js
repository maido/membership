/**
 * @prettier
 */
let env = process.env.NODE_ENV;

if (!env) {
    env = 'development';
}

require('dotenv').config({path: `.env.${env}`});

const plugins = [
    'gatsby-plugin-react-helmet',
    {
        resolve: `gatsby-source-filesystem`,
        options: {
            name: `images`,
            path: `${__dirname}/src/images`
        }
    },
    'gatsby-transformer-sharp',
    'gatsby-plugin-sharp',
    {
        resolve: `gatsby-plugin-manifest`,
        options: {
            name: 'gatsby-starter-default',
            short_name: 'starter',
            start_url: '/',
            background_color: '#000',
            theme_color: '#000',
            display: 'minimal-ui',
            icon: 'src/images/app-icon.png'
        }
    },
    {
        resolve: 'gatsby-plugin-emotion'
    },
    {
        resolve: `gatsby-source-contentful`,
        options: {
            spaceId: process.env.CONTENTFUL_SPACE_ID,
            accessToken: process.env.CONTENTFUL_DELIVERY_ACCESS_TOKEN
        }
    },
    'gatsby-plugin-flow',
    {
        resolve: 'gatsby-plugin-robots-txt',
        options: {
            policy: [{userAgent: '*', disallow: ['/']}],
            sitemap: null,
            host: null
        }
    },
    {
        resolve: `gatsby-plugin-gtag`,
        options: {
            trackingId: process.env.GOOGLE_TAG_MANAGER_ID,
            head: false,
            anonymize: true
        }
    }
];

if (env !== 'development') {
    plugins.push('gatsby-plugin-layout');
}

module.exports = {
    siteMetadata: {
        title: 'British Fashion Council – Membership',
        description: '',
        author: '',
        navigation: {
            main: [
                {to: '/', label: 'Home'},
                {to: '/news/', label: 'News'},
                {to: '/opportunities-and-support/', label: 'Opportunities & Support'},
                {to: '/reports-and-insights/', label: 'Reports & Insights'},
                {to: '/designer-fact-files/', label: 'Designer Fact File'},
                {to: '/events/', label: 'Calendar'},
                {to: '/member-network/', label: 'Member & Patron Network'},
                {
                    to: 'https://www.britishfashioncouncil.co.uk/membership_view.aspx',
                    label: 'My Profile'
                }
            ]
        }
    },
    plugins
};

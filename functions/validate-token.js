const fetch = require('node-fetch');

exports.handler = async event => {
    if (event.queryStringParameters.token) {
        return fetch(
            `https://www.britishfashioncouncil.co.uk/webservices/membership.aspx?token=${
                event.queryStringParameters.token
            }`
        )
            .then(response => response.text())
            .then(response => {
                return {
                    statusCode: 200,
                    body: response
                };
            })
            .catch(error => ({statusCode: 422, body: String(error)}));
    } else {
        return {
            statusCode: 200,
            body: 'token missing'
        };
    }
};

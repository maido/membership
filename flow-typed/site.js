type ContentfulBusinessStage = {
    title?: string
};

type ContentfulPerson = {
    firstName?: string,
    lastName?: string,
    isInMemberNetwork?: *,
    company?: string,
    city?: string,
    websiteUrl?: string,
    facebookUrl?: string,
    twitterUrl?: string,
    instagramUrl?: string,
    youTubeUrl?: string,
    image?: string,
    tagline?: string,
    content?: string,
    contacts?: Array<Object>
};

type ContentfulMetaContent = {
    title?: string,
    description?: string,
    image?: string,
    twitterTitle?: string,
    twitterDescription?: string,
    twitterImage?: string
};

type ContentfulAddress = {
    name?: string,
    street?: string,
    city?: string,
    country?: string,
    postcode?: string,
    websiteUrl?: string,
    telephone?: string,
    email?: string
};

type ContentfulCompany = {
    title?: string,
    slug?: string,
    companyType?: string,
    isInMemberNetwork?: *,
    websiteUrl?: string,
    facebookUrl?: string,
    instagramUrl?: string,
    twitterUrl?: string,
    youTubeUrl?: string,
    image?: string,
    tagline?: string,
    content?: string,
    leadContactFirstName?: string,
    leadContactLastName?: string,
    leadContactRole?: string,
    leadContactImage?: string,
    contacts?: Array<Object>,
    address?: string
};

type ContentfulAsset = {
    title?: string,
    image?: Array<Object>
};

type ContentfulContactDetails = {
    title?: string,
    firstName?: string,
    lastName?: string,
    email?: string,
    telephone?: string
};

type ContentfulHomePage = {
    title?: string,
    slug?: string,
    heroImage?: string,
    heroTitle?: string,
    heroTitleImage?: Array<Object>,
    featuredPosts?: Array<Object>
};

type ContentfulContentType = {
    type?: string,
    title?: string
};

type ContentfulEditorial = {
    title?: string,
    slug?: string,
    isFeatured?: *,
    pageType?: string,
    date?: *,
    introduction?: string,
    content?: string,
    featuredImage?: string,
    author?: string,
    downloads?: Array<Object>,
    meta?: string
};

type ContentfulDesignerFactFile = {
    title?: string,
    slug?: string,
    category?: string,
    topic?: string,
    businessStage?: Array<Object>,
    date?: *,
    introduction?: string,
    content?: string,
    featuredImage?: string,
    downloads?: Array<Object>,
    author?: string,
    meta?: string
};

type ContentfulEvent = {
    title?: string,
    slug?: string,
    isFeatured?: *,
    startDate?: *,
    endDate?: *,
    websiteUrl?: string,
    introduction?: string,
    content?: string,
    featuredImage?: string,
    ticketsUrl?: string,
    sponsorLogos?: Array<Object>,
    supporterLogos?: Array<Object>,
    supplierLogos?: Array<Object>,
    address?: string,
    author?: string,
    meta?: string
};

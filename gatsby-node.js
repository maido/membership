const fs = require('fs');
const path = require('path');
const filter = require('lodash/filter');
const flatMap = require('lodash/flatMap');
const get = require('lodash/get');
const kebabCase = require('lodash/kebabCase');
const sortBy = require('lodash/sortBy');
const uniq = require('lodash/uniq');
const dayjs = require('dayjs');

/**
 * code inluenced from:
 * https://github.com/dominicfallows/gatsby-plugin-json-output/blob/master/src/utils/createJsonFileAsync.ts
 */
const createJsonFileAsync = async (siteUrl, publicPath, _path, data) => {
    try {
        const fileObject = {
            $id: `${siteUrl}${path.join(_path)}`,
            $schema: 'http://json-schema.org/draft-07/schema#',
            ...data
        };
        const fileJson = JSON.stringify(flatMap(data));
        const filePath = path.resolve(path.join(publicPath, _path, 'feed.json'));

        await fs.writeFile(filePath, fileJson, err => {
            if (err) {
                console.log(err);
                throw new Error(err.message);
            }
        });
    } catch (err) {
        throw new Error(err);
    }
};

exports.createSchemaCustomization = ({actions, schema}) => {
    /**
     * Create a custom type for events so we can easily filter for past/future events.
     */
    actions.createTypes([
        schema.buildObjectType({
            name: 'ContentfulEvent',
            interfaces: ['Node'],
            fields: {
                isFuture: {
                    type: 'Boolean!',
                    resolve: source => {
                        return (
                            new Date(source.startDate) > new Date() ||
                            new Date(source.endDate) > new Date()
                        );
                    }
                }
            }
        })
    ]);

    /**
     * Looks like this is the new, preferred way for defining types.
     * TODO: Review previous type definitions.
     */
    const typeDefs = `
    type ContentfulDesignerFactFile implements Node {
        memberType: String
    }
    type ContentfulEditorial implements Node {
        memberType: String
    }
    type ContentfulEvent implements Node {
        memberType: String
    }
    type ContentfulCompany implements Node {
        memberType: String
    }
    type ContentfulOpportunitiesAndSupport implements Node {
        memberType: String
    }
    `;
    actions.createTypes(typeDefs);
};

exports.sourceNodes = ({actions}) => {
    const {createTypes} = actions;

    const typeDefs = `
        type ContentfulPerson implements Node @infer {
            company: String,
            city: String,
            tagline: String
            websiteUrl: String,
            facebookUrl: String,
            twitterUrl: String,
            youTubeUrl: String,
            instagramUrl: String
            contacts: ContentfulContactDetails
        }
        type ContentfulContactDetails implements Node @infer {
            title: String
        }
        type ContentfulDesignerFactFile implements Node {
            memberType: String
        }
        type ContentfulEditorial implements Node {
            memberType: String
        }
        type ContentfulEvent implements Node {
            memberType: String
        }
        type ContentfulCompany implements Node {
            memberType: String
        }
        type ContentfulOpportunitiesAndSupport implements Node {
            memberType: String
        }
        type ContentfulHomePage implements Node {
            heroTitleImage: [ContentfulAsset]
            introductionText: String
        }
    `;

    createTypes(typeDefs);
};

exports.createPages = ({graphql, actions}) => {
    const {createPage} = actions;

    const createListingPages = (
        content,
        templatePath,
        urlPath,
        pageType,
        paginationSize = 20,
        context = {}
    ) => {
        const paginationPages = Math.ceil(content.length / paginationSize) || 1;

        const paginationComponent = path.resolve(`./src/templates/${templatePath}-listing.js`);

        [...Array(paginationPages).keys()].map(i => {
            console.log(
                `creating page: /${urlPath}/page/${i + 1}/ (offset: ${
                    i > 0 ? i * paginationSize : 0
                }, page: ${i + 1})`
            );

            createPage({
                component: paginationComponent,
                context: {
                    ...context,
                    currentPage: i + 1,
                    limit: paginationSize,
                    pageType,
                    skip: i > 0 ? i * paginationSize : 0,
                    totalPosts: content.length,
                    urlPath
                },
                path: `/${urlPath}/page/${i + 1}/`
            });

            if (i === 0) {
                createPage({
                    component: paginationComponent,
                    context: {
                        ...context,
                        currentPage: i + 1,
                        limit: paginationSize,
                        pageType,
                        skip: i > 0 ? i * paginationSize : 0,
                        totalPosts: content.length,
                        urlPath
                    },
                    path: `/${urlPath}/`
                });
            }
        });
    };

    return new Promise((resolve, reject) => {
        graphql(`
            {
                events: allContentfulEvent(sort: {fields: [startDate], order: ASC}) {
                    edges {
                        node {
                            id
                            slug
                            startDate(formatString: "YYYY/MM/DD")
                            title
                            memberType
                            content {
                                content
                            }
                            featuredImage {
                                fixed(height: 200, width: 200) {
                                    src
                                }
                            }
                        }
                    }
                }

                announcements: allContentfulEditorial(
                    filter: {pageType: {eq: "Announcement"}}
                    sort: {fields: [date], order: DESC}
                ) {
                    edges {
                        node {
                            id
                            slug
                            title
                            memberType
                            content {
                                content
                            }
                            featuredImage {
                                fixed(height: 200, width: 200) {
                                    src
                                }
                            }
                        }
                    }
                }

                reportsAndInsights: allContentfulEditorial(
                    filter: {pageType: {eq: "Reports & Insights"}}
                    sort: {fields: [date], order: DESC}
                ) {
                    edges {
                        node {
                            id
                            slug
                            title
                            memberType
                            content {
                                content
                            }
                            featuredImage {
                                fixed(height: 200, width: 200) {
                                    src
                                }
                            }
                        }
                    }
                }

                news: allContentfulEditorial(
                    filter: {pageType: {eq: "News"}}
                    sort: {fields: [date], order: DESC}
                ) {
                    edges {
                        node {
                            id
                            slug
                            title
                            date
                            memberType
                            content {
                                content
                            }
                            pageType
                            featuredImage {
                                fixed(height: 200, width: 200) {
                                    src
                                }
                            }
                        }
                    }
                }

                companyTypes: allContentfulCompanyType(sort: {fields: [title], order: ASC}) {
                    edges {
                        node {
                            title
                        }
                    }
                }

                companyTypesOrdered: contentfulMemberNetworkIntroduction {
                    companyTypes {
                        title
                    }
                }

                memberNetworks: allContentfulCompany(
                    filter: {isInMemberNetwork: {eq: true}, companyType: {title: {ne: null}}}
                    sort: {fields: [title], order: ASC}
                ) {
                    edges {
                        node {
                            id
                            title
                            memberType
                            companyType {
                                title
                            }
                            content {
                                content
                            }
                            image {
                                fixed(height: 200, width: 200) {
                                    src
                                }
                            }
                        }
                    }
                }

                designerFactFiles: allContentfulDesignerFactFile(
                    sort: {fields: [title], order: ASC}
                ) {
                    edges {
                        node {
                            id
                            slug
                            title
                            memberType
                            category {
                                title
                            }
                            topic {
                                title
                            }
                            content {
                                content
                            }
                            featuredImage {
                                fixed(height: 200, width: 200) {
                                    src
                                }
                            }
                        }
                    }
                }

                opportunitiesAndSupport: allContentfulOpportunitiesAndSupport(
                    sort: {fields: [title], order: ASC}
                ) {
                    edges {
                        node {
                            id
                            slug
                            title
                            memberType
                            pageType
                            content {
                                content
                            }
                            featuredImage {
                                fixed(height: 200, width: 200) {
                                    src
                                }
                            }
                        }
                    }
                }
            }
        `)
            .then(result => {
                const indexedJSONFiles = [];

                if (result.errors) {
                    throw result.errors;
                    reject(result.errors);
                }

                if (result.data.events) {
                    const component = path.resolve(`./src/templates/event-detail.js`);

                    result.data.events.edges.map(edge => {
                        console.log(
                            `creating page: /event/${edge.node.startDate}/${edge.node.slug}`
                        );

                        createPage({
                            component,
                            context: {id: edge.node.id},
                            path: `/events/${edge.node.startDate}/${edge.node.slug}/`
                        });
                    });

                    indexedJSONFiles.push(
                        result.data.events.edges.map(edge => ({
                            endDate: dayjs(edge.node.endDate).format('dddd, MMMM YYYY'),
                            date: dayjs(edge.node.startDate).format('dddd, MMMM YYYY'),
                            id: edge.node.id,
                            image: get(edge, 'node.featuredImage.fixed.src'),
                            memberType: edge.node.memberType,
                            slug: edge.node.slug,
                            startDate: dayjs(edge.node.startDate).format('dddd, MMMM YYYY'),
                            title: edge.node.title,
                            type: 'Event'
                        }))
                    );
                }

                if (result.data.reportsAndInsights) {
                    const component = path.resolve(`./src/templates/editorial-detail.js`);

                    result.data.reportsAndInsights.edges.map(edge => {
                        console.log(`creating page: /reports-and-insights/${edge.node.slug}/`);

                        createPage({
                            component,
                            context: {id: edge.node.id, pageType: 'Reports & Insights'},
                            path: `/reports-and-insights/${edge.node.slug}/`
                        });
                    });

                    createListingPages(
                        result.data.reportsAndInsights.edges,
                        'editorial',
                        'reports-and-insights',
                        'Reports & Insights'
                    );
                    indexedJSONFiles.push(
                        result.data.reportsAndInsights.edges.map(edge => ({
                            content: edge.node.content ? edge.node.content.content : '',
                            id: edge.node.id,
                            image: get(edge, 'node.featuredImage.fixed.src'),
                            memberType: edge.node.memberType,
                            slug: edge.node.slug,
                            title: edge.node.title,
                            type: 'Reports & Insights'
                        }))
                    );
                }

                if (result.data.news) {
                    const component = path.resolve(`./src/templates/editorial-detail.js`);

                    result.data.news.edges.map(edge => {
                        console.log(`creating page: /news/${edge.node.slug}/`);

                        createPage({
                            component,
                            context: {id: edge.node.id, pageType: 'News'},
                            path: `/news/${edge.node.slug}/`
                        });
                    });

                    createListingPages(result.data.news.edges, 'editorial', 'news', 'News');
                    indexedJSONFiles.push(
                        result.data.news.edges.map(edge => ({
                            content: edge.node.content ? edge.node.content.content : '',
                            date: dayjs(edge.node.date).format('dddd, MMMM YYYY'),
                            id: edge.node.id,
                            image: get(edge, 'node.featuredImage.fixed.src'),
                            memberType: edge.node.memberType,
                            slug: edge.node.slug,
                            title: edge.node.title,
                            type: 'News'
                        }))
                    );
                }

                if (result.data.memberNetworks) {
                    const component = path.resolve(`./src/templates/member-network-detail.js`);

                    result.data.memberNetworks.edges.map(edge => {
                        console.log(
                            `creating page: /member-network/${kebabCase(
                                `${edge.node.companyType.title}`
                            )}s/${kebabCase(`${edge.node.title}`)}/`
                        );

                        createPage({
                            component,
                            context: {id: edge.node.id},
                            path: `/member-network/${kebabCase(
                                `${edge.node.companyType.title}`
                            )}s/${kebabCase(`${edge.node.title}`)}/`
                        });
                    });

                    let companyTypes = result.data.companyTypes.edges.map(c => c.node.title);

                    if (result.data.companyTypesOrdered) {
                        companyTypes = result.data.companyTypesOrdered.companyTypes.map(
                            c => c.title
                        );
                    }

                    companyTypes.map(companyType => {
                        createListingPages(
                            filter(
                                result.data.memberNetworks.edges,
                                c => c.node.companyType.title === companyType
                            ),
                            'member-network',
                            `member-network/${kebabCase(companyType)}s`,
                            'Member Network',
                            30,
                            {
                                allCompanyTypes: companyTypes.map(c => `${c}s`),
                                companyType
                            }
                        );
                    });

                    indexedJSONFiles.push(
                        result.data.memberNetworks.edges.map(edge => ({
                            content: edge.node.content ? edge.node.content.content : '',
                            id: edge.node.id,
                            image: get(edge, 'node.image.fixed.src'),
                            memberType: edge.node.memberType,
                            slug: edge.node.slug,
                            title: edge.node.title,
                            type: 'Member Network'
                        }))
                    );
                }

                if (result.data.designerFactFiles) {
                    const component = path.resolve(`./src/templates/designer-fact-file-detail.js`);

                    result.data.designerFactFiles.edges.map(edge => {
                        const category = kebabCase(get(edge, 'node.category.title', ''));
                        const topic = kebabCase(get(edge, 'node.topic.title', ''));

                        console.log(
                            `creating page: /designer-fact-files/${category}/${topic}/${edge.node.slug}/`
                        );

                        createPage({
                            component,
                            context: {id: edge.node.id},
                            path: `/designer-fact-files/${category}/${topic}/${edge.node.slug}/`
                        });
                    });

                    indexedJSONFiles.push(
                        result.data.designerFactFiles.edges.map(edge => ({
                            author: edge.node.author,
                            businessStage: edge.node.businessStage,
                            category: edge.node.category,
                            content: edge.node.content ? edge.node.content.content : '',
                            id: edge.node.id,
                            image: get(edge, 'node.featuredImage.fixed.src'),
                            memberType: edge.node.memberType,
                            slug: edge.node.slug,
                            title: edge.node.title,
                            topic: edge.node.topic,
                            type: 'Designer Fact Files'
                        }))
                    );
                }

                if (result.data.opportunitiesAndSupport) {
                    const component = path.resolve(
                        `./src/templates/opportunities-and-support-detail.js`
                    );

                    result.data.opportunitiesAndSupport.edges.map(edge => {
                        const pageType = kebabCase(get(edge, 'node.pageType', ''));

                        console.log(
                            `creating page: /opportunities-and-support/${pageType}/${edge.node.slug}/`
                        );

                        createPage({
                            component,
                            context: {id: edge.node.id},
                            path: `/opportunities-and-support/${pageType}/${edge.node.slug}/`
                        });
                    });

                    indexedJSONFiles.push(
                        result.data.opportunitiesAndSupport.edges.map(edge => ({
                            content: edge.node.content ? edge.node.content.content : '',
                            id: edge.node.id,
                            image: get(edge, 'node.featuredImage.fixed.src'),
                            memberType: edge.node.memberType,
                            pageType: edge.node.pageType,
                            slug: edge.node.slug,
                            title: edge.node.title,
                            type: 'Opportunities & Support'
                        }))
                    );
                }

                createJsonFileAsync(
                    'https://membership.britishfashioncouncil.com/',
                    './public',
                    '',
                    indexedJSONFiles
                );

                resolve();
            })
            .catch(console.log);
    });
};
//

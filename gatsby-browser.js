// @flow
const Cookie = require('js-cookie');
require('url-search-params-polyfill');

const redirectToLogin = () => {
    if (process.env.NODE_ENV === 'production') {
        window.location = 'https://www.britishfashioncouncil.co.uk/membership_view.aspx';
    }
};

const checkAuth = token => {
    fetch(`/.netlify/functions/validate-token?token=${token}`)
        .then(response => response.text())
        .then((response = '') => {
            if (response === 'MembershipValid=Yes') {
                Cookie.set('membership_token', token, {expires: 1});
            } else {
                redirectToLogin();
            }
        })
        .catch(() => redirectToLogin());
};

const checkCookie = () => {
    const token = Cookie.get('membership_token');

    if (!token) {
        redirectToLogin();
    } else {
        // checkAuth(token);
    }
};

exports.onClientEntry = () => {
    if (typeof window !== 'undefined') {
        const searchParams = new URLSearchParams(window.location.search);

        if (searchParams && searchParams.has('token')) {
            checkAuth(searchParams.get('token'));
        } else {
            checkCookie();
        }
    }
};

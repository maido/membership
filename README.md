# Contentful

Model types - https://www.contentful.com/developers/docs/concepts/data-model/
Migrations - https://github.com/contentful/contentful-migration
TypeScript interfaces - https://github.com/contentful/contentful-migration/blob/master/index.d.ts

## Scripts

Run a migration:
`contentful space migration --space-id [CONTENTFUL SPACE ID] contentful/migrations/[MIGRATION NUMBER].js`

Delete all entries, assets and content types (reset the space):
`node contentful/delete-all-content-and-content-models.js`

Scaffold basic starting data:
`node contentful/scaffold-data`

Export all environment data and assets:
`node contentful/export-content`

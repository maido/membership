/**
 * @prettier
 */
require('dotenv').config();
const contentfulExport = require('contentful-export');

const options = {
    environmentId: process.env.CONTENTFUL_ENVIRONMENT_ID,
    spaceId: process.env.CONTENTFUL_SPACE_ID,
    managementToken: process.env.CONTENTFUL_MANAGEMENT_ACCESS_TOKEN,
    exportDir: './contentful/exports',
    contentFile: `export-${process.env.CONTENTFUL_ENVIRONMENT_ID}-${new Date().toISOString()}.json`,
    includeDrafts: true,
    downloadAssets: true
};

contentfulExport(options)
    .then(result => console.log('Exported successfully!'))
    .catch(console.error);

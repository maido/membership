module.exports = migration => {
    const designerFactFile = migration.editContentType('designerFactFile');

    designerFactFile.editField('category').required(true);
    designerFactFile.editField('topic').required(true);

    //

    const designerFactFileIntroduction = migration
        .createContentType('designerFactFileIntroduction')
        .name('Designer Fact File - Introduction')
        .description('The landing page of the Designer Fact File.');

    designerFactFileIntroduction
        .createField('title')
        .name('Title')
        .type('Symbol')
        .required(true);
    designerFactFileIntroduction.displayField('title');

    designerFactFileIntroduction
        .createField('introduction')
        .name('Introduction')
        .type('Text')
        .required(true);
    designerFactFileIntroduction.changeEditorInterface('introduction', 'markdown');

    designerFactFileIntroduction
        .createField('businessStagesTitle')
        .name('Business stages - Title')
        .type('Symbol')
        .required(true);

    designerFactFileIntroduction
        .createField('businessStagesDescription')
        .name('Business stages - Description')
        .type('Text')
        .required(false);
    designerFactFileIntroduction.changeEditorInterface('businessStagesDescription', 'markdown');

    designerFactFileIntroduction
        .createField('businessStages')
        .name('Business stages')
        .type('Array')
        .items({
            type: 'Link',
            linkType: 'Entry',
            validations: [
                {
                    size: {min: 1, max: 10},
                    linkContentType: ['businessStage']
                }
            ]
        })
        .required(true);
    designerFactFileIntroduction.changeEditorInterface('businessStages', 'entryLinksEditor');
};

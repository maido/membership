module.exports = migration => {
    const event = migration.editContentType('event');

    event.changeEditorInterface('ticketsUrl', 'singleLine');
};

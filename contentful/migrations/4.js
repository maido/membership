const config = require('../config');

module.exports = migration => {
    const company = migration.editContentType('company');

    company
        .createField('companyType')
        .name('Company type')
        .type('Symbol')
        .required(true)
        .validations([{in: config.companyTypes}]);
    company.changeEditorInterface('companyType', 'dropdown');
    company.moveField('companyType').afterField('slug');

    company
        .createField('isInMemberNetwork')
        .name('Is in Member Network?')
        .type('Boolean');
    company.moveField('isInMemberNetwork').afterField('companyType');
};

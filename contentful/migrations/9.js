module.exports = migration => {
    const company = migration.editContentType('company');

    company.updateField('leadContactFirstName').required(false);

    company.updateField('leadContactLastName').required(false);

    company.updateField('leadContactRole').required(false);

    company.updateField('leadContactImage').required(false);
};

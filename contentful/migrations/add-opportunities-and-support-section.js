const config = require('../config');

module.exports = migration => {
    const opportunitiesAndSupport = migration
        .createContentType('opportunitiesAndSupport')
        .name('Opportunities and Support')
        .description('Content and downloads for opportunities and support articles.');

    opportunitiesAndSupport
        .createField('title')
        .name('Title')
        .type('Symbol')
        .required(true);
    opportunitiesAndSupport.displayField('title');

    opportunitiesAndSupport
        .createField('slug')
        .name('Slug')
        .type('Symbol')
        .required(true)
        .validations([{unique: true}]);
    opportunitiesAndSupport.changeEditorInterface('slug', 'slugEditor');

    opportunitiesAndSupport
        .createField('pageType')
        .name('Page type')
        .type('Symbol')
        .required(true)
        .validations([{in: config.opportunitiesAndSuppportTypes}]);
    opportunitiesAndSupport.changeEditorInterface('pageType', 'dropdown');

    opportunitiesAndSupport
        .createField('date')
        .name('Date')
        .type('Date')
        .required(false);
    opportunitiesAndSupport.changeEditorInterface('date', 'datePicker');

    opportunitiesAndSupport
        .createField('introduction')
        .name('Introduction')
        .type('Text')
        .required(false);
    opportunitiesAndSupport.changeEditorInterface('introduction', 'markdown');

    opportunitiesAndSupport
        .createField('content')
        .name('Content')
        .type('Text')
        .required(false);
    opportunitiesAndSupport.changeEditorInterface('content', 'markdown');

    opportunitiesAndSupport
        .createField('featuredImage')
        .name('Featured image')
        .type('Link')
        .linkType('Asset')
        .required(false);

    opportunitiesAndSupport
        .createField('downloads')
        .name('Downloads')
        .type('Array')
        .items({
            type: 'Link',
            linkType: 'Asset',
            validations: [{linkMimetypeGroup: config.assetTypes}]
        })
        .required(false);
    opportunitiesAndSupport.changeEditorInterface('downloads', 'assetLinksEditor');

    opportunitiesAndSupport
        .createField('meta')
        .name('Meta content')
        .type('Link')
        .linkType('Entry')
        .required(false)
        .validations([{linkContentType: ['meta']}]);
};

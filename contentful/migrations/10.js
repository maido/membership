module.exports = migration => {
    const memberNetworkIntroduction = migration
        .createContentType('memberNetworkIntroduction')
        .name('Member Network - Introduction')
        .description('The landing page of the Member Network.');

    memberNetworkIntroduction
        .createField('title')
        .name('Title')
        .type('Symbol')
        .required(true);
    memberNetworkIntroduction.displayField('title');

    memberNetworkIntroduction
        .createField('introduction')
        .name('Introduction')
        .type('Text')
        .required(true);
    memberNetworkIntroduction.changeEditorInterface('introduction', 'markdown');

    memberNetworkIntroduction
        .createField('companyTypeTitle')
        .name('Company types - Title')
        .type('Symbol')
        .required(true);

    memberNetworkIntroduction
        .createField('companyTypeDescription')
        .name('Company types - Description')
        .type('Text')
        .required(false);
    memberNetworkIntroduction.changeEditorInterface('companyTypeDescription', 'markdown');
};

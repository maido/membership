module.exports = migration => {
    const company = migration.editContentType('company');

    company
        .createField('instagramUrl')
        .name('Instagram URL')
        .type('Symbol')
        .required(false);
    company.changeEditorInterface('instagramUrl', 'urlEditor');
    company.moveField('instagramUrl').beforeField('twitterUrl');

    company
        .createField('leadContactRole')
        .name('Lead contact - role')
        .type('Symbol')
        .required(true);
    company.moveField('leadContactRole').beforeField('leadContactImage');

    company.deleteField('contactDetails');
    company
        .createField('contacts')
        .name('Contacts')
        .type('Array')
        .items({
            type: 'Link',
            linkType: 'Entry',
            validations: [{linkContentType: ['contactDetails']}]
        })
        .required(false);
    company.changeEditorInterface('contacts', 'entryLinksEditor');
    company.moveField('contacts').afterField('leadContactImage');

    company.moveField('address').afterField('contacts');
    company.moveField('tagline').beforeField('content');
};

module.exports = migration => {
    const editorial = migration.editContentType('editorial');

    editorial
        .createField('youTubeVideoUrl')
        .name('YouTube Video URL')
        .type('Symbol')
        .required(false);
    editorial.changeEditorInterface('youTubeVideoUrl', 'urlEditor');
    editorial.moveField('youTubeVideoUrl').afterField('content');
    // editorial.changeFieldControl('youTubeVideoUrl', 'builtin', 'singleLine', {
    //     helpText:
    //         'The URL should be formatted like this: https://www.youtube.com/watch?v=dlKLDJWzMGQ'
    // });
};

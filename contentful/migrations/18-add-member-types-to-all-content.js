const config = require('../config');

module.exports = migration => {
    const models = ['company', 'designerFactFile', 'event', 'editorial', 'opportunitiesAndSupport'];

    models.forEach(model => {
        const modelContentType = migration.editContentType(model);

        modelContentType
            .createField('memberType')
            .name('Member type')
            .type('Symbol')
            .required(false)
            .validations([{in: config.contentMemberTypes}]);
        modelContentType.changeEditorInterface('memberType', 'dropdown');
        modelContentType.moveField('memberType').afterField('slug');
    });
};

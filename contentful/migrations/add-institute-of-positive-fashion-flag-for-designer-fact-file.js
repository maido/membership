module.exports = migration => {
    const designerFactFile = migration.editContentType('designerFactFile');

    designerFactFile
        .createField('showOnInstituteOfPositiveFashionWebsite')
        .name('Show on Institute of Positive Fashion website?')
        .type('Boolean');
    designerFactFile.moveField('showOnInstituteOfPositiveFashionWebsite').afterField('author');
};

module.exports = migration => {
    const memberNetworkIntroduction = migration.editContentType('memberNetworkIntroduction');

    memberNetworkIntroduction
        .createField('ctaTitle')
        .name('CTA Title')
        .type('Symbol')
        .required(true);

    memberNetworkIntroduction
        .createField('ctaUrl')
        .name('CTA URL')
        .type('Text')
        .required(true);
};

module.exports = migration => {
    const address = migration.editContentType('address');

    address
        .createField('websiteUrl')
        .name('Website URL')
        .type('Symbol')
        .required(false);
    address.changeEditorInterface('websiteUrl', 'urlEditor');

    address
        .createField('telephone')
        .name('Telephone')
        .type('Symbol')
        .required(false);

    address
        .createField('email')
        .name('Email')
        .type('Symbol')
        .required(false);
};

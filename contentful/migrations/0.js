const config = require('../config');

module.exports = migration => {
    /**
     * Meta content
     */
    const meta = migration
        .createContentType('meta')
        .name('Meta content')
        .description('Meta content for social sharing and SEO');

    meta.createField('title')
        .name('Title')
        .type('Symbol')
        .required(true);
    meta.displayField('title');

    meta.createField('description')
        .name('Description')
        .type('Symbol')
        .required(false);

    meta.createField('image')
        .name('Image')
        .type('Link')
        .linkType('Asset')
        .required(false);

    meta.createField('twitterTitle')
        .name('Twitter Title')
        .type('Symbol')
        .required(false);
    meta.changeEditorInterface('twitterTitle', 'singleLine', {
        helpText: 'If left blank, the Title value will be used (if provided)'
    });

    meta.createField('twitterDescription')
        .name('Twitter Description')
        .type('Symbol')
        .required(false);
    meta.changeEditorInterface('twitterDescription', 'singleLine', {
        helpText: 'If left blank, the Description value will be used (if provided)'
    });

    meta.createField('twitterImage')
        .name('Twitter Image')
        .type('Link')
        .linkType('Asset')
        .required(false);
    meta.changeEditorInterface('twitterImage', 'assetLinkEditor', {
        helpText: 'If left blank, the Image file will be used (if provided)'
    });

    /**
     * Business stages
     */
    const businessStage = migration
        .createContentType('businessStage')
        .name('Business stage')
        .description('Categorising of business by their size');

    businessStage
        .createField('title')
        .name('Title')
        .type('Symbol')
        .required(true)
        .validations([{unique: true}]);
    businessStage.displayField('title');

    /**
     * Content categorisation
     */
    const contentType = migration
        .createContentType('contentType')
        .name('Content type')
        .description('Categorising content');

    contentType
        .createField('type')
        .name('Type')
        .type('Symbol')
        .required(true)
        .validations([{in: config.contentTypes}]);
    contentType.changeEditorInterface('type', 'dropdown');

    contentType
        .createField('title')
        .name('Title')
        .type('Symbol')
        .required(true);
    contentType.displayField('title');

    /**
     * Contact details
     */
    const contactDetails = migration
        .createContentType('contactDetails')
        .name('Contact details')
        .description('Contact details for a person or company');

    contactDetails
        .createField('title')
        .name('Title')
        .type('Symbol')
        .required(true)
        .validations([{unique: true}]);
    contactDetails.displayField('title');

    contactDetails
        .createField('firstName')
        .name('First name')
        .type('Symbol')
        .required(true);

    contactDetails
        .createField('lastName')
        .name('Last name')
        .type('Symbol')
        .required(true);

    contactDetails
        .createField('email')
        .name('Email')
        .type('Symbol')
        .required(false);

    contactDetails
        .createField('telephone')
        .name('Telephone')
        .type('Symbol')
        .required(false);

    /**
     * Address
     */
    const address = migration
        .createContentType('address')
        .name('Address')
        .description('An address for a person or company');

    address
        .createField('name')
        .name('Name / Company name')
        .type('Symbol')
        .required(true)
        .validations([{unique: true}]);
    address.displayField('name');

    address
        .createField('street')
        .name('Street')
        .type('Symbol')
        .required(true);

    address
        .createField('city')
        .name('City')
        .type('Symbol')
        .required(true);

    address
        .createField('country')
        .name('Country')
        .type('Symbol')
        .required(true)
        .validations([{in: config.countries}]);
    address.changeEditorInterface('country', 'dropdown');

    address
        .createField('postcode')
        .name('Postcode')
        .type('Symbol')
        .required(false);

    /**
     * Company
     */
    const company = migration
        .createContentType('company')
        .name('Company')
        .description('Details for a company');

    company
        .createField('title')
        .name('Title')
        .type('Symbol')
        .required(true)
        .validations([{unique: true}]);
    company.displayField('title');

    company
        .createField('slug')
        .name('Slug')
        .type('Symbol')
        .required(true);
    company.changeEditorInterface('slug', 'slugEditor');

    company
        .createField('address')
        .name('Address')
        .type('Link')
        .linkType('Entry')
        .required(false)
        .validations([{linkContentType: ['address']}]);

    company
        .createField('contactDetails')
        .name('Contact details')
        .type('Link')
        .linkType('Entry')
        .required(false)
        .validations([{linkContentType: ['contactDetails']}]);

    company
        .createField('websiteUrl')
        .name('Website URL')
        .type('Symbol')
        .required(false);
    company.changeEditorInterface('websiteUrl', 'urlEditor');

    company
        .createField('facebookUrl')
        .name('Facebook URL')
        .type('Symbol')
        .required(false);
    company.changeEditorInterface('facebookUrl', 'urlEditor');

    company
        .createField('twitterUrl')
        .name('Twitter URL')
        .type('Symbol')
        .required(false);
    company.changeEditorInterface('twitterUrl', 'urlEditor');

    company
        .createField('youTubeUrl')
        .name('YouTube URL')
        .type('Symbol')
        .required(false);
    company.changeEditorInterface('youTubeUrl', 'urlEditor');

    company
        .createField('image')
        .name('Image')
        .type('Link')
        .linkType('Asset')
        .required(false);

    company
        .createField('content')
        .name('Content')
        .type('Text')
        .required(false);
    company.changeEditorInterface('content', 'markdown');

    company
        .createField('tagline')
        .name('Tagline')
        .type('Symbol')
        .required(false);

    /**
     * Person
     */
    const person = migration
        .createContentType('person')
        .name('Person')
        .description('Details for a person');

    person
        .createField('firstName')
        .name('First name')
        .type('Symbol')
        .required(true);
    person.displayField('firstName');

    person
        .createField('lastName')
        .name('Last name')
        .type('Symbol')
        .required(true);

    person
        .createField('isInMemberNetwork')
        .name('Is in Member Network?')
        .type('Boolean');

    person
        .createField('company')
        .name('Company')
        .type('Link')
        .linkType('Entry')
        .required(false)
        .validations([{linkContentType: ['company']}]);

    person
        .createField('city')
        .name('City')
        .type('Symbol')
        .required(false);

    person
        .createField('websiteUrl')
        .name('Website URL')
        .type('Symbol')
        .required(false);
    person.changeEditorInterface('websiteUrl', 'urlEditor');

    person
        .createField('facebookUrl')
        .name('Facebook URL')
        .type('Symbol')
        .required(false);
    person.changeEditorInterface('facebookUrl', 'urlEditor');

    person
        .createField('twitterUrl')
        .name('Twitter URL')
        .type('Symbol')
        .required(false);
    person.changeEditorInterface('twitterUrl', 'urlEditor');

    person
        .createField('instagramUrl')
        .name('Instagram URL')
        .type('Symbol')
        .required(false);
    person.changeEditorInterface('instagramUrl', 'urlEditor');

    person
        .createField('youTubeUrl')
        .name('YouTube URL')
        .type('Symbol')
        .required(false);
    person.changeEditorInterface('youTubeUrl', 'urlEditor');

    person
        .createField('image')
        .name('Image')
        .type('Link')
        .linkType('Asset')
        .required(false);

    person
        .createField('tagline')
        .name('Tagline')
        .type('Symbol')
        .required(false);

    person
        .createField('content')
        .name('Content')
        .type('Text')
        .required(false);
    person.changeEditorInterface('content', 'markdown');

    person
        .createField('contacts')
        .name('Contacts')
        .type('Array')
        .items({
            type: 'Link',
            linkType: 'Entry',
            validations: [{linkContentType: ['contactDetails']}]
        })
        .required(false);
    person.changeEditorInterface('contacts', 'entryLinksEditor');

    /**
     * Asset
     */
    const asset = migration
        .createContentType('asset')
        .name('Asset')
        .description('Downloadable asset files');

    asset
        .createField('title')
        .name('Title')
        .type('Symbol')
        .required(true);
    asset.displayField('title');

    asset
        .createField('image')
        .name('Image')
        .type('Array')
        .items({
            type: 'Link',
            linkType: 'Asset',
            validations: [{linkMimetypeGroup: config.assetTypes}]
        })
        .required(false);
    asset.changeEditorInterface('image', 'assetLinksEditor');

    /**
     * Editorial
     */
    const editorial = migration
        .createContentType('editorial')
        .name('Editorial')
        .description('Long-form editorial pages, such as an announcement');

    editorial
        .createField('title')
        .name('Title')
        .type('Symbol')
        .required(true);
    editorial.displayField('title');

    editorial
        .createField('slug')
        .name('Slug')
        .type('Symbol')
        .required(true)
        .validations([{unique: true}]);
    editorial.changeEditorInterface('slug', 'slugEditor');

    editorial
        .createField('isFeatured')
        .name('Is featured')
        .type('Boolean')
        .required(false);

    editorial
        .createField('pageType')
        .name('Page type')
        .type('Symbol')
        .required(true)
        .validations([{in: config.editorialPageTypes}]);
    editorial.changeEditorInterface('pageType', 'dropdown');

    editorial
        .createField('date')
        .name('Date')
        .type('Date')
        .required(false);
    editorial.changeEditorInterface('date', 'datePicker');

    editorial
        .createField('introduction')
        .name('Introduction')
        .type('Text')
        .required(false);
    editorial.changeEditorInterface('introduction', 'markdown');

    editorial
        .createField('content')
        .name('Content')
        .type('Text')
        .required(false);
    editorial.changeEditorInterface('content', 'markdown');

    editorial
        .createField('featuredImage')
        .name('Featured image')
        .type('Link')
        .linkType('Asset')
        .required(false);

    editorial
        .createField('author')
        .name('Author')
        .type('Link')
        .linkType('Entry')
        .required(false)
        .validations([{linkContentType: ['person', 'company']}]);

    editorial
        .createField('downloads')
        .name('Downloads')
        .type('Array')
        .items({
            type: 'Link',
            linkType: 'Asset',
            validations: [{linkMimetypeGroup: config.assetTypes}]
        })
        .required(false);
    editorial.changeEditorInterface('downloads', 'assetLinksEditor');

    editorial
        .createField('meta')
        .name('Meta content')
        .type('Link')
        .linkType('Entry')
        .required(false)
        .validations([{linkContentType: ['meta']}]);

    /**
     * Event
     */
    const event = migration
        .createContentType('event')
        .name('Event')
        .description('Events');

    event
        .createField('title')
        .name('Title')
        .type('Symbol')
        .required(true);
    event.displayField('title');

    event
        .createField('slug')
        .name('Slug')
        .type('Symbol')
        .required(true)
        .validations([{unique: true}]);
    event.changeEditorInterface('slug', 'slugEditor');

    event
        .createField('isFeatured')
        .name('Is featured')
        .type('Boolean')
        .required(false);

    event
        .createField('startDate')
        .name('Start Date')
        .type('Date')
        .required(true);
    event.changeEditorInterface('date', 'datePicker');

    event
        .createField('endDate')
        .name('End Date')
        .type('Date')
        .required(false);
    event.changeEditorInterface('date', 'datePicker');

    event
        .createField('websiteUrl')
        .name('Website URL')
        .type('Symbol')
        .required(false);
    event.changeEditorInterface('websiteUrl', 'urlEditor');

    event
        .createField('introduction')
        .name('Introduction')
        .type('Text')
        .required(false);
    event.changeEditorInterface('introduction', 'markdown');

    event
        .createField('content')
        .name('Content')
        .type('Text')
        .required(false);
    event.changeEditorInterface('content', 'markdown');

    event
        .createField('featuredImage')
        .name('Featured image')
        .type('Link')
        .linkType('Asset')
        .required(false);

    event
        .createField('ticketsUrl')
        .name('Tickets URL')
        .type('Symbol')
        .required(false);
    event.changeEditorInterface('ticketsUrl', 'urlEditor');

    event
        .createField('address')
        .name('Address')
        .type('Link')
        .linkType('Entry')
        .required(false)
        .validations([{linkContentType: ['address']}]);

    event
        .createField('author')
        .name('Author')
        .type('Link')
        .linkType('Entry')
        .required(false)
        .validations([{linkContentType: ['person', 'company']}]);

    event
        .createField('meta')
        .name('Meta content')
        .type('Link')
        .linkType('Entry')
        .required(false)
        .validations([{linkContentType: ['meta']}]);

    /**
     * Designer Fact File
     */
    const designerFactFile = migration
        .createContentType('designerFactFile')
        .name('Designer Fact File')
        .description('Content and downloads for designers and companies');

    designerFactFile
        .createField('title')
        .name('Title')
        .type('Symbol')
        .required(true);
    designerFactFile.displayField('title');

    designerFactFile
        .createField('slug')
        .name('Slug')
        .type('Symbol')
        .required(true)
        .validations([{unique: true}]);
    designerFactFile.changeEditorInterface('slug', 'slugEditor');

    designerFactFile
        .createField('category')
        .name('Category')
        .type('Link')
        .linkType('Entry')
        .required(false)
        .validations([{linkContentType: ['contentType']}]);

    designerFactFile
        .createField('topic')
        .name('Topic')
        .type('Link')
        .linkType('Entry')
        .required(false)
        .validations([{linkContentType: ['contentType']}]);

    designerFactFile
        .createField('businessStage')
        .name('Business stage')
        .type('Array')
        .items({
            type: 'Link',
            linkType: 'Entry',
            validations: [{linkContentType: ['businessStage']}]
        })
        .required(true);
    designerFactFile.changeEditorInterface('businessStage', 'entryLinksEditor');

    designerFactFile
        .createField('date')
        .name('Date')
        .type('Date')
        .required(false);
    designerFactFile.changeEditorInterface('date', 'datePicker');

    designerFactFile
        .createField('introduction')
        .name('Introduction')
        .type('Text')
        .required(false);
    designerFactFile.changeEditorInterface('introduction', 'markdown');

    designerFactFile
        .createField('content')
        .name('Content')
        .type('Text')
        .required(false);
    designerFactFile.changeEditorInterface('content', 'markdown');

    designerFactFile
        .createField('featuredImage')
        .name('Featured image')
        .type('Link')
        .linkType('Asset')
        .required(false);

    designerFactFile
        .createField('author')
        .name('Author')
        .type('Link')
        .linkType('Entry')
        .required(false)
        .validations([{linkContentType: ['person', 'company']}]);

    designerFactFile
        .createField('downloads')
        .name('Downloads')
        .type('Array')
        .items({
            type: 'Link',
            linkType: 'Asset',
            validations: [{linkMimetypeGroup: config.assetTypes}]
        })
        .required(false);
    asset.changeEditorInterface('image', 'assetLinksEditor');

    designerFactFile
        .createField('meta')
        .name('Meta content')
        .type('Link')
        .linkType('Entry')
        .required(false)
        .validations([{linkContentType: ['meta']}]);

    /**
     * Home page
     */
    const homePage = migration
        .createContentType('homePage')
        .name('Home Page')
        .description('Curated content and images for the home page');

    homePage
        .createField('title')
        .name('Title')
        .type('Symbol')
        .required(true);
    homePage.displayField('title');

    homePage
        .createField('slug')
        .name('Slug')
        .type('Symbol')
        .required(true)
        .validations([{unique: true}]);
    homePage.changeEditorInterface('slug', 'slugEditor');

    homePage
        .createField('featuredPosts')
        .name('Featured posts')
        .type('Array')
        .items({
            type: 'Link',
            linkType: 'Entry',
            validations: [
                {
                    size: {min: 1, max: 10},
                    linkContentType: ['editorial', 'event', 'designerFactFile', 'company']
                }
            ]
        })
        .required(true);
    homePage.changeEditorInterface('businessStage', 'entryLinksEditor');

    homePage
        .createField('heroImage')
        .name('Hero image')
        .type('Array')
        .items({
            type: 'Link',
            linkType: 'Asset',
            validations: [{size: {min: 1, max: 1}, linkMimetypeGroup: 'image'}]
        })
        .required(false);
    homePage.changeEditorInterface('heroImage', 'assetLinksEditor');
};

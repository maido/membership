const config = require('../config');

module.exports = migration => {
    const editorial = migration.editContentType('editorial');

    editorial
        .editField('pageType')
        .validations([{in: config.editorialPageTypes}]);
};
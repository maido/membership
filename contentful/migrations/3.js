module.exports = migration => {
    const company = migration.editContentType('company');

    company
        .createField('leadContactFirstName')
        .name('Lead contact - First name')
        .type('Symbol')
        .required(false);
    company.moveField('leadContactFirstName').afterField('tagline');

    company
        .createField('leadContactLastName')
        .name('Lead contact - Last name')
        .type('Symbol')
        .required(false);
    company.moveField('leadContactLastName').afterField('leadContactFirstName');

    company
        .createField('leadContactImage')
        .name('Lead contact - image')
        .type('Link')
        .linkType('Asset')
        .required(false);
    company.changeEditorInterface('leadContactImage', 'assetLinkEditor');
    company.moveField('leadContactImage').afterField('leadContactLastName');
};

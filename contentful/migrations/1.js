const config = require('../config');

module.exports = migration => {
    const event = migration.editContentType('event');

    event
        .createField('sponsorLogos')
        .name('Sponsor logos')
        .type('Array')
        .items({
            type: 'Link',
            linkType: 'Asset',
            validations: [{linkMimetypeGroup: 'image'}]
        })
        .required(false);
    event.changeEditorInterface('sponsorLogos', 'assetLinksEditor');
    event.moveField('sponsorLogos').beforeField('address');

    event
        .createField('supporterLogos')
        .name('Supporter logos')
        .type('Array')
        .items({
            type: 'Link',
            linkType: 'Asset',
            validations: [{linkMimetypeGroup: 'image'}]
        })
        .required(false);
    event.changeEditorInterface('supporterLogos', 'assetLinksEditor');
    event.moveField('supporterLogos').afterField('sponsorLogos');

    event
        .createField('supplierLogos')
        .name('Supplier logos')
        .type('Array')
        .items({
            type: 'Link',
            linkType: 'Asset',
            validations: [{linkMimetypeGroup: 'image'}]
        })
        .required(false);
    event.changeEditorInterface('supplierLogos', 'assetLinksEditor');
    event.moveField('supplierLogos').afterField('supporterLogos');
};

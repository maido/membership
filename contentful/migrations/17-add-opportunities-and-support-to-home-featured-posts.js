module.exports = migration => {
    const homePage = migration.editContentType('homePage');

    homePage
        .editField('featuredPosts')
        .name('Featured posts')
        .type('Array')
        .items({
            type: 'Link',
            linkType: 'Entry',
            validations: [
                {
                    size: {min: 1, max: 10},
                    linkContentType: [
                        'editorial',
                        'event',
                        'designerFactFile',
                        'company',
                        'opportunitiesAndSupport'
                    ]
                }
            ]
        })
        .required(true);
};

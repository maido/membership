module.exports = migration => {
    const homePage = migration.editContentType('homePage');

    homePage
        .createField('introductionText')
        .name('Introduction text')
        .type('Symbol')
        .required(true);
};

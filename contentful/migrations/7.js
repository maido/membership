module.exports = migration => {
    const businessStage = migration.editContentType('businessStage');

    businessStage
        .createField('description')
        .name('Description')
        .type('Symbol')
        .required(true);
};

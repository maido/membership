module.exports = migration => {
    const homePage = migration.editContentType('homePage');

    homePage
        .createField('heroTitle')
        .name('Hero title')
        .type('Symbol')
        .required(true);
    homePage.moveField('heroTitle').afterField('heroImage');

    homePage
        .createField('heroTitleImage')
        .name('Hero title image')
        .type('Array')
        .items({
            type: 'Link',
            linkType: 'Asset',
            validations: [{linkMimetypeGroup: 'image'}]
        })
        .required(true);
    homePage.changeEditorInterface('heroTitleImage', 'assetLinksEditor');
    homePage.moveField('heroTitleImage').afterField('heroTitle');
};

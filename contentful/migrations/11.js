module.exports = migration => {
    const companyType = migration
        .createContentType('companyType')
        .name('Company type')
        .description('Categorising of company types for the Member Network.');

    companyType
        .createField('title')
        .name('Title')
        .type('Symbol')
        .required(true)
        .validations([{unique: true}]);
    companyType.displayField('title');

    companyType
        .createField('description')
        .name('Description')
        .type('Symbol')
        .required(true);

    companyType
        .createField('image')
        .name('Image')
        .type('Link')
        .linkType('Asset')
        .required(true);

    //

    const company = migration.editContentType('company');

    company.deleteField('companyType');

    company
        .createField('companyType')
        .name('Company type')
        .type('Link')
        .linkType('Entry')
        .validations([{linkContentType: ['companyType']}])
        .required(false);
    company.changeEditorInterface('companyType', 'entryLinksEditor');

    //

    const memberNetworkIntroduction = migration.editContentType('memberNetworkIntroduction');

    memberNetworkIntroduction
        .createField('companyTypes')
        .name('Company Types')
        .type('Array')
        .items({
            type: 'Link',
            linkType: 'Entry',
            validations: [{size: {min: 1, max: 10}}, {linkContentType: ['companyType']}]
        })
        .required(true);
    memberNetworkIntroduction.changeEditorInterface('companyTypes', 'entryLinksEditor');
};

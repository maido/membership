require('dotenv').config();
const fs = require('fs');
const contentful = require('contentful-management');
const find = require('lodash/find');
const kebabCase = require('lodash/kebabCase');
const map = require('lodash/map');
const config = require('../config');

const client = contentful.createClient({
    accessToken: process.env.CONTENTFUL_MANAGEMENT_ACCESS_TOKEN
});

const data = [
    {
        title: 'London Fashion Week Mens June 2019',
        startDate: '08 June 2019',
        endDate: '10 June 2019'
    },
    {
        title: 'Fashion Forum',
        startDate: '18 June 2019',
        endDate: '17 June 2019'
    },
    {
        title: 'LONDON show ROOMS - Paris Menswear',
        startDate: '20 June 2019',
        endDate: '25 June 2019'
    },
    {
        title: 'London Fashion Week September 2019',
        startDate: '13 September 2019',
        endDate: '17 September 2019'
    },
    {
        title: 'LONDON show ROOMS - Paris Womenswear',
        startDate: '25 September 2019',
        endDate: '01 October 2019'
    },
    {
        title: 'The Fashion Awards',
        startDate: '02 December 2019'
    },
    {
        title: 'London Fashion Week Mens January 2020',
        startDate: '04 January 2020',
        endDate: '06 January 2020'
    },
    {
        title: 'London Fashion Week February 2020',
        startDate: '12 February 2020',
        endDate: '14 January 2020'
    },
    {
        title: 'London Fashion Week June Mens 2020',
        startDate: '13 June 2020',
        endDate: '15 June 2020'
    },
    {
        title: 'London Fashion Week September 2020',
        startDate: '18 September 2020',
        endDate: '18 September 2020'
    }
].map(i => ({
    ...i,
    slug: kebabCase(i.title)
}));

const scrapeData = () => {
    /**
     * Run this on https://www.britishfashioncouncil.co.uk/Calendar
     */
    const $items = document.querySelectorAll('.calendar-list a');

    [...$items].map($item => {
        const $background = $item.querySelector('.layer-type-6-panels-item-calendar');
        const $title = $item.querySelector('.layer-type-6-panels-item-title');
        const $date = $item.querySelector('.layer-type-6-panels-item-footer');

        $title.style.textTransform = 'none';

        let image = $background.style.backgroundImage.replace('url("', '');
        image = image.replace('url("', '');
        image = image.replace('")', '');

        return {
            image,
            title: $title.innerText,
            date: $date.innerText
        };
    });
};

const formatEntryFields = fields => {
    const formattedFields = {};

    map(fields, (value, key) => {
        if (key === 'startDate' || key === 'endDate') {
            value = new Date(value).toISOString();
            formattedFields[key] = {[config.locale]: value};
        } else {
            formattedFields[key] = {[config.locale]: value};
        }
    });

    return formattedFields;
};

const createEntry = async (environment, contentTypeId, fields) => {
    return new Promise(resolve => {
        environment
            .createEntry(contentTypeId, {fields: formatEntryFields(fields)})
            .then(entry => entry.publish())
            .then(entry => resolve(entry.sys.id));
    });
};

const createEvents = environment => {
    const ids = data.map(async item => createEntry(environment, 'event', item));

    return Promise.all(ids);
};

const importData = async environment => {
    console.log('Starting import');

    const eventIds = await createEvents(environment);
    eventIds.map(eventId => console.log(`Event created: ${eventId}`));
};

client
    .getSpace(process.env.CONTENTFUL_SPACE_ID)
    .then(space => space.getEnvironment(process.env.CONTENTFUL_ENVIRONMENT_ID))
    .then(importData);

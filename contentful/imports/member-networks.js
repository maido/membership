require('dotenv').config();
const contentful = require('contentful-management');
const map = require('lodash/map');
const kebabCase = require('lodash/kebabCase');
const faker = require('faker');
const config = require('../config');

const client = contentful.createClient({
    accessToken: process.env.CONTENTFUL_MANAGEMENT_ACCESS_TOKEN
});

const dataToImport = [...Array(50).keys()].map(i => ({
    firstName: faker.name.firstName(),
    lastName: faker.name.lastName(),
    isInMemberNetwork: true,
    city: faker.address.city(),
    image: faker.image.avatar(),
    websiteUrl: faker.internet.url()
}));

const formatEntryFields = (fields, assetId) => {
    const formattedFields = {};

    map(fields, (value, key) => {
        if (key === 'image') {
            formattedFields[key] = {
                [config.locale]: {
                    sys: {
                        type: 'Link',
                        linkType: 'Asset',
                        id: assetId
                    }
                }
            };
        } else {
            formattedFields[key] = {[config.locale]: value};
        }
    });

    return formattedFields;
};

const createEntries = async environment => {
    const ids = dataToImport.map(async entry => {
        return new Promise(resolve => {
            environment
                .createAsset({
                    fields: {
                        title: {[config.locale]: `${entry.firstName} ${entry.lastName}`},
                        file: {
                            [config.locale]: {
                                contentType: 'image/jpeg',
                                fileName: `${kebabCase(
                                    `${entry.firstName} ${entry.lastName}`
                                )}.jpg`,
                                upload: entry.image
                            }
                        }
                    }
                })
                .then(asset => asset.processForAllLocales())
                .then(asset => asset.publish())
                .then(asset => {
                    environment
                        .createEntry('person', {
                            fields: formatEntryFields(
                                {
                                    firstName: entry.firstName,
                                    lastName: entry.lastName,
                                    isInMemberNetwork: entry.isInMemberNetwork,
                                    city: entry.city,
                                    websiteUrl: entry.websiteUrl,
                                    image: entry.image
                                },
                                asset.sys.id
                            )
                        })
                        .then(newEntry => newEntry.publish())
                        .then(newEntry => resolve(newEntry.sys.id));
                });
        });
    });

    return Promise.all(ids);
};

const importData = async environment => {
    console.log('Starting import');

    const eventIds = await createEntries(environment);
    eventIds.map(eventId => console.log(`Person created: ${eventId}`));
};

client
    .getSpace(process.env.CONTENTFUL_SPACE_ID)
    .then(space => space.getEnvironment(process.env.CONTENTFUL_ENVIRONMENT_ID))
    .then(importData);

require('dotenv').config();
const fs = require('fs');
const contentful = require('contentful-management');
const get = require('lodash/get');
const find = require('lodash/find');
const head = require('lodash/head');
const map = require('lodash/map');
const kebabCase = require('lodash/kebabCase');
const split = require('lodash/split');
const config = require('../config');

const client = contentful.createClient({
    accessToken: process.env.CONTENTFUL_MANAGEMENT_ACCESS_TOKEN
});

const scrapeData = () => {
    /**
     * Run this on https://www.designerfactfile.com/DFF-Curriculum
     */
    const $items = document.querySelectorAll('.course-materials > .row');

    const entries = [...$items]
        .map($item => {
            const $title = $item.querySelector('.curriculum-area-title');
            const $topics = $item.querySelectorAll('.curriculum-area-topics a');
            const $coursesContainers = $item.nextElementSibling.querySelectorAll(
                '.course-materials-container'
            );

            const title = $title.innerText;
            const topics = [...$topics].map($topic => $topic.innerText);
            const coursesAndTopics = [...$coursesContainers].map(
                ($coursesContainer, topicIndex) => {
                    const $courses = $coursesContainer.querySelectorAll('.course-materials-item');
                    const topic = topics[topicIndex];
                    const courses = [...$courses].map($course => {
                        const $courseTitle = $course.querySelector('.course-materials-item-title');
                        const $businessStage = $course.querySelector(
                            '.course-materials-item-business-stage'
                        );
                        const $image = $course.querySelector('.course-materials-item-image > img');

                        return {
                            title: $courseTitle.innerText.trim().replace(/(\r\n|\n|\r)/gm, ''),
                            businessStage: $businessStage.innerText
                                .trim()
                                .replace(/(\r\n|\n|\r)/gm, ''),
                            featuredImage: $image.src,
                            topic: topics[topicIndex],
                            category: title
                        };
                    });

                    return courses;
                }
            );

            return coursesAndTopics.flatMap(c => c);
        })
        .flatMap(e => e);

    console.log(entries);
};

const dataToImport = [
    {
        title: 'Registering Your Business: Sole Trader, Partnership or Limited Company',
        businessStage: 'Explorer',
        featuredImage:
            'https://www.designerfactfile.com/uploads/files/1/CourseMaterials/David-Koma-SS15-Christopher-James-British-Fashion-Council-6.jpeg?w=500&h=500&mode=crop',
        topic: 'Business Strategy',
        category: 'Business Structure'
    },
    {
        title: 'Choosing Your Business Model',
        businessStage: 'Start up',
        featuredImage:
            'https://www.designerfactfile.com/uploads/files/1/CourseMaterials/Eudon-Choi-SS15-Daniel-Sims-British-Fashion-Council-1jpg.jpg?w=500&h=500&mode=crop',
        topic: 'Business Strategy',
        category: 'Business Structure'
    },
    {
        title: 'Developing Your Business Model',
        businessStage: 'Start up',
        featuredImage:
            'https://www.designerfactfile.com/uploads/files/1/CourseMaterials/Bora-Aksu-AW16-FOH-Kris-Mitchel-British-Fashion-CouncilLoRes-1.jpg?w=500&h=500&mode=crop',
        topic: 'Business Strategy',
        category: 'Business Structure'
    },
    {
        title: 'Fashion Business: Challenges & Opportunities',
        businessStage: 'Start up',
        featuredImage:
            'https://www.designerfactfile.com/uploads/files/1/CourseMaterials/Anya-Hindmarch-AW16-Backstage-Sam-Wilson-British-Fashion-Council-LoRes-1.jpg?w=500&h=500&mode=crop',
        topic: 'Business Strategy',
        category: 'Business Structure'
    },
    {
        title: 'Performance Against Competitors: Mapping Your Position',
        businessStage: 'Entrepreneur',
        featuredImage:
            'https://www.designerfactfile.com/uploads/files/1/CourseMaterials/Mulberry-AW16-FOH-Shaun-James-Cox-British-Fashion-Council-LoRes-1.jpg?w=500&h=500&mode=crop',
        topic: 'Business Strategy',
        category: 'Business Structure'
    },
    {
        title: 'Anti-Competition Law',
        businessStage: 'Start up',
        featuredImage:
            'https://www.designerfactfile.com/uploads/files/1/CourseMaterials/FYODOR-GOLAN-SS16-FOH-Dan-Sims-British-Fashion-Council-Hi-Res4.jpg?w=500&h=500&mode=crop',
        topic: 'Legal',
        category: 'Business Structure'
    },
    {
        title: 'How to Protect your Designs and Design Rights',
        businessStage: 'Start up',
        featuredImage:
            'https://www.designerfactfile.com/images/default-course-material.jpg?w=500&h=500&mode=crop',
        topic: 'Legal',
        category: 'Business Structure'
    },
    {
        title: 'Intellectual Property & Trade Marks',
        businessStage: 'Explorer',
        featuredImage:
            'https://www.designerfactfile.com/uploads/files/1/CourseMaterials/Burberry-AW16-FOH-Dan-Sims-British-Fashion-Council-Lo-Res3.jpg?w=500&h=500&mode=crop',
        topic: 'Legal',
        category: 'Business Structure'
    },
    {
        title: 'Licensing and Collaborations',
        businessStage: 'Start up',
        featuredImage:
            'https://www.designerfactfile.com/images/default-course-material.jpg?w=500&h=500&mode=crop',
        topic: 'Legal',
        category: 'Business Structure'
    },
    {
        title: 'Using Agreements & Contracts',
        businessStage: 'Explorer',
        featuredImage:
            'https://www.designerfactfile.com/uploads/files/1/CourseMaterials/Peter-Pilotto-SS16-Front-of-House-Dan-Sims-British-Fashion-Council-Hi-Res3.JPG?w=500&h=500&mode=crop',
        topic: 'Legal',
        category: 'Business Structure'
    },
    {
        title: 'Contracts with Suppliers',
        businessStage: 'Start up',
        featuredImage:
            'https://www.designerfactfile.com/uploads/files/1/CourseMaterials/Antonio-Berardi-AW16-FOH-Kris-Mitchell-British-Fashion-Council-Lo-Res1.jpg?w=500&h=500&mode=crop',
        topic: 'Legal',
        category: 'Business Structure'
    },
    {
        title: 'Bribery & Corruption',
        businessStage: 'Start up',
        featuredImage:
            'https://www.designerfactfile.com/uploads/files/1/CourseMaterials/Issa-AW14-Daniel-Sims-British-Fashion-Council-38.JPG?w=500&h=500&mode=crop',
        topic: 'Legal',
        category: 'Business Structure'
    },
    {
        title: 'Trading Terms & Conditions: Wholesale & Online',
        businessStage: 'Start up',
        featuredImage:
            'https://www.designerfactfile.com/uploads/files/1/CourseMaterials/Mulberry-AW16-FOH-Shaun-James-Cox-British-Fashion-Council-Hi-Res5.jpg?w=500&h=500&mode=crop',
        topic: 'Legal',
        category: 'Business Structure'
    },
    {
        title: 'Anti-Counterfeiting & Design Protection',
        businessStage: 'Start up',
        featuredImage:
            'https://www.designerfactfile.com/uploads/files/1/CourseMaterials/Ryan-Lo-AW16-Backstage-Sam-Wilson-British-Fashion-Council-LoRes1.jpg?w=500&h=500&mode=crop',
        topic: 'Legal',
        category: 'Business Structure'
    },
    {
        title: 'Property: Leasing Studio and Retail Space',
        businessStage: 'Start up',
        featuredImage:
            'https://www.designerfactfile.com/uploads/files/1/CourseMaterials/Peter-Pilotto-SS15-Daniel-Sims-British-Fashion-Council-6.jpg?w=500&h=500&mode=crop',
        topic: 'Legal',
        category: 'Business Structure'
    },
    {
        title: 'Reputation Management and Media Training',
        businessStage: 'Entrepreneur',
        featuredImage:
            'https://www.designerfactfile.com/uploads/files/1/CourseMaterials/J-JS-LEE-AW16-Backstage-Sam-Wilson-British-Fashion-Council-Lo-Res2.jpg?w=500&h=500&mode=crop',
        topic: 'Legal',
        category: 'Business Structure'
    },
    {
        title: 'Corporate Structure, Investment and Exit Strategies',
        businessStage: 'Future Brand',
        featuredImage:
            'https://www.designerfactfile.com/uploads/files/1/CourseMaterials/SIBLING-AW16-FOH-Dan-Sims-British-Fashion-Council-Lo-Res1.jpg?w=500&h=500&mode=crop',
        topic: 'Legal',
        category: 'Business Structure'
    },
    {
        title: 'Product Development Strategy',
        businessStage: 'Start up',
        featuredImage:
            'https://www.designerfactfile.com/uploads/files/1/CourseMaterials/Erdem-SS15-Shaun-James-Cox-British-Fashion-Council-3.jpg?w=500&h=500&mode=crop',
        topic: 'Product Development & Range Planning',
        category: 'Product'
    },
    {
        title: 'Wearable Technology',
        businessStage: 'Entrepreneur',
        featuredImage:
            'https://www.designerfactfile.com/uploads/files/1/CourseMaterials/David-Koma-AW16-Backstage-Dan-Sims-British-Fashion-Council-LoRes-1.jpg?w=500&h=500&mode=crop',
        topic: 'Product Development & Range Planning',
        category: 'Product'
    },
    {
        title: 'Basics of Trade Shows',
        businessStage: 'Start up',
        featuredImage:
            'https://www.designerfactfile.com/uploads/files/1/CourseMaterials/Holly-Fulton-AW16-Backstage-Sam-Wilson-British-Fashion-Council-HiRes14.jpg?w=500&h=500&mode=crop',
        topic: 'Wholesale',
        category: 'Sales'
    },
    {
        title: 'Department for International Trade - Export',
        businessStage: 'Start up',
        featuredImage:
            'https://www.designerfactfile.com/uploads/files/1/CourseMaterials/DFF.JPG?w=500&h=500&mode=crop',
        topic: 'Wholesale',
        category: 'Sales'
    },
    {
        title: 'How to Get Noticed by Buyers',
        businessStage: 'Start up',
        featuredImage:
            'https://www.designerfactfile.com/uploads/files/1/CourseMaterials/Simone-Rocha-AW14-Daniel-Sims-British-Fashion-Council-4.jpg?w=500&h=500&mode=crop',
        topic: 'Wholesale',
        category: 'Sales'
    },
    {
        title: 'Strategic Distribution & the Legal Implications',
        businessStage: 'Entrepreneur',
        featuredImage:
            'https://www.designerfactfile.com/uploads/files/1/CourseMaterials/Roksanda-SS15-Kensington-Leverne-British-Fashion-Council-6.jpg?w=500&h=500&mode=crop',
        topic: 'Wholesale',
        category: 'Sales'
    },
    {
        title: 'Deciding when E-commerce is Right for Your Business',
        businessStage: 'Start up',
        featuredImage:
            'https://www.designerfactfile.com/uploads/files/1/CourseMaterials/Holly-Fulton-SS15-Daniel-Sims-British-Fashion-Council-2.jpg?w=500&h=500&mode=crop',
        topic: 'E-commerce',
        category: 'Sales'
    },
    {
        title: 'E-commerce for International Growth',
        businessStage: 'Start up',
        featuredImage:
            'https://www.designerfactfile.com/uploads/files/1/CourseMaterials/Holly-Fulton-SS15-Daniel-Sims-British-Fashion-Council-5.jpg?w=500&h=500&mode=crop',
        topic: 'E-commerce',
        category: 'Sales'
    },
    {
        title: 'Preparing for E-Commerce Success',
        businessStage: 'Start up',
        featuredImage:
            'https://www.designerfactfile.com/images/default-course-material.jpg?w=500&h=500&mode=crop',
        topic: 'E-commerce',
        category: 'Sales'
    },
    {
        title: 'Taking Your Fashion Business Global',
        businessStage: 'Start up',
        featuredImage:
            'https://www.designerfactfile.com/images/default-course-material.jpg?w=500&h=500&mode=crop',
        topic: 'E-commerce',
        category: 'Sales'
    },
    {
        title: 'E-fulfilment Considerations: Warehousing, Pick and Pack, Dispatch and Returns',
        businessStage: 'Entrepreneur',
        featuredImage:
            'https://www.designerfactfile.com/uploads/files/1/CourseMaterials/3Y4B0694_Ryan-Lo_5801.jpg?w=500&h=500&mode=crop',
        topic: 'E-commerce',
        category: 'Sales'
    },
    {
        title: 'Considering Key Shopping Areas',
        businessStage: 'Future Brand',
        featuredImage:
            'https://www.designerfactfile.com/uploads/files/1/CourseMaterials/Shrimps-AW16-Presentation-Shaun-James-Cox-British-Fashion-Council-Lo-Res3.jpg?w=500&h=500&mode=crop',
        topic: 'Retail',
        category: 'Sales'
    },
    {
        title: 'Retail Property: Choosing the Right Location',
        businessStage: 'Future Brand',
        featuredImage:
            'https://www.designerfactfile.com/uploads/files/1/CourseMaterials/Simone-Rocha-SS15-backstage-Shaun-James-Cox-British-Fashion-Council-5.jpg?w=500&h=500&mode=crop',
        topic: 'Retail',
        category: 'Sales'
    },
    {
        title: 'Setting up a Pop-Up Shop',
        businessStage: 'Start up',
        featuredImage:
            'https://www.designerfactfile.com/uploads/files/1/CourseMaterials/Ashley-Williams-SS16-Front-of-House-Sam-Wilson-British-Fashion-Council-Hi-Res24.JPG?w=500&h=500&mode=crop',
        topic: 'Retail',
        category: 'Sales'
    },
    {
        title: 'Smart Ways to Sell Overstock',
        businessStage: 'Start up',
        featuredImage:
            'https://www.designerfactfile.com/uploads/files/1/CourseMaterials/Ashish-SS16-Backstage-Dan-Sims-British-Fashion-Council-Hi-Res21.JPG?w=500&h=500&mode=crop',
        topic: 'Retail',
        category: 'Sales'
    },
    {
        title: 'Finance and Credit from Manufacturers',
        businessStage: 'Start up',
        featuredImage:
            'https://www.designerfactfile.com/images/default-course-material.jpg?w=500&h=500&mode=crop',
        topic: 'Process',
        category: 'Manufacturing'
    },
    {
        title: 'Forecasting Your Costs and Capping Production',
        businessStage: 'Entrepreneur',
        featuredImage:
            'https://www.designerfactfile.com/uploads/files/1/CourseMaterials/Holly-Fulton-SS15-Daniel-Sims-British-Fashion-Council-3.jpg?w=500&h=500&mode=crop',
        topic: 'Process',
        category: 'Manufacturing'
    },
    {
        title: 'Manufacturing Overview',
        businessStage: 'Start up',
        featuredImage:
            'https://www.designerfactfile.com/uploads/files/1/CourseMaterials/Pringle-of-Scotland-SS15-backstage-Daniel-Sims-British-Fashion-Council-6.jpg?w=500&h=500&mode=crop',
        topic: 'Process',
        category: 'Manufacturing'
    },
    {
        title: 'Production Management',
        businessStage: 'Start up',
        featuredImage:
            'https://www.designerfactfile.com/images/default-course-material.jpg?w=500&h=500&mode=crop',
        topic: 'Process',
        category: 'Manufacturing'
    },
    {
        title: 'Sourcing a Manufacturer',
        businessStage: 'Start up',
        featuredImage:
            'https://www.designerfactfile.com/images/default-course-material.jpg?w=500&h=500&mode=crop',
        topic: 'Process',
        category: 'Manufacturing'
    },
    {
        title: 'Working with International Suppliers and Manufacturers',
        businessStage: 'Start up',
        featuredImage:
            'https://www.designerfactfile.com/uploads/files/1/CourseMaterials/House-of-Holland-AW14-Daniel-Sims-British-Fashion-Council-3.jpg?w=500&h=500&mode=crop',
        topic: 'Logistics',
        category: 'Manufacturing'
    },
    {
        title: 'Accountants vs Bookkeepers vs Finance Directors',
        businessStage: 'Start up',
        featuredImage:
            'https://www.designerfactfile.com/images/default-course-material.jpg?w=500&h=500&mode=crop',
        topic: 'Accounting',
        category: 'Finance'
    },
    {
        title: 'Extracting Cash from your Company',
        businessStage: 'Start up',
        featuredImage:
            'https://www.designerfactfile.com/uploads/files/1/CourseMaterials/Temperley-AW16-FOH-Shaun-James-Cox-British-Fashion-Council-Lo-Res4.jpg?w=500&h=500&mode=crop',
        topic: 'Accounting',
        category: 'Finance'
    },
    {
        title: 'Keeping Accounts, Books, VAT & HMRC Up to Date',
        businessStage: 'Start up',
        featuredImage:
            'https://www.designerfactfile.com/uploads/files/1/CourseMaterials/Mary-Katrantzou-AW15-Shaun-James-Cox-British-Fashion-Council-10.jpg?w=500&h=500&mode=crop',
        topic: 'Accounting',
        category: 'Finance'
    },
    {
        title: 'Reporting Requirements: Year End Accounts, VAT, Annual Returns, Corporate Returns',
        businessStage: 'Start up',
        featuredImage:
            'https://www.designerfactfile.com/images/default-course-material.jpg?w=500&h=500&mode=crop',
        topic: 'Accounting',
        category: 'Finance'
    },
    {
        title: 'The Benefits of VAT',
        businessStage: 'Start up',
        featuredImage:
            'https://www.designerfactfile.com/uploads/files/1/CourseMaterials/J.-JS-Lee-SS16-Backstage-Sam-Wilson-British-Fashion-Council-Hi-Res17.jpg?w=500&h=500&mode=crop',
        topic: 'Accounting',
        category: 'Finance'
    },
    {
        title: 'The Importance of Finance Planning',
        businessStage: 'Start up',
        featuredImage:
            'https://www.designerfactfile.com/images/default-course-material.jpg?w=500&h=500&mode=crop',
        topic: 'Accounting',
        category: 'Finance'
    },
    {
        title: 'Understanding and Managing your Cash Flow',
        businessStage: 'Start up',
        featuredImage:
            'https://www.designerfactfile.com/uploads/files/1/CourseMaterials/Nasir-Mazhar-SS15-Daniel-Sims-British-Fashion-Council-3.jpg?w=500&h=500&mode=crop',
        topic: 'Accounting',
        category: 'Finance'
    },
    {
        title: 'Preparing for Audit',
        businessStage: 'Entrepreneur',
        featuredImage:
            'https://www.designerfactfile.com/uploads/files/1/CourseMaterials/Richard-Nicoll-AW14-Daniel-Sims-British-Fashion-Council-1.jpg?w=500&h=500&mode=crop',
        topic: 'Accounting',
        category: 'Finance'
    },
    {
        title: 'Reading Balance Sheets & Profit & Loss Statements',
        businessStage: 'Entrepreneur',
        featuredImage:
            'https://www.designerfactfile.com/uploads/files/1/CourseMaterials/Jonathan-Saunders-AW15-Shaun-James-Cox-British-Fashion-Council-11.jpg?w=500&h=500&mode=crop',
        topic: 'Accounting',
        category: 'Finance'
    },
    {
        title: 'Options for Funding',
        businessStage: 'Explorer',
        featuredImage:
            'https://www.designerfactfile.com/uploads/files/1/CourseMaterials/Roksanda-AW15-backstage-Kensington-Leverne-British-Fashion-Council-7.jpg?w=500&h=500&mode=crop',
        topic: 'Funding & Investment',
        category: 'Finance'
    },
    {
        title: 'Early Stage Funding',
        businessStage: 'Start up',
        featuredImage:
            'https://www.designerfactfile.com/uploads/files/1/CourseMaterials/Peter-Pilotto-AW16-Backstage-Sam-Wilson-British-Fashion-Council-Lo-Res3.jpg?w=500&h=500&mode=crop',
        topic: 'Funding & Investment',
        category: 'Finance'
    },
    {
        title: 'Understanding Investment Options',
        businessStage: 'Entrepreneur',
        featuredImage:
            'https://www.designerfactfile.com/uploads/files/1/CourseMaterials/Holly-Fulton-SS16-Backstage-Eeva-Rinne-British-Fashion-Council-Hi-Res25.jpg?w=500&h=500&mode=crop',
        topic: 'Funding & Investment',
        category: 'Finance'
    },
    {
        title: 'The Fashion and Private Equity Dynamic',
        businessStage: 'Entrepreneur',
        featuredImage:
            'https://www.designerfactfile.com/uploads/files/1/CourseMaterials/Emilia-Wickstead-AW16-Backstage-Eeva-Rinne-British-Fashion-Council-Lo-Res21.jpg?w=500&h=500&mode=crop',
        topic: 'Funding & Investment',
        category: 'Finance'
    },
    {
        title: 'Understanding Bloggers & Digital Influencers',
        businessStage: 'Start up',
        featuredImage:
            'https://www.designerfactfile.com/uploads/files/1/CourseMaterials/Vivienne-Westwood-AW16-Backstage-Sam-Wilson-British-Fashion-Council-Lo-Res9.jpg?w=500&h=500&mode=crop',
        topic: 'Digital',
        category: 'Digital'
    },
    {
        title: 'Using Analytics for Valuable Results',
        businessStage: 'Entrepreneur',
        featuredImage:
            'https://www.designerfactfile.com/uploads/files/1/CourseMaterials/Mother-of-Pearl-AW16-Backstage-Laura-Huhta-British-Fashion-Council-Lo-Res30.jpg?w=500&h=500&mode=crop',
        topic: 'Digital',
        category: 'Digital'
    },
    {
        title: 'Using Social Media for Two-Way Brand Conversations',
        businessStage: 'Start up',
        featuredImage:
            'https://www.designerfactfile.com/uploads/files/1/CourseMaterials/Emilia-Wickstead-SS15-backstage-Daniel-Sims-British-Fashion-Council-3.jpg?w=500&h=500&mode=crop',
        topic: 'Digital',
        category: 'Digital'
    },
    {
        title: 'Considering User Experience: Balancing Creativity and Usability',
        businessStage: 'Entrepreneur',
        featuredImage:
            'https://www.designerfactfile.com/uploads/files/1/CourseMaterials/Nasir-Mazhar-AW14-Dan-Sims-British-Fashion-Council-2.jpg?w=500&h=500&mode=crop',
        topic: 'Digital',
        category: 'Digital'
    },
    {
        title: 'How to Use Display Advertising',
        businessStage: 'Entrepreneur',
        featuredImage:
            'https://www.designerfactfile.com/uploads/files/1/CourseMaterials/Marques-Almeida-AW16-FOH-Kris-Mitchell-British-Fashion-Council-Hi-Res4.jpg?w=500&h=500&mode=crop',
        topic: 'Digital',
        category: 'Digital'
    },
    {
        title: 'Understanding Affiliate Networks',
        businessStage: 'Entrepreneur',
        featuredImage:
            'https://www.designerfactfile.com/uploads/files/1/CourseMaterials/Simone-Rocha-SS15-Daniel-Sims-British-Fashion-Council-4.jpg?w=500&h=500&mode=crop',
        topic: 'Digital',
        category: 'Digital'
    },
    {
        title: 'Developing a Brand Strategy',
        businessStage: 'Start up',
        featuredImage:
            'https://www.designerfactfile.com/uploads/files/1/CourseMaterials/Christopher-Kane-AW15-Dan-Sims-BFC-Hi-Res-17.JPG?w=500&h=500&mode=crop',
        topic: 'Brand',
        category: 'Marketing & Communications'
    },
    {
        title: 'Developing a Content Strategy',
        businessStage: 'Start up',
        featuredImage:
            'https://www.designerfactfile.com/uploads/files/1/CourseMaterials/Holly-Fulton-AW16-Backstage-Sam-Wilson-British-Fashion-Council-HiRes14.jpg?w=500&h=500&mode=crop',
        topic: 'Brand',
        category: 'Marketing & Communications'
    },
    {
        title: 'Understanding Your Competition',
        businessStage: 'Start up',
        featuredImage:
            'https://www.designerfactfile.com/uploads/files/1/CourseMaterials/J.JS-Lee-SS15-Daniel-Sims-British-Fashion-Council-4.jpg?w=500&h=500&mode=crop',
        topic: 'Brand',
        category: 'Marketing & Communications'
    },
    {
        title: 'Building Your Team',
        businessStage: 'Start up',
        featuredImage:
            'https://www.designerfactfile.com/uploads/files/1/CourseMaterials/Mother-of-Pearl-AW16-FOH-Dan-Sims-British-Fashion-Council-Lo-Res4.jpg?w=500&h=500&mode=crop',
        topic: 'HR',
        category: 'Resources & Personal Development'
    },
    {
        title: 'Employees, Freelancers & Interns: A Guide to Employment Law',
        businessStage: 'Start up',
        featuredImage:
            'https://www.designerfactfile.com/uploads/files/1/CourseMaterials/Holly-Fulton-SS16-Backstage-Eeva-Rinne-British-Fashion-Council-Hi-Res36.jpg?w=500&h=500&mode=crop',
        topic: 'HR',
        category: 'Resources & Personal Development'
    },
    {
        title: 'Managing and Motivating Your Team Starts with Yourself',
        businessStage: 'Start up',
        featuredImage:
            'https://www.designerfactfile.com/images/default-course-material.jpg?w=500&h=500&mode=crop',
        topic: 'Personal Development',
        category: 'Resources & Personal Development'
    },
    {
        title: 'Priorities Management',
        businessStage: 'Start up',
        featuredImage:
            'https://www.designerfactfile.com/uploads/files/1/CourseMaterials/Fashion-East-AW16-FOH-Kris-Mitchell-British-Fashion-Council-Lo-Res11.jpg?w=500&h=500&mode=crop',
        topic: 'Personal Development',
        category: 'Resources & Personal Development'
    },
    {
        title: 'Project Management',
        businessStage: 'Start up',
        featuredImage:
            'https://www.designerfactfile.com/uploads/files/1/CourseMaterials/David-Korma-AW16-FOH-Dan-Sims-British-Fashion-Council-Lo-Res2.jpg?w=500&h=500&mode=crop',
        topic: 'Personal Development',
        category: 'Resources & Personal Development'
    },
    {
        title: 'A Green Approach to Working Practice',
        businessStage: 'Start up',
        featuredImage:
            'https://www.designerfactfile.com/images/default-course-material.jpg?w=500&h=500&mode=crop',
        topic: 'Sustainability',
        category: 'Positive Fashion'
    },
    {
        title: 'A Guide to Chemical Compliance',
        businessStage: 'Start up',
        featuredImage:
            'https://www.designerfactfile.com/uploads/files/1/CourseMaterials/Faustine-Steinmetz-AW16-Presentation-Kensington-Leverne-British-Fashion-Council-HiRes3.jpg?w=500&h=500&mode=crop',
        topic: 'Sustainability',
        category: 'Positive Fashion'
    },
    {
        title: 'How to Get Started as a Green Business',
        businessStage: 'Start up',
        featuredImage:
            'https://www.designerfactfile.com/images/default-course-material.jpg?w=500&h=500&mode=crop',
        topic: 'Sustainability',
        category: 'Positive Fashion'
    },
    {
        title: 'How to Talk Sustainability with Your Customers',
        businessStage: 'Start up',
        featuredImage:
            'https://www.designerfactfile.com/images/default-course-material.jpg?w=500&h=500&mode=crop',
        topic: 'Sustainability',
        category: 'Positive Fashion'
    },
    {
        title: 'The GCC Principles',
        businessStage: 'Start up',
        featuredImage:
            'https://www.designerfactfile.com/images/default-course-material.jpg?w=500&h=500&mode=crop',
        topic: 'Sustainability',
        category: 'Positive Fashion'
    },
    {
        title: 'Why Knowing Your Energy Footprint Matters',
        businessStage: 'Start up',
        featuredImage:
            'https://www.designerfactfile.com/images/default-course-material.jpg?w=500&h=500&mode=crop',
        topic: 'Sustainability',
        category: 'Positive Fashion'
    },
    {
        title: 'Ethical Trading',
        businessStage: 'Start up',
        featuredImage:
            'https://www.designerfactfile.com/uploads/files/1/CourseMaterials/Faustine-Steinmetz-SS16-Presentaion-Shaun-James-Cox-British-Fashion-Council-Hi-Res25.jpg?w=500&h=500&mode=crop',
        topic: 'Sustainability',
        category: 'Positive Fashion'
    },
    {
        title: 'Best Practice Guide for Model Agencies',
        businessStage: 'Start up',
        featuredImage:
            'https://www.designerfactfile.com/images/default-course-material.jpg?w=500&h=500&mode=crop',
        topic: 'Diversity & Model Health',
        category: 'Positive Fashion'
    },
    {
        title: 'Local Manufacturing and Craftsmanship',
        businessStage: 'Start up',
        featuredImage:
            'https://www.designerfactfile.com/images/default-course-material.jpg?w=500&h=500&mode=crop',
        topic: 'Local Manufacturing & Craftsmanship',
        category: 'Positive Fashion'
    },
    {
        title: 'Sourcing Best Practices: Key Questions to Ask',
        businessStage: 'Start up',
        featuredImage:
            'https://www.designerfactfile.com/images/default-course-material.jpg?w=500&h=500&mode=crop',
        topic: 'Local Manufacturing & Craftsmanship',
        category: 'Positive Fashion'
    }
];

const formatEntryFields = (fields, assetId, businessStages, topicAndCategories) => {
    const formattedFields = {};

    map(fields, (value, key) => {
        if (key === 'businessStage') {
            const id = get(find(businessStages, {title: value}), 'id');

            formattedFields[key] = {
                [config.locale]: [
                    {
                        sys: {
                            type: 'Link',
                            linkType: 'Entry',
                            id
                        }
                    }
                ]
            };
        } else if (key === 'category') {
            const id = get(find(topicAndCategories, {type: 'Category', title: value}), 'id');

            formattedFields[key] = {
                [config.locale]: {
                    sys: {
                        type: 'Link',
                        linkType: 'Entry',
                        id
                    }
                }
            };
        } else if (key === 'topic') {
            const id = get(find(topicAndCategories, {type: 'Topic', title: value}), 'id');

            formattedFields[key] = {
                [config.locale]: {
                    sys: {
                        type: 'Link',
                        linkType: 'Entry',
                        id
                    }
                }
            };
        } else if (key === 'featuredImage') {
            formattedFields[key] = {
                [config.locale]: {
                    sys: {
                        type: 'Link',
                        linkType: 'Asset',
                        id: assetId
                    }
                }
            };
        } else {
            formattedFields[key] = {[config.locale]: value};
        }
    });

    return formattedFields;
};

const getTopicAndCategories = async environment => {
    return new Promise(resolve => {
        environment.getEntries({content_type: 'contentType'}).then(entries => {
            const formattedEntries = entries.items.map(entry => ({
                type: entry.fields.type[config.locale],
                title: entry.fields.title[config.locale],
                id: entry.sys.id
            }));

            resolve(formattedEntries);
        });
    });
};

const getBusinessStages = async environment => {
    return new Promise(resolve => {
        environment.getEntries({content_type: 'businessStage'}).then(entries => {
            const formattedEntries = entries.items.map(entry => ({
                title: entry.fields.title[config.locale],
                id: entry.sys.id
            }));

            resolve(formattedEntries);
        });
    });
};

const createEntries = async environment => {
    const categoriesAndTopics = await getTopicAndCategories(environment);
    const businessStages = await getBusinessStages(environment);
    const ids = dataToImport.map(async entry => {
        return new Promise(resolve => {
            environment
                .createAsset({
                    fields: {
                        title: {[config.locale]: entry.title},
                        file: {
                            [config.locale]: {
                                contentType: 'image/jpeg',
                                fileName: `${kebabCase(entry.title)}.jpg`,
                                upload: head(split(entry.featuredImage, '?'))
                            }
                        }
                    }
                })
                .then(asset => asset.processForAllLocales())
                .then(asset => asset.publish())
                .then(asset => {
                    environment
                        .createEntry('designerFactFile', {
                            fields: formatEntryFields(
                                entry,
                                asset.sys.id,
                                businessStages,
                                categoriesAndTopics
                            )
                        })
                        .then(newEntry => entry.publish())
                        .then(newEntry => resolve(newEntry.sys.id));
                });
        });
    });

    return Promise.all(ids);
};

const importData = async environment => {
    console.log('Starting import');

    const eventIds = await createEntries(environment);
    eventIds.map(eventId => console.log(`Event created: ${eventId}`));
};

client
    .getSpace(process.env.CONTENTFUL_SPACE_ID)
    .then(space => space.getEnvironment(process.env.CONTENTFUL_ENVIRONMENT_ID))
    .then(importData);

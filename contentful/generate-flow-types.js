/**
 * @prettier
 */
require('dotenv').config();
const contentful = require('contentful-management');
const camelCase = require('lodash/camelCase');
const upperFirst = require('lodash/upperFirst');

const client = contentful.createClient({
    accessToken: process.env.CONTENTFUL_MANAGEMENT_ACCESS_TOKEN
});

const getFieldType = type => {
    if (type === 'Symbol' || type === 'Link' || type === 'Text') {
        return 'string';
    } else if (type === 'Array') {
        return 'Array<Object>';
    } else {
        return '*';
    }
};

client
    .getSpace(process.env.CONTENTFUL_SPACE_ID)
    .then(space => space.getEnvironment(process.env.CONTENTFUL_ENVIRONMENT_ID))
    .then(environment => environment.getContentTypes())
    .then(response => {
        if (response.items.length) {
            let fileContent = '';

            response.items.map(item => {
                fileContent = `${fileContent}

                type Contentful${upperFirst(camelCase(item.name))} = {
                    ${item.fields
                        .map(field => {
                            return `${field.id}?: ${getFieldType(field.type)}`;
                        })
                        .join(',\n')}
                };`;
            });

            console.log(fileContent);
        }
    })
    .catch(console.error);

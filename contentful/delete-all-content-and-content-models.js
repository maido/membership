require('dotenv').config();
const contentful = require('contentful-management');

const client = contentful.createClient({
    accessToken: process.env.CONTENTFUL_MANAGEMENT_ACCESS_TOKEN
});

const deleteAllEntries = environment => {
    return new Promise(resolveAll => {
        environment.getEntries({limit: 400}).then(entries => {
            if (entries.items.length > 0) {
                const itemsToDelete = entries.items.map(item => {
                    return new Promise(resolve => {
                        environment.getEntry(item.sys.id).then(entry => {
                            if (entry.isPublished()) {
                                entry.unpublish().then(() => entry.delete().then(resolve));
                            } else {
                                entry.delete().then(resolve);
                            }
                        });
                    });
                });

                Promise.all(itemsToDelete).then(() => {
                    console.log('All entries deleted');
                    resolveAll();
                });
            } else {
                console.log('No entries to delete');
                resolveAll();
            }
        });
    });
};

const deleteAllAssets = environment => {
    return new Promise(resolveAll => {
        environment.getAssets().then(assets => {
            if (assets.items.length > 0) {
                const assetsToDelete = assets.items.map(item => {
                    return new Promise(resolve => {
                        environment.getAsset(item.sys.id).then(asset => {
                            if (asset.isPublished()) {
                                asset.unpublish().then(() => asset.delete().then(resolve));
                            } else {
                                asset.delete().then(resolve);
                            }
                        });
                    });
                });

                Promise.all(assetsToDelete).then(() => {
                    console.log('All assets deleted');
                    resolveAll();
                });
            } else {
                console.log('No assets to delete');
                resolveAll();
            }
        });
    });
};

const deleteAllContentTypes = environment => {
    return new Promise(resolveAll => {
        environment.getContentTypes().then(contentTypes => {
            if (contentTypes.items.length > 0) {
                const contentTypesToDelete = contentTypes.items.map(item => {
                    return new Promise(resolve => {
                        environment.getContentType(item.sys.id).then(contentType => {
                            if (contentType.isPublished()) {
                                contentType
                                    .unpublish()
                                    .then(() => contentType.delete().then(resolve));
                            } else {
                                contentType.delete().then(resolve);
                            }
                        });
                    });
                });

                Promise.all(contentTypesToDelete).then(() => {
                    console.log('All content types deleted');
                    resolveAll();
                });
            } else {
                console.log('No content types to delete');
                resolveAll();
            }
        });
    });
};

client
    .getSpace(process.env.CONTENTFUL_SPACE_ID)
    .then(space => space.getEnvironment(process.env.CONTENTFUL_ENVIRONMENT_ID))
    .then(environment => {
        deleteAllEntries(environment)
            .then(() => deleteAllAssets(environment))
            .then(() => deleteAllContentTypes(environment));
    });

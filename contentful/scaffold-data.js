require('dotenv').config();
const fs = require('fs');
const contentful = require('contentful-management');
const find = require('lodash/find');
const map = require('lodash/map');
const config = require('./config');

const client = contentful.createClient({
    accessToken: process.env.CONTENTFUL_MANAGEMENT_ACCESS_TOKEN
});

const updateLocale = async environment => {
    return new Promise(resolve => {
        environment.getLocales().then(locales => {
            const defaultLocale = find(locales.items, {default: true});

            if (defaultLocale && defaultLocale.code !== config.locale) {
                defaultLocale.name = 'English - UK';
                defaultLocale.code = config.locale;

                defaultLocale.update().then(locale => resolve(locale.sys.id));
            } else {
                resolve(defaultLocale.sys.id);
            }
        });
    });
};

const createAsset = async (environment, fields) => {
    return new Promise(resolve => {
        environment
            .createAssetFromFiles({fields})
            .then(asset => asset.processForAllLocales())
            .then(asset => asset.publish())
            .then(asset => resolve(asset.sys.id));
    });
};

const createImageAssets = async environment => {
    const assets = [
        {
            title: {[config.locale]: 'BFC square logo'},
            file: {
                [config.locale]: {
                    contentType: 'image/jpeg',
                    fileName: 'bfc-logo-square.jpg',
                    file: fs.createReadStream('./contentful/assets/bfc-logo-square.jpg')
                }
            }
        },
        {
            title: {[config.locale]: 'BFC hero'},
            file: {
                [config.locale]: {
                    contentType: 'image/jpeg',
                    fileName: 'bfc-hero.jpg',
                    file: fs.createReadStream('./contentful/assets/bfc-hero.jpg')
                }
            }
        }
    ];
    const ids = assets.map(async asset => createAsset(environment, asset));

    return Promise.all(ids);
};

const formatEntryFields = fields => {
    const formattedFields = {};

    map(fields, (value, key) => {
        formattedFields[key] = {[config.locale]: value};
    });

    return formattedFields;
};

const createEntry = async (environment, contentTypeId, fields) => {
    return new Promise(resolve => {
        environment
            .createEntry(contentTypeId, {fields: formatEntryFields(fields)})
            .then(entry => entry.publish())
            .then(entry => resolve(entry.sys.id));
    });
};

const createAddress = async environment => {
    const id = await createEntry(environment, 'address', {
        name: 'British Fashion Council',
        street: 'Somerset House, South Wing, Strand',
        city: 'London',
        country: 'United Kingdom',
        postcode: 'WC2R 1LA'
    });

    return id;
};

const createContactDetails = async environment => {
    const contacts = [
        {
            title: 'British Fashion Council - Media',
            firstName: 'Bertrand',
            lastName: 'Dark',
            email: 'bertrand.dark@britishfashioncouncil.com',
            telephone: '+44 (0) 20 7759 1961'
        },
        {
            title: 'British Fashion Council - Director of Comms',
            firstName: 'Clara',
            lastName: 'Mercer',
            email: 'clara.mercer@britishfashioncouncil.com'
        },
        {
            title: 'British Fashion Council - Senior Digital Marketing',
            firstName: 'Molly',
            lastName: 'Scott-Wells',
            email: 'molly.scott-wells@britishfashioncouncil.com',
            telephone: '+44 (0) 20 7759 1956'
        },
        {
            title: 'British Fashion Council - Senior Comms',
            firstName: 'Michalis',
            lastName: 'Zodiatis',
            email: 'michalis.zodiatis@britishfashioncouncil.com',
            telephone: '+44 (0) 20 7759 1989'
        }
    ];
    const ids = contacts.map(async contact => createEntry(environment, 'contactDetails', contact));

    return Promise.all(ids);
};

const createBusinessStages = async environment => {
    const contacts = ['Explorer', 'Start up', 'Entrepeneur', 'Future Brand'];
    const ids = contacts.map(async contact =>
        createEntry(environment, 'businessStage', {
            title: contact
        })
    );

    return Promise.all(ids);
};

const createCompany = async (environment, addressId, contactDetailsId) => {
    const id = await createEntry(environment, 'company', {
        title: 'British Fashion Council',
        slug: 'british-fashion-council',
        address: {
            sys: {
                type: 'Link',
                linkType: 'Entry',
                id: addressId
            }
        },
        contactDetails: {
            sys: {
                type: 'Link',
                linkType: 'Entry',
                id: contactDetailsId
            }
        },
        websiteUrl: 'https://www.britishfashioncouncil.co.uk',
        facebookUrl: 'https://en-gb.facebook.com/britishfashioncouncil/',
        twitterUrl: 'https://twitter.com/BFC',
        youTubeUrl: 'https://www.youtube.com/channel/UCOTIeIEXL9LkkvoVKZDNr3A'
    });

    return id;
};

const createPerson = async (environment, addressId, contactDetailsIds) => {
    const id = await createEntry(environment, 'person', {
        firstName: 'Christopher',
        lastName: 'Kane',
        isInMemberNetwork: true,
        city: 'London',
        contacts: contactDetailsIds.map(contactDetailsId => ({
            sys: {
                type: 'Link',
                linkType: 'Entry',
                id: contactDetailsId
            }
        })),
        websiteUrl: 'https://www.christopherkane.com/',
        facebookUrl: 'https://www.facebook.com/ChristopherKaneOfficial/',
        twitterUrl: '',
        youTubeUrl: 'https://www.youtube.com/channel/UC8e0-Djdvo-sxPDmpStEq4g'
    });

    return id;
};

const createTopics = async environment => {
    const topics = [
        'Business Strategy',
        'Legal',
        'Accounting',
        'Product Development',
        'Costing & pricing',
        'Buyers',
        'Channels',
        'E-commerce',
        'Retail',
        'Process',
        'Logistics',
        'Funding & Investment',
        'Cash flow',
        'Brand',
        'Digital',
        'HR',
        'Personal Development',
        'Sustainability',
        'Diversity & Model Health',
        'Local Manufacturing & Craftsmanship'
    ];
    const ids = topics.map(async topic =>
        createEntry(environment, 'contentType', {
            type: 'Topic',
            title: topic
        })
    );

    return Promise.all(ids);
};

const createCategories = async environment => {
    const categories = [
        'Business Structure',
        'Product',
        'Sales',
        'Manufacturing',
        'Finance',
        'Marketing & Communications',
        'Resources & Personal Development',
        'Positive Fashion'
    ];
    const ids = categories.map(async category =>
        createEntry(environment, 'contentType', {
            type: 'Category',
            title: category
        })
    );

    return Promise.all(ids);
};

const scaffoldData = async environment => {
    console.log('Starting scaffold');

    const localeId = await updateLocale(environment);
    console.log(`Locale updated: ${localeId}`);

    const imageAssetIds = await createImageAssets(environment);
    imageAssetIds.map(imageAssetId => console.log(`Asset created: ${imageAssetId}`));

    const topicIds = await createTopics(environment);
    topicIds.map(topicId => console.log(`Topic created: ${topicId}`));

    const categoryIds = await createCategories(environment);
    categoryIds.map(categoryId => console.log(`Category created: ${categoryId}`));

    const businessStageIds = await createBusinessStages(environment);
    businessStageIds.map(businessStageId =>
        console.log(`Business Stage created: ${businessStageId}`)
    );

    const addressId = await createAddress(environment);
    console.log(`Address created: ${addressId}`);

    const contactDetailsIds = await createContactDetails(environment);
    contactDetailsIds.map(contactDetailsId =>
        console.log(`Contact Details created: ${contactDetailsId}`)
    );

    const companyId = await createCompany(environment, addressId, contactDetailsIds[0]);
    console.log(`Company created: ${companyId}`);

    const personId = await createPerson(environment, addressId, contactDetailsIds, companyId);
    console.log(`Person created: ${personId}`);
};

client
    .getSpace(process.env.CONTENTFUL_SPACE_ID)
    .then(space => space.getEnvironment(process.env.CONTENTFUL_ENVIRONMENT_ID))
    .then(scaffoldData);

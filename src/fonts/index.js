/**
 * @prettier
 */
import neuzeitGroRegularWoff from './NeuzeitGro-Regular.woff';
import neuzeitGroRegularWoff2 from './NeuzeitGro-Regular.woff2';
import neuzeitGroBoldWoff from './NeuzeitGro-Bold.woff';
import neuzeitGroBoldWoff2 from './NeuzeitGro-Bold.woff2';

export {neuzeitGroRegularWoff, neuzeitGroRegularWoff2, neuzeitGroBoldWoff, neuzeitGroBoldWoff2};

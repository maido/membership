/**
 * @prettier
 * @flow
 */
import React, {Fragment} from 'react';
import {graphql, navigate} from 'gatsby';
import {css} from '@emotion/core';
import get from 'lodash/get';
import dayjs from 'dayjs';
import AnimateWhenVisible from '../components/AnimateWhenVisible';
import Card from '../components/Card';
import Container from '../components/Container';
import LayoutContainer from '../components/LayoutContainer';
import MasonryLayout from '../components/MasonryLayout';
import Pagination from '../components/Pagination';
import * as UI from '../components/UI/styles';

type Props = {
    data: Object,
    pageContext: Object
};

const PAGINATION_SIZE = 20;

const EditorialPaginationTemplate = ({data, pageContext}: Props) => {
    const content = get(data, 'editorial.edges', []).map(i => i.node);

    const handlePageChange = page =>
        navigate(`/${pageContext.urlPath}/page/${page}`, {replace: true});

    return (
        <Fragment>
            <LayoutContainer>
                {content.length > 0 ? (
                    <MasonryLayout
                        columns={3}
                        defaultColumns={2}
                        items={content.map(item => ({
                            key: item.title,
                            title: item.title,
                            component: (
                                <Card
                                    image={item.featuredImage ? item.featuredImage.fluid : null}
                                    inGrid={true}
                                    introduction={
                                        item.introduction ? item.introduction.introduction : null
                                    }
                                    isFeatured={item.isFeatured}
                                    memberType={item.memberType}
                                    tag={item.date ? dayjs(item.date).format('DD/MM/YYYY') : null}
                                    title={item.title}
                                    to={`/${pageContext.urlPath}/${item.slug}`}
                                />
                            )
                        }))}
                    />
                ) : (
                    <div
                        css={css`
                            align-items: center;
                            display: flex;
                            min-height: 100vh;
                            justify-content: center;
                            text-align: center;
                            width: 100%;
                        `}
                    >
                        <Container>
                            <AnimateWhenVisible watch={false}>
                                <UI.Heading3>Sorry, no content found</UI.Heading3>
                                <p>There are no posts to show right now. Check back soon!</p>
                            </AnimateWhenVisible>
                        </Container>
                    </div>
                )}

                {pageContext.totalPosts > 0 && (
                    <Pagination
                        activePage={pageContext.currentPage}
                        currentItems={content.length}
                        itemsCountPerPage={PAGINATION_SIZE}
                        onChange={handlePageChange}
                        pageRangeDisplayed={5}
                        skip={pageContext.skip}
                        totalItemsCount={pageContext.totalPosts}
                    />
                )}
            </LayoutContainer>
        </Fragment>
    );
};

export const query = graphql`
    query editorialListingQuery($limit: Int!, $skip: Int!, $pageType: String!) {
        editorial: allContentfulEditorial(
            filter: {pageType: {eq: $pageType}}
            sort: {fields: [date], order: DESC}
            limit: $limit
            skip: $skip
        ) {
            edges {
                node {
                    title
                    slug
                    memberType
                    date(formatString: "YYYY/MM/DD")
                    isFeatured
                    featuredImage {
                        fluid: sizes(maxWidth: 600) {
                            ...GatsbyContentfulSizes
                        }
                    }
                }
            }
        }
        allEditorial: allContentfulEditorial(
            filter: {pageType: {eq: $pageType}}
            sort: {fields: [date], order: DESC}
        ) {
            edges {
                node {
                    title
                    slug
                    date(formatString: "YYYY/MM/DD")
                    isFeatured
                    featuredImage {
                        fluid: sizes(maxWidth: 600) {
                            ...GatsbyContentfulSizes
                        }
                    }
                }
            }
        }
    }
`;

export default EditorialPaginationTemplate;

/**
 * @prettier
 * @flow
 */
import React, {useState} from 'react';
import {graphql, navigate} from 'gatsby';
import MiniSearch from 'minisearch';
import {css} from '@emotion/core';
import get from 'lodash/get';
import kebabCase from 'lodash/kebabCase';
import AnimateWhenVisible from '../components/AnimateWhenVisible';
import FixedHeader from '../components/FixedHeader';
import LayoutContainer from '../components/LayoutContainer';
import ListCard from '../components/ListCard';
import Pagination from '../components/Pagination';
import Tabs from '../components/Tabs';
import TextInput from '../components/TextInput';
import {getPersonsTagline} from '../globals/functions';
import {breakpoints} from '../globals/variables';
import * as UI from '../components/UI/styles';

type Props = {
    data: {
        memberNetworks: {
            edges: Array<{node: ContentfulEditorial}>
        }
    },
    pageContext: Object
};

const MemberNetworkPaginationTemplate = ({data, pageContext}: Props) => {
    const content = get(data, 'memberNetworks.edges', []).map(item => item.node);
    const allContentList = get(data, 'allMemberNetworks.edges', []).map((item, index) => ({
        ...item.node,
        ...{id: index}
    }));
    const memberNetworksById = allContentList.reduce((byId, item) => {
        byId[item.id] = item;
        return byId;
    }, {});

    const [filter, setFilter] = useState('');
    const [filteredContent, setFilteredContent] = useState([]);

    const miniSearch = new MiniSearch({
        fields: ['title', 'leadContactFirstName', 'leadContactLastName']
    });
    miniSearch.addAll(allContentList);

    const handleTextInputChange = (event: KeyboardEvent) => {
        const $target = ((event.target: any): HTMLInputElement);

        setFilter($target.value);
        setFilteredContent(
            miniSearch
                .search($target.value, {fuzzy: 0.3, prefix: true})
                .map(({id}) => memberNetworksById[id])
        );
    };

    const resetFilter = (event?: Event) => {
        if (event) {
            event.preventDefault();
        }

        setFilter('');
        setFilteredContent([]);
    };

    const handlePageChange = page => {
        navigate(`/member-network/${kebabCase(pageContext.companyType)}s/page/${page}`, {
            replace: true
        });
    };

    const handleCompanyTypeChange = companyType => {
        navigate(`/member-network/${kebabCase(companyType)}`, {replace: true});
    };

    const contentList = filter !== '' ? filteredContent : content;

    return (
        <LayoutContainer>
            <FixedHeader>
                <div
                    css={[
                        css`
                            display: flex;
                            flex-direction: column;
                            flex-grow: 1;
                            justify-content: center;
                        `,
                        UI.responsiveHorizontalPadding
                    ]}
                >
                    {filter !== '' && (
                        <UI.FadeInUp>
                            <UI.Label>Searching names</UI.Label>
                        </UI.FadeInUp>
                    )}
                    <UI.FadeInUp>
                        <TextInput
                            handleChange={handleTextInputChange}
                            handleClear={resetFilter}
                            placeholder="Search by name..."
                            value={filter}
                        />
                    </UI.FadeInUp>
                </div>

                <UI.FadeInUp delay={200} css={UI.responsiveHorizontalPadding}>
                    <Tabs
                        items={pageContext.allCompanyTypes}
                        handleTabClick={handleCompanyTypeChange}
                        activeTab={`${pageContext.companyType}s`}
                    />
                </UI.FadeInUp>
            </FixedHeader>

            {contentList.length > 0 &&
                contentList.map((item, index) => (
                    <AnimateWhenVisible delay={index * 100} key={item.title}>
                        <ListCard
                            description={getPersonsTagline(item)}
                            image={item.image ? item.image.fluid : ''}
                            imageType="fluid"
                            imageLogo={true}
                            link={item.websiteUrl}
                            size="medium"
                            title={item.title}
                            to={`/member-network/${kebabCase(item.companyType.title)}s/${kebabCase(
                                item.title
                            )}`}
                        />
                    </AnimateWhenVisible>
                ))}

            {contentList.length === 0 && filter === '' && (
                <UI.FadeInUp>
                    <ListCard
                        title=""
                        description={`Sorry, there are no ${pageContext.companyType} members yet.`}
                        handleClick={resetFilter}
                    />
                </UI.FadeInUp>
            )}

            {contentList.length === 0 && filter !== '' && (
                <UI.FadeInUp>
                    <ListCard
                        title="No results"
                        description="Sorry, we couldn't find a member by that name."
                        handleClick={resetFilter}
                    />
                </UI.FadeInUp>
            )}

            {filter === '' && contentList.length > 0 && (
                <Pagination
                    activePage={pageContext.currentPage}
                    currentItems={contentList.length}
                    itemsCountPerPage={30}
                    onChange={handlePageChange}
                    pageRangeDisplayed={5}
                    skip={pageContext.skip}
                    totalItemsCount={
                        allContentList.filter(c => c.companyType.title === pageContext.companyType)
                            .length
                    }
                    type="members"
                />
            )}
        </LayoutContainer>
    );
};

export const query = graphql`
    query($companyType: String!, $limit: Int!, $skip: Int!) {
        memberNetworks: allContentfulCompany(
            filter: {isInMemberNetwork: {eq: true}, companyType: {title: {eq: $companyType}}}
            sort: {fields: [title], order: ASC}
            limit: $limit
            skip: $skip
        ) {
            edges {
                node {
                    title
                    companyType {
                        title
                    }
                    image {
                        fluid: sizes(maxWidth: 300) {
                            ...GatsbyContentfulSizes
                        }
                    }
                    content {
                        content
                    }
                    tagline
                    websiteUrl
                }
            }
        }
        allMemberNetworks: allContentfulCompany(
            filter: {isInMemberNetwork: {eq: true}, companyType: {title: {ne: null}}}
        ) {
            edges {
                node {
                    title
                    companyType {
                        title
                    }
                    image {
                        fluid: sizes(maxWidth: 300) {
                            ...GatsbyContentfulSizes
                        }
                    }
                    content {
                        content
                    }
                    tagline
                    websiteUrl
                }
            }
        }
    }
`;

export default MemberNetworkPaginationTemplate;

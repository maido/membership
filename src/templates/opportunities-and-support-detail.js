/**
 * @prettier
 * @flow
 */
import React from 'react';
import {Link, graphql} from 'gatsby';
import get from 'lodash/get';
import dayjs from 'dayjs';
import AnimateWhenVisible from '../components/AnimateWhenVisible';
import Container from '../components/Container';
import DownloadsList from '../components/DownloadsList';
import Editorial from '../components/Editorial';
import LayoutContainer from '../components/LayoutContainer';
import PageName from '../components/PageName';
import SEO from '../components/SEO';
import * as UI from '../components/UI/styles';

type Props = {data: Object};

const OpportunitiesAndSupportDetailTemplate = ({data}: Props) => {
    const content = get(data, 'opportunitiesAndSupport');
    const pageType = get(content, 'pageType');

    return (
        <LayoutContainer>
            <SEO description="" image="" title={content.title} />

            <PageName title="Opportunities & Support" />

            <Editorial
                content={get(content, 'content.content')}
                headerContent={
                    pageType && (
                        <UI.FadeInUp>
                            <UI.HorizontalList>
                                {(content.memberType || pageType) && (
                                    <>
                                        {content.memberType && (
                                            <li>
                                                <UI.MemberTypeBadge>
                                                    {content.memberType}
                                                </UI.MemberTypeBadge>
                                            </li>
                                        )}
                                        <li>
                                            <UI.Label>
                                                {dayjs(get(content, 'date')).format('D MMMM, YYYY')}
                                            </UI.Label>
                                        </li>
                                        <li>
                                            <Link
                                                to={`/opportunities-and-support/?type=${encodeURIComponent(
                                                    pageType
                                                )}`}
                                                css={[UI.label, UI.primaryText]}
                                            >
                                                {pageType}
                                            </Link>
                                        </li>
                                    </>
                                )}
                            </UI.HorizontalList>
                        </UI.FadeInUp>
                    )
                }
                heroImage={get(content, 'featuredImage')}
                title={get(content, 'title')}
                introduction={get(content, 'introduction.introduction')}
                topic={get(content, 'topic.title')}
            />

            {content.downloads && (
                <Container>
                    <AnimateWhenVisible>
                        <DownloadsList downloads={content.downloads} />
                    </AnimateWhenVisible>
                </Container>
            )}
        </LayoutContainer>
    );
};

export const query = graphql`
    query($id: String!) {
        opportunitiesAndSupport: contentfulOpportunitiesAndSupport(id: {eq: $id}) {
            title
            slug
            memberType
            date(formatString: "YYYY/MM/DD")
            introduction {
                introduction
            }
            content {
                content
            }
            featuredImage {
                fluid: sizes(maxWidth: 1000) {
                    ...GatsbyContentfulSizes
                }
            }
            pageType
        }
    }
`;

export default OpportunitiesAndSupportDetailTemplate;

/**
 * @prettier
 * @flow
 */
import React from 'react';
import {graphql} from 'gatsby';
import get from 'lodash/get';
import dayjs from 'dayjs';
import AnimateWhenVisible from '../components/AnimateWhenVisible';
import Container from '../components/Container';
import DownloadsList from '../components/DownloadsList';
import Editorial from '../components/Editorial';
import LayoutContainer from '../components/LayoutContainer';
import PageName from '../components/PageName';
import SEO from '../components/SEO';
import * as UI from '../components/UI/styles';

type Props = {
    data: Object,
    pageContext: Object
};

const EditorialDetailTemplate = ({data, pageContext}: Props) => {
    const content = get(data, 'editorial');

    let author;

    if (content.author) {
        author = {
            description: get(content, 'author.tagline'),
            image: get(content, 'author.image'),
            title: get(content, 'author.title')
        };
    }

    return (
        <LayoutContainer>
            <SEO
                description=""
                image={get(content.featuredImage, 'large.src')}
                title={content.title}
            />

            <PageName title={pageContext.pageType} />

            <Editorial
                author={author}
                headerContent={
                    (content.memberType || content.date) && (
                        <UI.FadeInUp>
                            <UI.HorizontalList blockAtMobile={true} flex={true}>
                                {content.memberType && (
                                    <li>
                                        <UI.MemberTypeBadge>
                                            {content.memberType}
                                        </UI.MemberTypeBadge>
                                    </li>
                                )}
                                {content.date && (
                                    <li>
                                        <UI.Label>
                                            {dayjs(content.date).format('D MMMM, YYYY')}
                                        </UI.Label>
                                    </li>
                                )}
                            </UI.HorizontalList>
                        </UI.FadeInUp>
                    )
                }
                title={get(content, 'title')}
                introduction={get(content, 'introduction.introduction')}
                content={get(content, 'content.content')}
                heroImage={get(content, 'featuredImage')}
                youTubeVideoUrl={get(content, 'youTubeVideoUrl')}
            />

            {content.downloads && (
                <Container>
                    <AnimateWhenVisible>
                        <DownloadsList downloads={content.downloads} />
                    </AnimateWhenVisible>
                </Container>
            )}
        </LayoutContainer>
    );
};

export const query = graphql`
    query($id: String!) {
        editorial: contentfulEditorial(id: {eq: $id}) {
            title
            slug
            memberType
            date(formatString: "YYYY/MM/DD")
            introduction {
                introduction
            }
            content {
                content
            }
            featuredImage {
                fluid: sizes(maxWidth: 1000) {
                    ...GatsbyContentfulSizes
                }
            }
            youTubeVideoUrl
            downloads {
                title
                file {
                    fileName
                    url
                    details {
                        size
                    }
                    contentType
                }
            }
        }
    }
`;

export default EditorialDetailTemplate;

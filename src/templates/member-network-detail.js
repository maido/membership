/**
 * @prettier
 * @flow
 */
import React, {Fragment} from 'react';
import {graphql} from 'gatsby';
import Image from 'gatsby-image';
import get from 'lodash/get';
import ReactMarkdown from 'react-markdown';
import Address from '../components/Address';
import AnimateWhenVisible from '../components/AnimateWhenVisible';
import Container from '../components/Container';
import CompanyTypeLogo from '../components/CompanyTypeLogo';
import IconLinkList from '../components/IconLinkList';
import LayoutContainer from '../components/LayoutContainer';
import DetailTemplateHeader from '../components/DetailTemplateHeader';
import PageName from '../components/PageName';
import SEO from '../components/SEO';
import * as UI from '../components/UI/styles';

type Props = {data: Object};

const getLeadContact = content => {
    if (content.leadContactFirstName && content.leadContactLastName) {
        return {
            title: 'Lead Contact',
            firstName: content.leadContactFirstName,
            lastName: content.leadContactLastName,
            role: content.leadContactRole,
            image: content.leadContactImage
        };
    }
};

const MemberNetworkDetailTemplate = ({data}: Props) => {
    const content = get(data, 'memberNetwork');

    if (!content) {
        return <div />;
    }

    const contacts = get(content, 'contacts', []) || [];
    const leadContact = getLeadContact(content);
    const allContacts = [...contacts, leadContact].filter(c => c);

    return (
        <LayoutContainer>
            <SEO description={get(content, 'content.content')} image="" title={content.title} />

            <PageName title="Member Network" />

            <Container>
                <DetailTemplateHeader
                    name={content.title}
                    image={content.image ? content.image.fluid : ''}
                    websiteUrl={content.websiteUrl}
                    instagramUrl={content.instagramUrl}
                    twitterUrl={content.twitterUrl}
                    facebookUrl={content.facebookUrl}
                    youtubeUrl={content.youTubeUrl}
                >
                    {content.companyType && content.companyType.title && (
                        <UI.FadeIn delay={800}>
                            <CompanyTypeLogo type={content.companyType.title.toLowerCase()} />
                        </UI.FadeIn>
                    )}
                </DetailTemplateHeader>

                <UI.ResponsiveSpacer size="l" />

                {content.content && (
                    <AnimateWhenVisible>
                        {content.tagline && (
                            <p css={[UI.greyDarkText, UI.leadText]}>{content.tagline}</p>
                        )}
                        <ReactMarkdown css={UI.rteContent} source={content.content.content} />
                    </AnimateWhenVisible>
                )}

                <UI.ResponsiveSpacer size="l" />

                {(content.address || contacts) && (
                    <UI.GreyBox>
                        <UI.LayoutContainer size="l" stack>
                            {content.address && (
                                <UI.LayoutItem sizeAtMobile={6 / 12}>
                                    <UI.Label>Company details</UI.Label>
                                    <UI.Spacer size="xs" />

                                    <Address address={content.address} />

                                    <UI.Spacer />

                                    <IconLinkList
                                        email={content.address.email}
                                        telephone={content.address.telephone}
                                        websiteUrl={content.address.websiteUrl}
                                    />
                                </UI.LayoutItem>
                            )}

                            <UI.LayoutItem sizeAtMobile={6 / 12}>
                                {allContacts.map((contact, index) => (
                                    <Fragment key={contact.title}>
                                        <UI.Label>{contact.title}</UI.Label>
                                        <UI.Spacer size="xs" />

                                        <strong>
                                            {contact.firstName} {contact.lastName}
                                        </strong>

                                        {['role', 'email', 'telephone'].map(info => {
                                            if (contact[info]) {
                                                return (
                                                    <>
                                                        <br />
                                                        {contact[info]}
                                                    </>
                                                );
                                            }
                                        })}
                                        {contact.image && (
                                            <>
                                                <UI.Spacer size="s" />
                                                <Image fixed={contact.image.fixed} />
                                            </>
                                        )}

                                        {index < allContacts.length - 1 && <UI.Spacer />}
                                    </Fragment>
                                ))}
                            </UI.LayoutItem>
                        </UI.LayoutContainer>
                    </UI.GreyBox>
                )}
            </Container>
        </LayoutContainer>
    );
};

export const query = graphql`
    query($id: String!) {
        memberNetwork: contentfulCompany(
            id: {eq: $id}
            isInMemberNetwork: {eq: true}
            companyType: {title: {ne: null}}
        ) {
            title
            tagline
            companyType {
                title
            }
            image {
                fluid: sizes(maxWidth: 500) {
                    ...GatsbyContentfulSizes
                }
            }
            content {
                content
            }
            contacts {
                title
                firstName
                lastName
                email
                telephone
            }
            address {
                name
                street
                city
                country
                postcode
                websiteUrl
                telephone
                email
            }
            websiteUrl
            facebookUrl
            instagramUrl
            twitterUrl
            youTubeUrl
            leadContactLastName
            leadContactFirstName
            leadContactRole
            leadContactImage {
                fixed(cropFocus: CENTER, quality: 75, width: 250) {
                    ...GatsbyContentfulFixed
                }
            }
        }
    }
`;

export default MemberNetworkDetailTemplate;

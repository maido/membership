/**
 * @prettier
 * @flow
 */
import React, {Fragment} from 'react';
import {graphql} from 'gatsby';
import dayjs from 'dayjs';
import get from 'lodash/get';
import ReactMarkdown from 'react-markdown';
import Address from '../components/Address';
import AnimateWhenVisible from '../components/AnimateWhenVisible';
import Container from '../components/Container';
import CTAButton from '../components/CTAButton';
import DetailTemplateHeader from '../components/DetailTemplateHeader';
import LayoutContainer from '../components/LayoutContainer';
import LogoList from '../components/LogoList';
import PageName from '../components/PageName';
import SEO from '../components/SEO';
import * as UI from '../components/UI/styles';
import {combineStartEndDate} from '../pages/events';
import {hasDatePassed} from '../globals/functions';

type Props = {data: Object};

const formatTicketsUrl = (ticketsUrl, eventTitle) => {
    if (ticketsUrl.includes('mailto')) {
        return `${ticketsUrl}?subject=${encodeURIComponent(`Tickets for ${eventTitle}`)}`;
    } else {
        return ticketsUrl;
    }
};

const EventDetailTemplate = ({data}: Props) => {
    const content = get(data, 'event');
    const hasPassed = hasDatePassed(content.endDate ? content.endDate : content.startDate);

    content.startDateFormatted = dayjs(content.startDate).format('YYYY/MM/DD');
    content.endDateFormatted = dayjs(content.endDate).format('D MMMM, YYYY');

    return (
        <LayoutContainer>
            <SEO description="" image="" title={get(content, 'title')} />

            <PageName title="Events" />

            <Container>
                <DetailTemplateHeader
                    name={content.title}
                    image={content.featuredImage ? content.featuredImage.fluid : ''}
                    badge={hasPassed ? 'Is finished' : ''}
                    memberType={content.memberType}
                    websiteUrl={content.websiteUrl}
                />

                <UI.Spacer sizeAtMobile="l" />

                <AnimateWhenVisible delay={200}>
                    <UI.GreyBox>
                        <UI.LayoutContainer size="l" stack>
                            {content.address && (
                                <UI.LayoutItem sizeAtMobile={4 / 12}>
                                    <UI.Label>Where</UI.Label>
                                    <UI.Spacer size="xs" />

                                    <Address address={content.address} />
                                </UI.LayoutItem>
                            )}

                            <UI.LayoutItem sizeAtMobile={8 / 12}>
                                <UI.LayoutContainer size="l" stack>
                                    <UI.LayoutItem sizeAtDesktop={6 / 12}>
                                        <UI.Label>{hasPassed ? 'Was' : 'When'}</UI.Label>
                                        <UI.Spacer size="xs" />

                                        <span
                                            dangerouslySetInnerHTML={{
                                                __html: combineStartEndDate(
                                                    content.startDate,
                                                    content.endDate
                                                )
                                            }}
                                        />
                                    </UI.LayoutItem>
                                    <UI.LayoutItem sizeAtDesktop={6 / 12}>
                                        {!hasPassed && content.ticketsUrl && (
                                            <Fragment>
                                                <UI.Label>Enquiries</UI.Label>
                                                <UI.Spacer size="xs" />

                                                <CTAButton
                                                    to={formatTicketsUrl(
                                                        content.ticketsUrl,
                                                        content.title
                                                    )}
                                                    border
                                                    ghost
                                                    block
                                                >
                                                    Find out more
                                                </CTAButton>
                                            </Fragment>
                                        )}
                                    </UI.LayoutItem>
                                </UI.LayoutContainer>
                            </UI.LayoutItem>
                        </UI.LayoutContainer>
                    </UI.GreyBox>
                </AnimateWhenVisible>

                <UI.Spacer sizeAtMobile="l" />

                {content.content && (
                    <AnimateWhenVisible>
                        <ReactMarkdown css={UI.rteContent} source={content.content.content} />
                    </AnimateWhenVisible>
                )}

                {content.sponsorLogos && (
                    <Fragment>
                        <UI.Divider />
                        <UI.ContentSection>
                            <LogoList title="Sponsors" images={content.sponsorLogos} />
                        </UI.ContentSection>
                    </Fragment>
                )}

                {content.supporterLogos && (
                    <Fragment>
                        <UI.Divider />
                        <UI.ContentSection>
                            <LogoList title="Supporters" images={content.supporterLogos} />
                        </UI.ContentSection>
                    </Fragment>
                )}

                {content.supplierLogos && (
                    <Fragment>
                        <UI.Divider />
                        <UI.ContentSection>
                            <LogoList title="Suppliers" images={content.supplierLogos} />
                        </UI.ContentSection>
                    </Fragment>
                )}
            </Container>
        </LayoutContainer>
    );
};

export const query = graphql`
    query($id: String!) {
        event: contentfulEvent(id: {eq: $id}) {
            title
            slug
            memberType
            isFeatured
            startDate(formatString: "YYYY/MM/DD HH:mm:ss")
            endDate(formatString: "YYYY/MM/DD HH:mm:ss")
            address {
                name
                street
                city
                country
                postcode
            }
            featuredImage {
                fluid: sizes(maxWidth: 500) {
                    ...GatsbyContentfulSizes
                }
            }
            sponsorLogos {
                fluid: sizes(maxWidth: 240) {
                    ...GatsbyContentfulSizes
                }
            }
            supporterLogos {
                fluid: sizes(maxWidth: 240) {
                    ...GatsbyContentfulSizes
                }
            }
            supplierLogos {
                fluid: sizes(maxWidth: 240) {
                    ...GatsbyContentfulSizes
                }
            }
            content {
                content
            }
            websiteUrl
            ticketsUrl
        }
    }
`;

export default EventDetailTemplate;

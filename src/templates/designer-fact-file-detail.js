/**
 * @prettier
 * @flow
 */
import React from 'react';
import {Link, graphql} from 'gatsby';
import get from 'lodash/get';
import AnimateWhenVisible from '../components/AnimateWhenVisible';
import Container from '../components/Container';
import DownloadsList from '../components/DownloadsList';
import Editorial from '../components/Editorial';
import LayoutContainer from '../components/LayoutContainer';
import PageName from '../components/PageName';
import SEO from '../components/SEO';
import * as UI from '../components/UI/styles';

type Props = {data: Object};

const DesignerFactFileDetailTemplate = ({data}: Props) => {
    const content = get(data, 'designerFactFile');
    const memberType = get(content, 'memberType');
    const category = get(content, 'category.title');
    const topic = get(content, 'topic.title');
    const businessStage = get(content, 'businessStage.0.title');

    let author;

    if (content.author) {
        author = {
            description: get(content, 'author.tagline'),
            image: get(content, 'author.image'),
            title: get(content, 'author.title')
        };

        if (!author.description) {
            author.description = get(content, 'author.content.content');
        }
    }

    return (
        <LayoutContainer>
            <SEO description="" image="" title={content.title} />

            <PageName title="Designer Fact Files" />

            <Editorial
                author={author}
                content={get(content, 'content.content')}
                headerContent={
                    (memberType || category || topic || businessStage) && (
                        <UI.FadeInUp>
                            <UI.HorizontalList blockAtMobile={true} flex={true}>
                                {memberType && (
                                    <li>
                                        <UI.MemberTypeBadge>{memberType}</UI.MemberTypeBadge>
                                    </li>
                                )}
                                {category && (
                                    <li>
                                        <Link
                                            to={`/designer-fact-files/topics/?category=${encodeURIComponent(
                                                category
                                            )}&topic=All`}
                                            css={[UI.label, UI.primaryText]}
                                        >
                                            {category}
                                        </Link>
                                    </li>
                                )}
                                {topic && (
                                    <li>
                                        <Link
                                            to={`/designer-fact-files/topics/?category=${encodeURIComponent(
                                                category
                                            )}&topic=${encodeURIComponent(topic)}`}
                                            css={UI.label}
                                        >
                                            {topic}
                                        </Link>
                                    </li>
                                )}
                                {businessStage && (
                                    <li>
                                        <UI.ThemeKey themeKey={businessStage} />
                                        &nbsp;&nbsp;
                                        <UI.Label>{businessStage}</UI.Label>
                                    </li>
                                )}
                            </UI.HorizontalList>
                        </UI.FadeInUp>
                    )
                }
                heroImage={get(content, 'featuredImage')}
                title={get(content, 'title')}
                introduction={get(content, 'introduction.introduction')}
                topic={get(content, 'topic.title')}
            />

            {content.downloads && (
                <Container>
                    <AnimateWhenVisible>
                        <DownloadsList downloads={content.downloads} />
                    </AnimateWhenVisible>
                </Container>
            )}
        </LayoutContainer>
    );
};

export const query = graphql`
    query($id: String!) {
        designerFactFile: contentfulDesignerFactFile(id: {eq: $id}) {
            title
            slug
            memberType
            introduction {
                introduction
            }
            content {
                content
            }
            featuredImage {
                fluid: sizes(maxWidth: 1000) {
                    ...GatsbyContentfulSizes
                }
            }
            topic {
                title
            }
            category {
                title
            }
            businessStage {
                title
            }
            downloads {
                title
                file {
                    fileName
                    url
                    details {
                        size
                    }
                    contentType
                }
            }
            author {
                title
                image {
                    fluid: sizes(maxWidth: 300) {
                        ...GatsbyContentfulSizes
                    }
                }
                tagline
                content {
                    content
                }
                internal {
                    type
                }
            }
        }
    }
`;

export default DesignerFactFileDetailTemplate;

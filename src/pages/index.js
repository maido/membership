/**
 * @prettier
 * @flow
 */
import React, {Fragment} from 'react';
import {graphql} from 'gatsby';
import get from 'lodash/get';
import kebabCase from 'lodash/kebabCase';
import dayjs from 'dayjs';
import AnimateWhenVisible from '../components/AnimateWhenVisible';
import CardCarousel from '../components/CardCarousel';
import CarouselListCard from '../components/CarouselListCard';
import Container from '../components/Container';
import LayoutContainer from '../components/LayoutContainer';
import LatestPostsHero from '../components/LatestPostsHero';
import SEO from '../components/SEO';
import Theme from '../components/Theme';
import * as UI from '../components/UI/styles';
import {getPersonsTagline, hasDatePassed} from '../globals/functions';

type Props = {
    data: {
        heroImage: Object,
        events: {edges: Array<Object>},
        designerFactFiles: {edges: Array<Object>},
        announcements: {edges: Array<Object>},
        reportsAndInsights: {edges: Array<Object>},
        memberNetworks: {edges: Array<Object>}
    }
};

type Card = {
    description?: string,
    image?: Object,
    label?: string,
    link?: string,
    subtitle?: string,
    tag?: string,
    tagType?: string,
    title?: string,
    to?: string
};

/**
 * While we do filter out old events when we build the site, there could be instances where
 * events will become 'old' before another build happens. This filter catches those instances
 * and removes them.
 */
const removeOldEvents = post => {
    if (post.node) {
        post = post.node;
    }

    if (post.__typename === 'ContentfulEvent') {
        if (hasDatePassed(post.endDate ? post.endDate : post.startDate)) {
            return false;
        }
    }

    return true;
};

const formatPostForCard = post => {
    let formattedPost: Card = {};

    if (post.node) {
        post = post.node;
    }

    if (post.internal) {
        post.__typename = post.internal.type;
    }

    if (post.__typename === 'ContentfulDesignerFactFile') {
        formattedPost = {
            tag: post.topic.title,
            image: post.featuredImage,
            label: 'Designer Fact File',
            memberType: post.memberType,
            to: `/designer-fact-files/${kebabCase(post.category.title)}/${kebabCase(
                post.topic.title
            )}/${post.slug}/`,
            title: post.title
        };
    } else if (post.__typename === 'ContentfulEditorial') {
        const pageSlugs = {
            Announcement: 'announcements',
            News: 'news',
            'Reports & Insights': 'reports-and-insights'
        };

        formattedPost = {
            image: post.featuredImage,
            label: post.pageType,
            memberType: post.memberType,
            to: `/${pageSlugs[post.pageType]}/${post.slug}/`,
            title: post.title
        };
    } else if (post.__typename === 'ContentfulCompany') {
        formattedPost = {
            description: getPersonsTagline(post),
            image: post.image,
            label: 'Member Network',
            link: post.websiteUrl,
            memberType: post.memberType,
            subtitle: get(post, 'companyType.title'),
            to: `/member-network/${kebabCase(post.title)}/`,
            title: post.title
        };
    } else if (post.__typename === 'ContentfulEvent') {
        formattedPost = {
            tag: dayjs(post.startDate).format('DD/MM/YYYY'),
            tagType: 'date',
            // isFeatured: post.isFeatured,
            // featuredLabel: 'Member Events',
            image: post.featuredImage,
            label: 'Event',
            memberType: post.memberType,
            to: `/events/${post.startDate}/${post.slug}/`,
            title: post.title
        };
    } else if (post.__typename === 'ContentfulOpportunitiesAndSupport') {
        formattedPost = {
            image: post.featuredImage,
            label: post.pageType,
            memberType: post.memberType,
            to: `/opportunities-and-support/${kebabCase(post.pageType)}/${post.slug}/`,
            title: post.title
        };
    }

    return formattedPost;
};

const IndexPage = ({data}: Props) => {
    const carousels = [
        {label: 'News', key: 'news', viewAll: '/news/'},
        {
            label: 'Opportunities & Support',
            key: 'opportunitiesAndSupport',
            viewAll: '/opportunities-and-support/'
        },
        {label: 'Reports & Insights', key: 'reportsAndInsights', viewAll: '/reports-and-insights/'},
        {label: 'Designer Fact File', key: 'designerFactFiles', viewAll: '/designer-fact-files/'},
        {label: 'Calendar', key: 'events', viewAll: '/events/'}
        // {label: 'Member Network', key: 'memberNetworks', viewAll: '/member-network/'}
    ];

    return (
        <LayoutContainer>
            <SEO description="" image="" title="Home" />
            <Theme theme="greyLightest">
                <Container>
                    {data && data.home && (
                        <Fragment>
                            <LatestPostsHero
                                backgroundImage={data.home.heroImage}
                                text={data.home.introductionText}
                                title={data.home.heroTitle}
                                titleImage={get(data.home, 'heroTitleImage.0.fixed.src')}
                            />

                            <UI.Spacer size="m" sizeAtMobile="l" />

                            <UI.FadeInUp delay={250}>
                                <CardCarousel label="Latest">
                                    {data.home.featuredPosts.map(formatPostForCard).map(post => (
                                        <CarouselListCard key={post.title} {...post} />
                                    ))}
                                </CardCarousel>
                            </UI.FadeInUp>
                        </Fragment>
                    )}
                </Container>
            </Theme>

            {carousels
                .filter(c => data[c.key].edges.length)
                .map((carousel, index) => (
                    <AnimateWhenVisible delay={index * 0.15} key={carousel.key}>
                        <Container>
                            <CardCarousel
                                label={carousel.label}
                                showNav={data[carousel.key].edges.length >= 3}
                                viewAll={carousel.viewAll}
                            >
                                {data[carousel.key].edges
                                    .filter(removeOldEvents)
                                    .map(formatPostForCard)
                                    .map(post => (
                                        <CarouselListCard key={post.title} {...post} label="" />
                                    ))}
                            </CardCarousel>
                        </Container>

                        {index < carousels.length - 1 && <UI.Divider />}
                    </AnimateWhenVisible>
                ))}
        </LayoutContainer>
    );
};

export default IndexPage;

export const query = graphql`
    query {
        home: contentfulHomePage {
            title
            introductionText
            heroImage {
                fluid: sizes(maxWidth: 800) {
                    ...GatsbyContentfulSizes
                }
            }
            heroTitle
            heroTitleImage {
                fixed: sizes(maxWidth: 800) {
                    ...GatsbyContentfulSizes
                }
            }
            featuredPosts {
                ... on Node {
                    ... on ContentfulEvent {
                        title
                        slug
                        memberType
                        startDate(formatString: "YYYY/MM/DD")
                        endDate(formatString: "YYYY/MM/DD")
                        featuredImage {
                            fixed(cropFocus: CENTER, height: 260, quality: 75, width: 380) {
                                ...GatsbyContentfulFixed
                            }
                        }
                        internal {
                            type
                        }
                    }
                    ... on ContentfulEditorial {
                        title
                        slug
                        memberType
                        featuredImage {
                            fixed(cropFocus: CENTER, height: 260, quality: 75, width: 380) {
                                ...GatsbyContentfulFixed
                            }
                        }
                        pageType
                        internal {
                            type
                        }
                    }
                    ... on ContentfulDesignerFactFile {
                        title
                        slug
                        memberType
                        featuredImage {
                            fixed(cropFocus: CENTER, height: 260, quality: 75, width: 380) {
                                ...GatsbyContentfulFixed
                            }
                        }
                        topic {
                            title
                        }
                        category {
                            title
                        }
                        internal {
                            type
                        }
                    }
                    ... on ContentfulCompany {
                        title
                        memberType
                        companyType {
                            title
                        }
                        websiteUrl
                        image {
                            fixed(cropFocus: CENTER, height: 260, quality: 75, width: 380) {
                                ...GatsbyContentfulFixed
                            }
                        }
                        internal {
                            type
                        }
                    }
                    ... on ContentfulOpportunitiesAndSupport {
                        title
                        slug
                        memberType
                        featuredImage {
                            fixed(cropFocus: CENTER, height: 260, quality: 75, width: 380) {
                                ...GatsbyContentfulFixed
                            }
                        }
                        pageType
                        internal {
                            type
                        }
                    }
                }
            }
        }
        events: allContentfulEvent(
            filter: {isFuture: {eq: true}}
            sort: {fields: startDate, order: ASC}
            limit: 20
        ) {
            edges {
                node {
                    title
                    slug
                    memberType
                    startDate(formatString: "YYYY/MM/DD")
                    endDate(formatString: "YYYY/MM/DD")
                    isFeatured
                    featuredImage {
                        fixed(cropFocus: CENTER, height: 260, quality: 75, width: 380) {
                            ...GatsbyContentfulFixed
                        }
                    }
                    internal {
                        type
                    }
                }
            }
        }
        designerFactFiles: allContentfulDesignerFactFile(
            sort: {fields: updatedAt, order: DESC}
            limit: 10
        ) {
            edges {
                node {
                    title
                    slug
                    memberType
                    category {
                        title
                    }
                    topic {
                        title
                    }
                    featuredImage {
                        fixed(cropFocus: CENTER, height: 260, quality: 75, width: 380) {
                            ...GatsbyContentfulFixed
                        }
                    }
                    internal {
                        type
                    }
                }
            }
        }
        announcements: allContentfulEditorial(
            filter: {pageType: {eq: "Announcement"}}
            sort: {fields: createdAt, order: DESC}
            limit: 10
        ) {
            edges {
                node {
                    title
                    slug
                    memberType
                    featuredImage {
                        fixed(cropFocus: CENTER, height: 260, quality: 75, width: 380) {
                            ...GatsbyContentfulFixed
                        }
                    }
                    pageType
                    internal {
                        type
                    }
                }
            }
        }
        reportsAndInsights: allContentfulEditorial(
            filter: {pageType: {eq: "Reports & Insights"}}
            sort: {fields: createdAt, order: DESC}
            limit: 10
        ) {
            edges {
                node {
                    title
                    slug
                    memberType
                    featuredImage {
                        fixed(cropFocus: CENTER, height: 260, quality: 75, width: 380) {
                            ...GatsbyContentfulFixed
                        }
                    }
                    pageType
                    internal {
                        type
                    }
                }
            }
        }
        news: allContentfulEditorial(
            filter: {pageType: {eq: "News"}}
            sort: {fields: createdAt, order: DESC}
            limit: 10
        ) {
            edges {
                node {
                    title
                    slug
                    memberType
                    featuredImage {
                        fixed(cropFocus: CENTER, height: 260, quality: 75, width: 380) {
                            ...GatsbyContentfulFixed
                        }
                    }
                    pageType
                    internal {
                        type
                    }
                }
            }
        }
        memberNetworks: allContentfulCompany(
            filter: {isInMemberNetwork: {eq: true}, companyType: {title: {ne: null}}}
            sort: {fields: title, order: ASC}
            limit: 10
        ) {
            edges {
                node {
                    title
                    memberType
                    companyType {
                        title
                    }
                    websiteUrl
                    tagline
                    content {
                        content
                    }
                    image {
                        fixed(cropFocus: CENTER, height: 260, quality: 75, width: 380) {
                            ...GatsbyContentfulFixed
                        }
                    }
                    internal {
                        type
                    }
                }
            }
        }
        opportunitiesAndSupport: allContentfulOpportunitiesAndSupport(
            sort: {fields: createdAt, order: DESC}
            limit: 10
        ) {
            edges {
                node {
                    title
                    slug
                    memberType
                    featuredImage {
                        fixed(cropFocus: CENTER, height: 260, quality: 75, width: 380) {
                            ...GatsbyContentfulFixed
                        }
                    }
                    pageType
                    internal {
                        type
                    }
                }
            }
        }
    }
`;

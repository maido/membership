/**
 * @prettier
 * @flow
 */
import React, {useState} from 'react';
import {graphql, Link, navigate} from 'gatsby';
import {css} from '@emotion/core';
import get from 'lodash/get';
import head from 'lodash/head';
import kebabCase from 'lodash/kebabCase';
import sortBy from 'lodash/sortBy';
import uniq from 'lodash/uniq';
import 'url-search-params-polyfill';
import AnimateWhenVisible from '../../components/AnimateWhenVisible';
import CTAButton from '../../components/CTAButton';
import DropdownMenu from '../../components/DropdownMenu';
import FixedHeader from '../../components/FixedHeader';
import FixedHeaderInfoButton from '../../components/FixedHeaderInfoButton';
import LayoutContainer from '../../components/LayoutContainer';
import ListCard from '../../components/ListCard';
import SEO from '../../components/SEO';
import Tabs from '../../components/Tabs';
import * as UI from '../../components/UI/styles';
import {scrollToTop} from '../../globals/functions';

type Props = {
    data: {
        designerFactFiles: {
            edges: Array<{node: ContentfulEditorial}>
        }
    },
    location: Object
};

export const getCategoriesAndTopics = items => {
    const categoriesAndTopics = {};
    const categories = sortBy(
        uniq(items.map(item => get(item, 'category.title'))),
        item => item
    ).filter(c => c);

    categories.map(category => {
        if (!categoriesAndTopics[category]) {
            categoriesAndTopics[category] = [];
        }
    });

    items.map(item => {
        const category = get(item, 'category.title');
        const topic = get(item, 'topic.title');

        if (!topic || !categoriesAndTopics[category]) {
            return;
        }

        if (!categoriesAndTopics[category].includes(topic)) {
            categoriesAndTopics[category].push(topic);
        }
    });

    return categoriesAndTopics;
};

const getDefaultCategory = (searchParams, categoriesAndTopics) => {
    if (searchParams.has('category')) {
        return decodeURI(searchParams.get('category'));
    } else {
        return head(Object.keys(categoriesAndTopics));
    }
};

const getDefaultTopic = searchParams => {
    if (searchParams.has('topic')) {
        return decodeURI(searchParams.get('topic'));
    } else {
        return 'All';
    }
};

const isEntryVisible = (entry, activeTopic, activeCategory) => {
    if (activeTopic === 'All' && entry.category) {
        return entry.category.title === activeCategory;
    } else {
        return (
            entry.category &&
            entry.category.title === activeCategory &&
            entry.topic && entry.topic.title === activeTopic
        );
    }
};

const updateNavigationSearch = (pathname, category, topic) => {
    navigate(
        `${pathname}?category=${encodeURIComponent(category)}&topic=${encodeURIComponent(topic)}`,
        {
            replace: false
        }
    );
};

const DesignerFactFilesGuidesPage = ({data, location}: Props) => {
    const content = get(data, 'designerFactFiles.edges', []).map(i => i.node);
    const categoriesAndTopics = getCategoriesAndTopics(content);

    const searchParams = new URLSearchParams(location.search);
    const defaultCategory = getDefaultCategory(searchParams, categoriesAndTopics);

    const defaultTopic = getDefaultTopic(searchParams);
    const [activeCategory, setActiveCategory] = useState(defaultCategory);
    const [activeTopic, setActiveTopic] = useState(defaultTopic);
    const [tabs, setTabs] = useState(['All', ...categoriesAndTopics[defaultCategory]]);

    const handleCategoryChange = newCategory => {
        if (newCategory) {
            setActiveCategory(newCategory);
            setTabs(['All', ...categoriesAndTopics[newCategory]]);
            setActiveTopic('All');
            scrollToTop();
            updateNavigationSearch(location.pathname, newCategory, 'All');
        }
    };

    const handleTopicChange = newTopic => {
        setActiveTopic(newTopic);
        scrollToTop();
        updateNavigationSearch(location.pathname, activeCategory, newTopic);
    };

    return (
        <LayoutContainer>
            <SEO description="" image="" title="Designer Fact File" />
            <FixedHeader>
                <div
                    css={[
                        css`
                            display: flex;
                            flex-direction: column;
                            flex-grow: 1;
                            justify-content: center;
                            position: relative;
                        `,
                        UI.responsiveHorizontalPadding
                    ]}
                >
                    <UI.FadeInUp>
                        <UI.Label>Select a category:</UI.Label>

                        <DropdownMenu
                            activeItem={activeCategory}
                            items={Object.keys(categoriesAndTopics).map(category => ({
                                label: category,
                                value: category
                            }))}
                            handleClick={handleCategoryChange}
                        />

                        <FixedHeaderInfoButton to="/designer-fact-files">
                            About
                        </FixedHeaderInfoButton>
                    </UI.FadeInUp>
                </div>

                <UI.FadeInUp delay={200} css={UI.responsiveHorizontalPadding}>
                    <Tabs items={tabs} handleTabClick={handleTopicChange} activeTab={activeTopic} />
                </UI.FadeInUp>
            </FixedHeader>

            <UI.LayoutContainer flush>
                {content
                    .filter(item => isEntryVisible(item, activeTopic, activeCategory))
                    .map((item, index) => (
                        <UI.LayoutItem
                            sizeAtDesktopLarge={6 / 12}
                            key={`${activeTopic}-${item.slug}`}
                        >
                            <AnimateWhenVisible delay={index * 100}>
                                <ListCard
                                    image={get(item, 'featuredImage.fixed')}
                                    memberType={get(item, 'memberType')}
                                    tag={get(item, 'businessStage.0.title')}
                                    themeKey={get(item, 'businessStage.0.title')}
                                    title={item.title}
                                    to={`/designer-fact-files/${kebabCase(
                                        get(item, 'category.title')
                                    )}/${kebabCase(get(item, 'topic.title'))}/${item.slug}`}
                                />
                            </AnimateWhenVisible>
                        </UI.LayoutItem>
                    ))}
            </UI.LayoutContainer>
        </LayoutContainer>
    );
};

export const query = graphql`
    query {
        designerFactFiles: allContentfulDesignerFactFile(sort: {fields: [title], order: ASC}) {
            edges {
                node {
                    title
                    slug
                    memberType
                    featuredImage {
                        fixed(height: 90, width: 90) {
                            ...GatsbyContentfulFixed
                        }
                    }
                    topic {
                        title
                    }
                    category {
                        title
                    }
                    businessStage {
                        title
                    }
                }
            }
        }
    }
`;

export default DesignerFactFilesGuidesPage;

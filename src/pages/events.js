/**
 * @prettier
 * @flow
 */
import React, {Fragment, useState} from 'react';
import {graphql, navigate} from 'gatsby';
import {css} from '@emotion/core';
import dayjs from 'dayjs';
import get from 'lodash/get';
import indexOf from 'lodash/indexOf';
import truncate from 'lodash/truncate';
import 'url-search-params-polyfill';
import FixedHeader from '../components/FixedHeader';
import LayoutContainer from '../components/LayoutContainer';
import ListCard from '../components/ListCard';
import MasonryLayout from '../components/MasonryLayout';
import MonthSelector from '../components/MonthSelector';
import SEO from '../components/SEO';
import YearSelector from '../components/YearSelector';
import * as UI from '../components/UI/styles';
import {hasDatePassed, scrollToTop} from '../globals/functions';
import {log} from 'util';

type Props = {
    data: {
        designerFactFiles: {
            edges: Array<{node: ContentfulEditorial}>
        }
    },
    location: Object
};

const monthLabels = [
    'All',
    'January',
    'February',
    'March',
    'April',
    'May',
    'June',
    'July',
    'August',
    'September',
    'October',
    'November',
    'December'
];

const getFormattedTime = date => {
    const minutes = dayjs(date).format('mm');
    const time = dayjs(date).format(minutes === '00' ? 'ha' : 'h:mma');

    if (time !== '12am') {
        return time;
    }
};

export const combineStartEndDate = (start: string, end: string) => {
    const startDate = dayjs(start);
    const startTime = getFormattedTime(start);
    let combinedDate = '';

    if (end) {
        const endDate = dayjs(end);
        const endTime = getFormattedTime(end);
        const isSameMonth = endDate.isSame(startDate, 'month');
        const isSameYear = endDate.isSame(startDate, 'year');
        const isSameDay = endDate.isSame(startDate, 'day');

        if (startTime) {
            combinedDate = `${startDate.format('D MMMM')}, ${startTime}`;
        } else {
            combinedDate = startDate.format('D');
        }

        if (isSameDay) {
            combinedDate = `${combinedDate} ${endTime ? `<em>—</em> ${endTime}` : ''}`;
        } else {
            if ((isSameYear && isSameMonth) || isSameMonth) {
                combinedDate = `${combinedDate} <em>—</em> `;
            } else if (isSameYear) {
                if (startTime) {
                    combinedDate = `${startDate.format('D MMMM')}, ${startTime}`;
                } else {
                    combinedDate = startDate.format('D MMMM');
                }

                combinedDate = `${combinedDate} <em>—</em> `;
            }

            combinedDate = `${combinedDate} ${endDate.format('D MMMM YYYY')} ${
                endTime ? `, ${endTime}` : ''
            }`;
        }
    } else {
        combinedDate = `${startDate.format('D MMMM YYYY')} ${startTime ? `, ${startTime}` : ''}`;
    }

    return combinedDate;
};

const getDefaultYear = searchParams => {
    if (searchParams.has('year')) {
        return decodeURI(searchParams.get('year'));
    } else {
        return dayjs().year();
    }
};

const getDefaultMonth = searchParams => {
    if (searchParams.has('month')) {
        return decodeURI(searchParams.get('month'));
    } else {
        const thisMonth = monthLabels[dayjs().month() + 1];

        return thisMonth ? thisMonth : 'All';
    }
};

const updateNavigationSearch = (pathname, month, year = '') => {
    navigate(`${pathname}?month=${encodeURIComponent(month)}&year=${encodeURIComponent(year)}`, {
        replace: false
    });
};

const eventIsInYear = (item: Object, year: number) => {
    const startsInSameYear = dayjs(item.startDate).year() == year;
    const endsInSameYear = dayjs(item.endDate).year() == year;

    return startsInSameYear || endsInSameYear;
};

const getDefaultFilteredContent = (defaultMonth: string, contentByMonth: Object) => {
    if (defaultMonth !== 'All' && contentByMonth[defaultMonth]) {
        return contentByMonth[defaultMonth];
    }

    return [];
};

const EventsPage = ({data, location}: Props) => {
    const content = get(data, 'events.edges', []).map(item => {
        const description = item.node.introduction
            ? item.node.introduction
            : truncate(get(item.node.content, 'content', ''), {length: 70, separator: ' '});

        return {
            ...item.node,
            description,
            endDateFormatted: dayjs(item.node.endDate).format('D MMMM, YYYY'),
            startDateFormatted: dayjs(item.node.startDate).format('D MMMM, YYYY')
        };
    });
    const contentByMonth = content.reduce((byMonth, item) => {
        const month = monthLabels[dayjs(item.startDate).month() + 1];

        if (!byMonth[month]) {
            byMonth[month] = [];
        }

        byMonth[month].push(item);

        return byMonth;
    }, {});
    const contentYears = [...new Set(content.map(i => dayjs(i.startDateFormatted).year()))];

    const searchParams = new URLSearchParams(location.search);
    const defaultMonth = getDefaultMonth(searchParams);
    const defaultYear = getDefaultYear(searchParams);

    const [activeMonth, setActiveMonth] = useState(defaultMonth);
    const [activeYear, setActiveYear] = useState(defaultYear);
    const [filteredContent, setFilteredContent] = useState(
        getDefaultFilteredContent(defaultMonth, contentByMonth)
    );

    const handleMonthChange = (newMonth: string) => {
        setActiveMonth(newMonth);

        if (newMonth === 'All') {
            setFilteredContent(content);
        } else {
            const newFilteredContent = contentByMonth[newMonth];

            setFilteredContent(newFilteredContent ? newFilteredContent : []);
        }

        scrollToTop();
        updateNavigationSearch(location.pathname, newMonth, activeYear);
    };

    const handleYearChange = (newYear: string) => {
        setActiveYear(newYear);
        scrollToTop();
        updateNavigationSearch(location.pathname, activeMonth, newYear);
    };

    const resetActiveMonth = (event?: Event) => {
        if (event) {
            event.preventDefault();
        }

        setActiveMonth('All');
        setFilteredContent([]);
        scrollToTop();
        updateNavigationSearch(location.pathname, 'All', activeYear);
    };

    const contentList = activeMonth !== 'All' ? filteredContent : content;
    const contentListForYear = contentList.filter(item => eventIsInYear(item, activeYear));

    return (
        <LayoutContainer>
            <SEO description="" image="" title="Events" />
            <FixedHeader size="default">
                <div
                    css={css`
                        display: flex;
                        flex-direction: column;
                        flex-grow: 1;
                        justify-content: center;
                    `}
                >
                    <UI.FadeInUp>
                        <YearSelector
                            activeYear={activeYear}
                            handleClick={handleYearChange}
                            items={contentYears}
                        />
                        <MonthSelector
                            activeMonth={indexOf(monthLabels, activeMonth)}
                            handleClick={handleMonthChange}
                            items={monthLabels}
                        />
                    </UI.FadeInUp>
                </div>
            </FixedHeader>

            {contentListForYear && contentListForYear.length > 0 ? (
                <Fragment>
                    <MasonryLayout
                        columns={2}
                        items={contentListForYear.map(item => ({
                            key: item.title,
                            title: item.title,
                            component: (
                                <ListCard
                                    badge={
                                        hasDatePassed(item.endDate ? item.endDate : item.startDate)
                                            ? 'Is finished'
                                            : ''
                                    }
                                    description={item.isFeatured ? item.description : ''}
                                    image={
                                        item.isFeatured && item.featuredImage
                                            ? item.featuredImage.fixed
                                            : ''
                                    }
                                    isFeatured={item.isFeatured}
                                    label={item.isFeatured ? 'Member Events' : ''}
                                    memberType={item.memberType}
                                    tag={
                                        item.isFeatured
                                            ? combineStartEndDate(item.startDate, item.endDate)
                                            : item.startDateFormatted
                                    }
                                    tagType="date"
                                    title={item.title}
                                    to={`/events/${dayjs(item.startDate).format('YYYY/MM/DD')}/${
                                        item.slug
                                    }/`}
                                />
                            )
                        }))}
                    />

                    <UI.FadeInUp style={{margin: '24px 0', textAlign: 'center'}}>
                        <UI.Label>
                            Showing {contentList.length} of {contentList.length} events
                        </UI.Label>
                    </UI.FadeInUp>
                </Fragment>
            ) : (
                <UI.FadeInUp>
                    <ListCard
                        title="No events"
                        description={`Sorry, we couldn't find any events in ${activeMonth}`}
                        handleClick={resetActiveMonth}
                    />
                </UI.FadeInUp>
            )}
        </LayoutContainer>
    );
};

export const query = graphql`
    query {
        events: allContentfulEvent(sort: {fields: [startDate], order: ASC}) {
            edges {
                node {
                    title
                    slug
                    memberType
                    isFeatured
                    startDate(formatString: "YYYY/MM/DD HH:mm:ss")
                    endDate(formatString: "YYYY/MM/DD HH:mm:ss")
                    featuredImage {
                        fixed(height: 280, width: 360) {
                            ...GatsbyContentfulFixed
                        }
                    }
                    content {
                        content
                    }
                }
            }
        }
    }
`;

export default EventsPage;

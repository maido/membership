/**
 * @prettier
 * @flow
 */
import React, {useEffect, useState} from 'react';
import {graphql, navigate} from 'gatsby';
import MiniSearch from 'minisearch';
import {css} from '@emotion/core';
import dayjs from 'dayjs';
import get from 'lodash/get';
import truncate from 'lodash/truncate';
import kebabCase from 'lodash/kebabCase';
import ContentLoader, {Facebook} from 'react-content-loader';
import AnimateWhenVisible from '../components/AnimateWhenVisible';
import FixedHeader from '../components/FixedHeader';
import LayoutContainer from '../components/LayoutContainer';
import ListCard from '../components/ListCard';
import {container as listCardContainer} from '../components/ListCard/styles';
import Pagination from '../components/Pagination';
import SEO from '../components/SEO';
import TextInput from '../components/TextInput';
import {getPersonsTagline} from '../globals/functions';
import {breakpoints} from '../globals/variables';
import * as UI from '../components/UI/styles';

type Props = {
    location: string
};

const getQuery = searchParams => {
    if (searchParams.has('query')) {
        return decodeURI(searchParams.get('query'));
    }
};

const getUrl = result => {
    switch (result.type) {
        case 'Designer Fact Files': {
            return `/events/${dayjs(result.startDate).format('YYYY/MM/DD')}/${result.slug}/`;
        }
        case 'Designer Fact Files': {
            return `/designer-fact-files/${kebabCase(get(result, 'category'))}/${kebabCase(
                get(result, 'topic')
            )}/${result.slug}`;
        }
        case 'Member Network': {
            return `/member-network/${kebabCase(result.memberType)}${result.slug}`;
        }
        case 'News': {
            return `/news/${result.slug}`;
        }
        case 'Opportunities & Support': {
            return `/opportunities-and-support/${result.pageType}/${result.slug}`;
        }
        case 'Reports & Insights': {
            return `/reports-and-insights/${result.slug}`;
        }
    }
};

const PAGINATION_SIZE = 40;

const SearchListingPage = ({location}: Props) => {
    const searchParams = new URLSearchParams(location.search);
    const [query, setQuery] = useState('');
    const [hasFeed, setHasFeed] = useState(false);
    const [textInput, setTextInput] = useState(query);
    const [isSearching, setIsSearching] = useState(false);
    const [results, setResults] = useState([]);
    const [activePage, setActivePage] = useState(1);

    const [miniSearch] = useState(
        new MiniSearch({
            fields: [
                'content',
                'endDate',
                'id',
                'memberType',
                'slug',
                'startDate',
                'title',
                'type'
            ],
            storeFields: [
                'businessStage',
                'content',
                'category',
                'date',
                'endDate',
                'image',
                'id',
                'memberType',
                'pageType',
                'slug',
                'startDate',
                'title',
                'topic',
                'type'
            ]
        })
    );

    const fetchSourceData = async () => {
        if (!hasFeed) {
            const req = await fetch('/feed.json');
            const data = await req.json();

            if (req.ok && data) {
                setHasFeed(true);
                miniSearch.addAll(data);
            }
        }
    };

    const handleTextInputChange = (event: KeyboardEvent) => {
        const $target = ((event.target: any): HTMLInputElement);

        setTextInput($target.value);
    };

    const updateSearchParams = value => {
        searchParams.set('query', value);

        const newPath = window.location.pathname + '?' + searchParams.toString();

        history.pushState(null, '', newPath);
    };

    const resetQuery = (event?: Event) => {
        if (event) {
            event.preventDefault();
        }

        setQuery('');
        setTextInput('');
        setResults([]);
        updateSearchParams('');
    };

    const trackSearch = query => {
        if (typeof window !== 'undefined' && window.ga) {
            window.ga(`send`, `event`, {
                eventCategory: 'search',
                eventAction: 'submit',
                eventLabel: query
            });
        }
    };

    const search = query => {
        setIsSearching(true);

        const searchResults = miniSearch.search(query, {
            boost: {date: 3, title: 2},
            fuzzy: 0.1
        });

        setResults(searchResults);
        setIsSearching(false);
        trackSearch(query);
    };

    const handleSubmit = event => {
        if (event) {
            event.preventDefault();
        }

        updateSearchParams(textInput || '');
        search(textInput);
    };

    const setupSearch = async () => {
        const defaultQuery = getQuery(searchParams);

        if (defaultQuery) {
            setIsSearching(true);
            setQuery(defaultQuery);
            setTextInput(defaultQuery);
            await fetchSourceData();

            search(defaultQuery);
        }
    };

    const handlePageChange = page => {
        setActivePage(page);
        window.scrollTo({
            top: 0,
            left: 0,
            behavior: 'smooth'
        });
    };

    useEffect(() => {
        setupSearch();
    }, [location.search]);

    const startIndex = activePage > 1 ? (activePage - 1) * PAGINATION_SIZE : 0;
    const endIndex = startIndex + PAGINATION_SIZE;
    const filteredResults = results.filter((_, index) => index >= startIndex && index <= endIndex);

    return (
        <LayoutContainer>
            <SEO description="" image="" title="Search" />
            <FixedHeader size="medium">
                <div
                    css={[
                        css`
                            display: flex;
                            flex-direction: column;
                            flex-grow: 1;
                            justify-content: center;
                        `,
                        UI.responsiveHorizontalPadding
                    ]}
                >
                    <UI.FadeInUp as="form" onSubmit={handleSubmit}>
                        <TextInput
                            handleChange={handleTextInputChange}
                            handleClear={resetQuery}
                            placeholder="Search..."
                            value={textInput}
                        />
                    </UI.FadeInUp>
                </div>
            </FixedHeader>

            {isSearching &&
                [...Array(10)].map(i => (
                    <div
                        css={listCardContainer}
                        style={{flexShrink: 0, overflow: 'hidden', minWidth: 1000}}
                    >
                        <ContentLoader
                            speed={2}
                            width={1000}
                            height={100}
                            viewBox="0 0 1000 100"
                            backgroundColor="#f3f3f3"
                            foregroundColor="#ecebeb"
                            key={`loading-${i}`}
                        >
                            <rect x="0" y="0" rx="0" ry="0" x width="100" height="100" />
                            <rect x="134" y="12" rx="0" ry="0" width="447" height="25" />
                            <rect x="134" y="55" rx="0" ry="0" width="550" height="44" />
                        </ContentLoader>
                    </div>
                ))}

            {!isSearching && filteredResults.length > 0 && (
                <>
                    {filteredResults.map(result => {
                        const truncatedContent = result.content
                            ? truncate(result.content, {length: 150, separator: '...'})
                            : '';

                        return (
                            <ListCard
                                key={result.slug}
                                description={
                                    truncatedContent
                                        ? truncatedContent
                                              .replace(/(\r\n|\n|\r)/gm, '')
                                              .replace(/__/gm, '')
                                              .replace(/\*\*/gm, '')
                                        : ''
                                }
                                image={result.image ? `https:${result.image}` : ''}
                                imageType="direct"
                                memberType={get(result, 'memberType')}
                                tag={result.type}
                                themeKey={get(result, 'businessStage.0.title')}
                                title={result.title}
                                to={getUrl(result)}
                            >
                                {result.date ? dayjs(result.date).format('D MMMM, YYYY') : null}
                            </ListCard>
                        );
                    })}

                    <Pagination
                        activePage={activePage}
                        currentItems={
                            activePage > 1 ? filteredResults.length : filteredResults.length - 1
                        }
                        itemsCountPerPage={PAGINATION_SIZE}
                        onChange={handlePageChange}
                        pageRangeDisplayed={5}
                        skip={startIndex}
                        totalItemsCount={results.length}
                    />
                </>
            )}

            {!isSearching && filteredResults.length === 0 && query !== '' && (
                <UI.FadeInUp>
                    <ListCard
                        title="No results"
                        description="Sorry, we couldn't find any results for that search."
                        handleClick={resetQuery}
                    />
                </UI.FadeInUp>
            )}
        </LayoutContainer>
    );
};

export default SearchListingPage;

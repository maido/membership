/**
 * @prettier
 * @flow
 */
import React from 'react';
import {graphql} from 'gatsby';
import find from 'lodash/find';
import get from 'lodash/get';
import sortBy from 'lodash/sortBy';
import uniq from 'lodash/uniq';
import ReactMarkdown from 'react-markdown';
import AnimateWhenVisible from '../components/AnimateWhenVisible';
import BusinessStages from '../components/BusinessStages';
import CardCarousel from '../components/CardCarousel';
import CarouselListCard from '../components/CarouselListCard';
import Container from '../components/Container';
import CTAButton from '../components/CTAButton';
import LayoutContainer from '../components/LayoutContainer';
import SEO from '../components/SEO';
import * as UI from '../components/UI/styles';

type Props = {
    data: {
        designerFactFiles: {
            edges: Array<{node: ContentfulEditorial}>
        }
    },
    location: Object
};

const DesignerFactFilesPage = ({data, location}: Props) => {
    const content = get(data, 'page', {});
    const designerFactFiles = get(data, 'designerFactFiles.edges', []).map(i => i.node);
    const categoryTitles = sortBy(
        uniq(designerFactFiles.map(item => get(item, 'category.title'))),
        item => item
    ).filter(c => c);
    const categories = categoryTitles.map(category =>
        find(designerFactFiles, c => get(c, 'category.title') === category)
    );

    return (
        <LayoutContainer>
            <SEO description="" image="" title="Designer Fact File" />
            <Container>
                <AnimateWhenVisible delay={0}>
                    <UI.Heading1>{content.title}</UI.Heading1>
                </AnimateWhenVisible>

                <AnimateWhenVisible delay={150} watch={false}>
                    <ReactMarkdown css={UI.leadText} source={content.introduction.introduction} />

                    <UI.Spacer />

                    <CTAButton to="/designer-fact-files/topics" ghost wide>
                        View topics
                    </CTAButton>
                </AnimateWhenVisible>

                <UI.Spacer size="l" />

                <AnimateWhenVisible delay={250}>
                    <CardCarousel label="Explore by category">
                        {categories.map((category: ContentfulDesignerFactFile) => (
                            <CarouselListCard
                                {...category}
                                key={category.category.title}
                                image={category.featuredImage}
                                to={`/designer-fact-files/topics/?category=${encodeURIComponent(
                                    category.category.title
                                )}&topic=All`}
                                title={category.category.title}
                            />
                        ))}
                    </CardCarousel>
                </AnimateWhenVisible>

                <UI.GreyBox>
                    <UI.Heading4 css={UI.marginBottomS}>{content.businessStagesTitle}</UI.Heading4>
                    <p>{content.businessStagesDescription.businessStagesDescription}</p>

                    <UI.Spacer size="s" />

                    <BusinessStages stages={content.businessStages} />
                </UI.GreyBox>
            </Container>
        </LayoutContainer>
    );
};

export const query = graphql`
    query {
        page: contentfulDesignerFactFileIntroduction {
            title
            introduction {
                introduction
            }
            businessStagesTitle
            businessStagesDescription {
                businessStagesDescription
            }
            businessStages {
                title
                description
            }
        }
        designerFactFiles: allContentfulDesignerFactFile(sort: {fields: [title], order: ASC}) {
            edges {
                node {
                    title
                    slug
                    featuredImage {
                        fixed(cropFocus: CENTER, height: 245, quality: 75, width: 350) {
                            ...GatsbyContentfulFixed
                        }
                    }
                    topic {
                        title
                    }
                    category {
                        title
                    }
                    businessStage {
                        title
                        description
                    }
                }
            }
        }
    }
`;

export default DesignerFactFilesPage;

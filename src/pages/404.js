/**
 * @prettier
 * @flow
 */
import React from 'react';
import {css} from '@emotion/core';
import CTAButton from '../components/CTAButton';
import Container from '../components/Container';
import LayoutContainer from '../components/LayoutContainer';
import * as UI from '../components/UI/styles';

const Error404Page = () => (
    <LayoutContainer>
        <div
            css={css`
                align-items: center;
                display: flex;
                min-height: 100vh;
                justify-content: center;
                text-align: center;
                width: 100%;
            `}
        >
            <Container>
                <UI.Heading1>Sorry, page not found</UI.Heading1>

                <UI.Spacer />

                <CTAButton to="/" theme="primary">
                    Go to the home page
                </CTAButton>
            </Container>
        </div>
    </LayoutContainer>
);

export default Error404Page;

/**
 * @prettier
 * @flow
 */
import React from 'react';
import {graphql} from 'gatsby';
import filter from 'lodash/filter';
import get from 'lodash/get';
import kebabCase from 'lodash/kebabCase';
import sortBy from 'lodash/sortBy';
import take from 'lodash/take';
import uniq from 'lodash/uniq';
import ReactMarkdown from 'react-markdown';
import AnimateWhenVisible from '../components/AnimateWhenVisible';
import CardCarousel from '../components/CardCarousel';
import CarouselListCard from '../components/CarouselListCard';
import CompanyTypeLogo from '../components/CompanyTypeLogo';
import Container from '../components/Container';
import CTAButton from '../components/CTAButton';
import LayoutContainer from '../components/LayoutContainer';
import SEO from '../components/SEO';
import * as UI from '../components/UI/styles';

type Props = {
    data: {
        memberNetworks: {
            edges: Array<{node: ContentfulPerson}>
        }
    }
};

const MemberNetworkPage = ({data}: Props) => {
    const content = get(data, 'page', {});

    return (
        <LayoutContainer>
            <SEO description="" image="" title="Member Network" />
            <Container>
                <AnimateWhenVisible delay={0}>
                    <UI.Heading1>{content.title}</UI.Heading1>
                </AnimateWhenVisible>

                <AnimateWhenVisible delay={150} watch={false}>
                    <ReactMarkdown css={UI.leadText} source={content.introduction.introduction} />

                    <UI.Spacer />

                    <CTAButton to="/member-network/designer-members/" ghost wide>
                        View members
                    </CTAButton>
                </AnimateWhenVisible>

                <UI.Spacer size="l" />

                <AnimateWhenVisible delay={250}>
                    <CardCarousel>
                        {content.companyTypes.map(companyType => (
                            <CarouselListCard
                                key={companyType.title}
                                image={companyType.image}
                                to={`/member-network/${kebabCase(companyType.title)}s/`}
                                title={companyType.title}
                            />
                        ))}
                    </CardCarousel>
                </AnimateWhenVisible>

                <UI.GreyBox>
                    <UI.Heading4 css={UI.marginBottomS}>{content.companyTypeTitle}</UI.Heading4>
                    {content.companyTypeDescription && (
                        <ReactMarkdown
                            source={content.companyTypeDescription.companyTypeDescription}
                        />
                    )}

                    <UI.LayoutContainer size="l">
                        {content.companyTypes.map(companyType => (
                            <UI.LayoutItem
                                key={companyType.title}
                                sizeAtMobileSmall={6 / 12}
                                style={{marginTop: 30}}
                            >
                                <UI.Heading5
                                    as="a"
                                    href={`/member-network/${kebabCase(companyType.title)}s/`}
                                    css={[UI.marginBottomS, UI.blackText]}
                                >
                                    {companyType.title}
                                </UI.Heading5>

                                <p>{companyType.description}</p>
                            </UI.LayoutItem>
                        ))}
                    </UI.LayoutContainer>
                </UI.GreyBox>
            </Container>
        </LayoutContainer>
    );
};

export const query = graphql`
    query {
        page: contentfulMemberNetworkIntroduction {
            title
            introduction {
                introduction
            }
            companyTypeTitle
            companyTypes {
                title
                description
                image {
                    fixed(cropFocus: CENTER, height: 245, quality: 75, width: 350) {
                        ...GatsbyContentfulFixed
                    }
                }
            }
        }
    }
`;

export default MemberNetworkPage;

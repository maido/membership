/**
 * @prettier
 * @flow
 */
import React, {useState} from 'react';
import {graphql, Link, navigate} from 'gatsby';
import {css} from '@emotion/core';
import get from 'lodash/get';
import head from 'lodash/head';
import kebabCase from 'lodash/kebabCase';
import sortBy from 'lodash/sortBy';
import uniq from 'lodash/uniq';
import 'url-search-params-polyfill';
import AnimateWhenVisible from '../components/AnimateWhenVisible';
import CTAButton from '../components/CTAButton';
import DropdownMenu from '../components/DropdownMenu';
import FixedHeader from '../components/FixedHeader';
import FixedHeaderInfoButton from '../components/FixedHeaderInfoButton';
import LayoutContainer from '../components/LayoutContainer';
import ListCard from '../components/ListCard';
import SEO from '../components/SEO';
import Tabs from '../components/Tabs';
import * as UI from '../components/UI/styles';
import {scrollToTop} from '../globals/functions';

type Props = {
    data: {
        opportunitiesAndSupport: {
            edges: Array<{node: ContentfulEditorial}>
        }
    },
    location: Object
};

const getDefaultPageType = searchParams => {
    if (searchParams.has('type')) {
        return decodeURI(searchParams.get('type'));
    } else {
        return 'All';
    }
};

const isEntryVisible = (entry, activePageType) => {
    if (activePageType !== 'All' && entry.pageType) {
        return entry.pageType === activePageType;
    }

    return true;
};

const updateNavigationSearch = (pathname, pageType) => {
    navigate(`${pathname}?type=${encodeURIComponent(pageType)}`, {
        replace: false
    });
};

const OpportunitiesAndSupportPage = ({data, location}: Props) => {
    const content = get(data, 'opportunitiesAndSupport.edges', []).map(i => i.node);
    const pageTypes = ['All', 'Opportunities', 'Support'];
    const tabs = pageTypes;

    const searchParams = new URLSearchParams(location.search);
    const defaultPageType = getDefaultPageType(searchParams);
    const [activePageType, setActivePageType] = useState(defaultPageType);

    const handlePageTypeChange = newPageType => {
        if (newPageType) {
            setActivePageType(newPageType);
            scrollToTop();
            updateNavigationSearch(location.pathname, newPageType);
        }
    };

    return (
        <LayoutContainer>
            <SEO description="" image="" title="Opportunities & Support" />
            <FixedHeader size="small">
                <UI.FadeInUp delay={200} css={UI.responsiveHorizontalPadding}>
                    <Tabs
                        items={tabs}
                        handleTabClick={handlePageTypeChange}
                        activeTab={activePageType}
                    />
                </UI.FadeInUp>
            </FixedHeader>

            <UI.LayoutContainer flush>
                {content
                    .filter(item => isEntryVisible(item, activePageType))
                    .map((item, index) => (
                        <UI.LayoutItem
                            sizeAtDesktopLarge={6 / 12}
                            key={`${activePageType}-${item.slug}`}
                        >
                            <AnimateWhenVisible delay={index * 100}>
                                <ListCard
                                    image={get(item, 'featuredImage.fixed')}
                                    memberType={get(item, 'memberType')}
                                    tag={get(item, 'pageType')}
                                    title={item.title}
                                    to={`/opportunities-and-support/${kebabCase(
                                        get(item, 'pageType')
                                    )}/${item.slug}`}
                                />
                            </AnimateWhenVisible>
                        </UI.LayoutItem>
                    ))}
            </UI.LayoutContainer>
        </LayoutContainer>
    );
};

export const query = graphql`
    query {
        opportunitiesAndSupport: allContentfulOpportunitiesAndSupport(
            sort: {fields: [title], order: ASC}
        ) {
            edges {
                node {
                    title
                    slug
                    featuredImage {
                        fixed(height: 90, width: 90) {
                            ...GatsbyContentfulFixed
                        }
                    }
                    memberType
                    pageType
                }
            }
        }
    }
`;

export default OpportunitiesAndSupportPage;

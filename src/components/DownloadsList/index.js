/**
 * @prettier
 * @flow
 */
import React, {Fragment} from 'react';
import get from 'lodash/get';
import split from 'lodash/split';
import tail from 'lodash/tail';
import toUpper from 'lodash/toUpper';
import {bytesToSize} from '../../globals/functions';
import CTAButton from '../CTAButton';
import * as UI from '../UI/styles';

type Props = {
    downloads: Array<Object>
};

const getDownloadUrl = url => {
    if (url && !url.includes('http') && !url.includes('https')) {
        return `https://${url}`;
    } else {
        return url;
    }
};

const DownloadsList = ({downloads}: Props) => (
    <UI.GreyBox>
        <UI.Label>Downloads</UI.Label>
        <UI.Spacer size="s" />

        {downloads.map((download, index) => (
            <Fragment key={download.title}>
                <UI.LayoutContainer size="l">
                    <UI.LayoutItem sizeAtMobile={7 / 12} sizeAtDesktop={8 / 12}>
                        <strong css={UI.leadText}>{download.title}</strong>
                        <br />
                        <small css={UI.greyText}>
                            {tail(split(toUpper(get(download, 'file.contentType')), '/'))} –{' '}
                            {bytesToSize(get(download, 'file.details.size'))}
                        </small>
                    </UI.LayoutItem>
                    <UI.LayoutItem
                        key={download.title}
                        sizeAtMobile={5 / 12}
                        sizeAtDesktop={4 / 12}
                    >
                        <CTAButton
                            to={getDownloadUrl(get(download, 'file.url'))}
                            target="noopener"
                            border
                            ghost
                            block
                        >
                            Download
                        </CTAButton>
                    </UI.LayoutItem>
                </UI.LayoutContainer>
                {index < downloads.length - 1 && <UI.Spacer />}
            </Fragment>
        ))}
    </UI.GreyBox>
);

export default DownloadsList;

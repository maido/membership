/**
 * @prettier
 * @flow
 */
import * as React from 'react';
import Image from 'gatsby-image';
import ReactMarkdown from 'react-markdown';
import {useSpring, animated} from 'react-spring/web.cjs';
import dayjs from 'dayjs';
import AnimateWhenVisible from '../AnimateWhenVisible';
import AuthorInfo from '../AuthorInfo';
import Container from '../Container';
import ResponsiveVideo from '../ResponsiveVideo';
import * as UI from '../UI/styles';
import * as S from './styles';
import {formatContentForMarkdown} from '../../globals/functions';

type Props = {
    author?: Object,
    content?: string,
    headerContent?: React.Node,
    heroImage?: Object,
    introduction?: string,
    title?: string,
    youTubeVideoUrl?: string
};

const Editorial = ({
    author,
    content,
    headerContent,
    heroImage,
    introduction,
    title,
    youTubeVideoUrl
}: Props) => {
    const imageProps = useSpring({
        config: {mass: 1, tension: 150, friction: 30},
        delay: 200,
        from: {opacity: 0, transform: 'scale(1.1) translateY(30px) rotateX(30deg)'},
        to: {opacity: 1, transform: 'scale(1) translateY(0) rotateX(0)'}
    });

    return (
        <Container>
            <header style={{maxWidth: '95%'}}>
                {title && (
                    <AnimateWhenVisible delay={50}>
                        <UI.Heading1>{title}</UI.Heading1>
                    </AnimateWhenVisible>
                )}

                {introduction && (
                    <React.Fragment>
                        <UI.Spacer size="s" />
                        <AnimateWhenVisible delay={150}>
                            <ReactMarkdown
                                css={[UI.greyDarkText, UI.leadText]}
                                source={formatContentForMarkdown(introduction)}
                            />
                        </AnimateWhenVisible>

                        <UI.Spacer size="s" />
                    </React.Fragment>
                )}

                {headerContent && (
                    <S.HeaderContentContainer>{headerContent}</S.HeaderContentContainer>
                )}
            </header>

            {heroImage && heroImage.fluid && (
                <S.ImageContainer>
                    <animated.div style={imageProps}>
                        <Image fluid={heroImage.fluid} />
                    </animated.div>
                </S.ImageContainer>
            )}

            {author && author.title && <AuthorInfo {...author} />}

            <UI.ResponsiveSpacer />

            {content && (
                <ReactMarkdown css={UI.rteContent} source={formatContentForMarkdown(content)} />
            )}

            {youTubeVideoUrl && (
                <React.Fragment>
                    <UI.Spacer size="l" />
                    <ResponsiveVideo url={youTubeVideoUrl} />
                </React.Fragment>
            )}
        </Container>
    );
};

export default Editorial;

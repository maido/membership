/**
 * @prettier
 * @flow
 */
import styled from '@emotion/styled';
import {rem} from 'polished';
import {responsiveSpacing} from '../../globals/functions';
import {breakpoints, spacing} from '../../globals/variables';

export const PaddedText = styled.div`
    @media (min-width: ${rem(breakpoints.mobile)}) {
        padding-left: ${responsiveSpacing(spacing.m)};
        padding-right: ${responsiveSpacing(spacing.m)};
    }

    @media (min-width: ${rem(breakpoints.desktop)}) {
        padding-left: ${responsiveSpacing(spacing.l)};
        padding-right: ${responsiveSpacing(spacing.l)};
    }
`;

export const HeaderContentContainer = styled.div`
    margin-top: ${rem(spacing.s)};
    margin-bottom: ${rem(spacing.m)};
`;

export const ImageContainer = styled.div`
    margin-top: ${responsiveSpacing(spacing.s)};
    padding-bottom: ${responsiveSpacing(spacing.s)};
    padding-top: ${responsiveSpacing(spacing.s)};

    .gatsby-image-wrapper {
        max-height: ${responsiveSpacing(600)};
        max-height: 75vh;
        width: auto;

        img {
            height: 100% !important;
            width: auto !important;
        }
    }
`;

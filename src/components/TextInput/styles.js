/**
 * @prettier
 * @flow
 */
import styled from '@emotion/styled';
import {between, rem} from 'polished';
import {breakpoints, colors, fontFamilies, spacing, transitions} from '../../globals/variables';

export const Container = styled.div`
    align-items: center;
    display: flex;

    svg {
        height: auto;
        margin-right: ${rem(spacing.s)};
        margin-top: 6px;
        width: ${between('12px', '20px')};
    }

    @media (min-width: ${rem(breakpoints.tablet)}) {
        svg {
            margin-right: ${rem(spacing.m)};
            margin-top: 10px;
            vertical-align: middle;
        }
    }
`;

export const TextInput = styled.input`
    border: 0;
    font-family: ${fontFamilies.bold};
    font-size: ${between('18px', '28px')};
    line-height: 1;
    padding-bottom: ${rem(spacing.xs)};
    padding-top: ${rem(spacing.xs)};
    transition: ${transitions.default};
    width: 100%;

    &::placeholder {
        color: ${colors.greyLight};
    }

    &:focus {
        outline: none;
    }
`;

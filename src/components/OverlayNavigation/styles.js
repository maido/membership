/**
 * @prettier
 */
import {css} from '@emotion/core';
import styled from '@emotion/styled';
import {between, rem} from 'polished';
import {
    breakpoints,
    colors,
    fontFamilies,
    fontSizes,
    spacing,
    transitions
} from '../../globals/variables';

export const Container = styled.div`
    background-color: ${colors.white};
    bottom: 0;
    color: ${colors.black};
    left: 0;
    right: 0;
    opacity: 0;
    overflow: ;
    position: fixed;
    transition: all 0.4s ease-out;
    transition-delay: 0.4s;
    visibility: hidden;
    top: 0;
    z-index: 100;

    ${props => props.isActive && `opacity: 1; visibility: visible; transition-delay: 0s;`};

    @media (min-width: ${rem(breakpoints.tablet)}) {
        display: none;
    }
`;

export const Wrapper = styled.nav`
    display: flex;
    flex-direction: column;
    height: calc(100% - 100px);
    justify-content: center;
    margin-top: 100px;
    -webkit-overflow-scrolling: touch;
    padding: ${rem(spacing.m)};
    transition: ${transitions.slow};
    width: 100%;
`;

export const LinksContainer = styled.div`
    display: flex;
    flex-direction: column;
    flex-grow: 1;
    justify-content: center;

    @media only screen and (max-height: 850px) and (orientation: landscape) {
        display: block;
        text-align: center;

        > div,
        a {
            display: inline-block;
        }

        > div + div {
            margin-left: ${rem(spacing.m)};
        }

        a {
            font-size: ${rem(22)} !important;
        }
    }
`;

export const link = css`
    color: ${colors.black};
    display: block;
    font-family: ${fontFamilies.bold};
    font-size: ${between(`${fontSizes.lead}px`, `${fontSizes.lead + 5}px`)};
    padding: ${rem(spacing.xs)} 0;
    text-align: center;

    &:hover,
    &:active,
    &[data-active='true'] {
        color: ${colors.primary};
    }
`;

/**
 * @prettier
 * @flow
 */
import React from 'react';
import {Link} from 'gatsby';
import {useSpring, useTrail, animated} from 'react-spring/web.cjs';
import SiteFooter from '../SiteFooter';
import {getLinkProps} from '../../globals/functions';
import * as S from './styles';

type Props = {
    handleClick: Function,
    isActive: boolean,
    links: Array<Object>
};

const OverlayNavigation = ({handleClick, isActive, links}: Props) => {
    const [linkProps, setLinkProps] = useTrail(links.length, () => ({
        config: {mass: 5, tension: 2000, friction: 200},
        opacity: 0,
        transform: 'scale(1.05) translateY(10px) rotateX(10deg)'
    }));

    if (isActive) {
        setLinkProps({opacity: 1, transform: 'scale(1) translateY(0) rotateX(0)'});
    } else {
        setLinkProps({opacity: 0, transform: 'scale(1.05) translateY(10px) rotateX(10deg)'});
    }

    return (
        <S.Container isActive={isActive} className={isActive && 'is-active'}>
            <S.Wrapper>
                <S.LinksContainer>
                    {linkProps.map((props, index) => (
                        <animated.div key={links[index].to} style={props}>
                            <Link
                                to={links[index].to}
                                key={links[index].to}
                                css={S.link}
                                getProps={getLinkProps}
                                onClick={handleClick}
                            >
                                {links[index].label}
                            </Link>
                        </animated.div>
                    ))}
                </S.LinksContainer>

                <SiteFooter hasLogo={false} />
            </S.Wrapper>
        </S.Container>
    );
};

export default OverlayNavigation;

/**
 * @prettier
 * @flow
 */
import styled from '@emotion/styled';
import {rem} from 'polished';
import {breakpoints, colors, fontFamilies, spacing, transitions} from '../../globals/variables';
import {responsiveSpacing} from '../../globals/functions';
import {FIXED_HEADER_HEIGHT} from '../FixedHeader/styles';

export const Container = styled.div`
    display: flex;
    height: ${rem(50)};
    justify-content: center;
    max-width: 100%;
    position: relative;
    z-index: 100;

    @media (max-width: ${rem(breakpoints.tablet)}) {
        transform: translateY(30px);
    }
`;

export const HiddenScrollContainer = styled.div`
    overflow-x: scroll;
    overflow-y: hidden;
    position: relative;
    -webkit-overflow-scrolling: touch;
    width: auto;
`;

export const ScrollContainer = styled.div`
    align-items: center;
    background-color: ${colors.white};
    display: flex;
    justify-content: center;
    width: 100%;
`;

export const TextWrapper = styled.span`
    align-self: center;
    display: flex;
`;

export const Button = styled.button`
    align-self: center;
    color: ${colors.greyLight};
    display: flex;
    font-family: ${fontFamilies.bold};
    font-size: ${rem(22)};
    justify-content: center;
    padding: 0 ${responsiveSpacing(spacing.m)};
    position: relative;
    transition: ${transitions.default};
    vertical-align: middle;

    &::after {
        background-color: ${colors.primary};
        border-radius: 50%;
        top: ${rem(50 * 0.65)};
        content: '';
        height: ${rem(4)};
        opacity: 0;
        position: absolute;
        left: 50%;
        transform: translateY(${rem(spacing.m)}) scale(0);
        transition: ${transitions.default};
        transition-duration: 0.4s;
        width: ${rem(4)};
    }

    &:hover,
    &:focus {
        color: ${colors.greyDark};
    }

    @media (max-width: ${rem(breakpoints.tablet)}) {
        &::after {
            top: ${rem(50 * 0.625)};
        }
    }

    @media (min-width: ${rem(breakpoints.mobile)}) {
        &::after {
            bottom: ${rem(2)};
        }
    }

    ${props =>
        props.position &&
        props.position === 'secondary' &&
        `
        color: ${colors.greyDark};
        transition-delay: .03s;

        &::after {
            opacity: 1;
            transform: translateY(0) scale(1);
            transition-delay: .1s;
        }
    `}
`;

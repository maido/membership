/**
 * @prettier
 * @flow
 */
import React, {useEffect, useRef} from 'react';
import smoothScrollIntoView from 'smooth-scroll-into-view-if-needed';
import * as S from './styles';

type Props = {
    activeYear?: number | string,
    handleClick: Function,
    items: Array<string>
};

const YearSelector = ({activeYear = new Date().getFullYear(), handleClick, items = []}: Props) => {
    const $containerRef = useRef(null);

    const setActiveYearPosition = ($element: HTMLElement) => {
        if ($element) {
            smoothScrollIntoView($element, {behavior: 'smooth', inline: 'center'});
        }
    };

    const getPositionType = (activeYear, year) => {
        return activeYear == year ? 'secondary' : 'tertiary';
    };

    const handleYearClick = (event: UIEvent, year) => {
        const $target = ((event.target: any): HTMLButtonElement);

        if ($target) {
            handleClick(year);
            setActiveYearPosition($target);
        }
    };

    useEffect(() => {
        /**
         * Needs some work...
         */
        if ($containerRef.current) {
            const $defaultYear = $containerRef.current.querySelector(
                `[data-year-index="${activeYear}"]`
            );

            if ($defaultYear) {
                $defaultYear.click();
            }
        }
    }, []);

    return (
        <S.Container ref={$containerRef}>
            <S.ScrollContainer>
                {items.map((year, index) => (
                    <S.Button
                        type="button"
                        position={getPositionType(activeYear, year)}
                        key={year}
                        data-year-index={index}
                        onClick={event => handleYearClick(event, year)}
                    >
                        <S.TextWrapper>{year}</S.TextWrapper>
                    </S.Button>
                ))}
            </S.ScrollContainer>
        </S.Container>
    );
};

export default YearSelector;

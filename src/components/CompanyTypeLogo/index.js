/**
 * @prettier
 * @flow
 */
import React from 'react';
import * as S from './styles';
import associateLogo from '../../images/member-associate.jpg';
import designerLogo from '../../images/member-designer.jpg';
import patronLogo from '../../images/member-patron.jpg';

type Props = {type: string};

const CompanyTypeLogo = ({type}: Props) => {
    if (type === 'associate') {
        return <S.Logo src={associateLogo} alt="Associate Member" />;
    } else if (type === 'designer') {
        return <S.Logo src={designerLogo} alt="Designer Member" />;
    } else if (type === 'patron') {
        return <S.Logo src={patronLogo} alt="Patron Member" />;
    }

    return null;
};

export default CompanyTypeLogo;

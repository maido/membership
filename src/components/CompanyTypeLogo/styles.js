/**
 * @prettier
 * @flow
 */
import styled from '@emotion/styled';
import {rem} from 'polished';
import {colors} from '../../globals/variables';

export const Logo = styled.img`
    width: ${rem(200)};
`;

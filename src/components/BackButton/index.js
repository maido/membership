/**
 * @prettier
 * @flow
 */
import React, {Fragment} from 'react';
import {navigate} from 'gatsby';
import {animated, useTransition} from 'react-spring/web.cjs';
import * as UI from '../UI/styles';
import * as S from './styles';

type Props = {
    location: Object
};

const getBackButtonUrl = pathname => {
    const paths = pathname.split('/').filter(p => p !== '');

    if (paths.length > 1) {
        if (paths[0] === 'designer-fact-files') {
            if (paths.length > 2) {
                return `/${paths[0]}/topics/`;
            }
        } else if (paths[0] === 'member-network') {
            if (paths.length > 2) {
                return `/${paths[0]}/${paths[1]}/`;
            }
        } else {
            return `/${paths[0]}/`;
        }
    }

    return '';
};

const BackButton = ({location}: Props) => {
    const backButtonUrl = getBackButtonUrl(location.pathname);
    const backButtonTransitions = useTransition(backButtonUrl, null, {
        config: {mass: 1, tension: 200, friction: 30},
        from: {
            position: 'absolute',
            opacity: 0,
            transform: 'scale(1.1) translateY(15px) rotateX(30deg)'
        },
        enter: {opacity: 1, transform: 'scale(1) translateY(0) rotateX(0)'},
        leave: {opacity: 0, transform: 'scale(1.1) translateY(15px) rotateX(30deg)'}
    });

    const handleBackButtonClick = () => navigate(backButtonUrl, {replace: true});

    return (
        <Fragment>
            {backButtonTransitions.map(
                ({item, key, props}) =>
                    item && (
                        <animated.div key={key} style={props}>
                            <UI.Spacer />

                            <UI.FlexLink
                                as="button"
                                onClick={handleBackButtonClick}
                                css={S.backButton}
                            >
                                <svg
                                    width="16"
                                    height="11"
                                    viewBox="0 0 16 11"
                                    xmlns="http://www.w3.org/2000/svg"
                                >
                                    <path
                                        d="M5.21928.16544c.22556-.22059.58067-.22059.80623 0 .21794.21313.21794.56786 0 .7805L1.93747 4.9438h13.48999c.31447 0 .57254.24444.57254.55197 0 .30753-.25807.5599-.57254.5599h-13.49l4.08805 3.99043c.21794.22059.21794.57581 0 .78845-.22556.22059-.58067.22059-.80623 0L.16346 5.89025c-.21795-.21314-.21795-.56786 0-.7805L5.21928.16544z"
                                        fill="#FF534B"
                                        fillRule="nonzero"
                                    />
                                </svg>{' '}
                                Back to all
                            </UI.FlexLink>
                        </animated.div>
                    )
            )}
        </Fragment>
    );
};

export default BackButton;

/**
 * @prettier
 * @flow
 */
import {css} from '@emotion/core';
import {colors, fontFamilies} from '../../globals/variables';

export const backButton = css`
    color: ${colors.primary};
    font-family: ${fontFamilies.default};

    &:hover,
    &:focus {
        &,
        * {
            color: ${colors.black};
            fill: ${colors.black};
        }
    }
`;

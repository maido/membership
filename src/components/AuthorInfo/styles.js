/**
 * @prettier
 * @flow
 */
import {css} from '@emotion/core';
import styled from '@emotion/styled';
import {rem} from 'polished';
import {
    breakpoints,
    colors,
    fontFamilies,
    fontSizes,
    spacing,
    transitions
} from '../../globals/variables';
import {responsiveSpacing} from '../../globals/functions';

export const Container = styled.div`
    background-color: ${colors.greyLightest};
    display: flex;
    padding: ${rem(spacing.m)};
`;

export const Title = styled.span`
    color: ${colors.black};
    display: block;
    font-size: ${rem(fontSizes.h5)};
    line-height: 1.3;
    margin-bottom: ${rem(spacing.xs)};
    margin-top: ${rem(spacing.xs)};
`;

export const ImageContainer = styled.div`
    margin-right: ${responsiveSpacing(spacing.s)};
    padding: ${rem(spacing.s)};

    &,
    & * {
        background-color: ${colors.white};
        align-items: center;
        display: flex;
        justify-content: center;
        text-align: center;
        width: ${rem(120)};

        img {
            height: auto;
            // max-width: ${rem(120)};
            vertical-align: top;
        }
    }
`;

export const Description = styled.p`
    color: ${colors.greyDark};
    font-size: ${rem(14)};
    line-height: 1.5;
    margin-bottom: 0;
`;

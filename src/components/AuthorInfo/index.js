/**
 * @prettier
 * @flow
 */
import React from 'react';
import Image from 'gatsby-image';
import * as UI from '../UI/styles';
import * as S from './styles';

type Props = {
    description?: string,
    image?: Object,
    title?: string
};

const AuthorInfo = ({description, image, title}: Props) => (
    <S.Container>
        {image && (
            <S.ImageContainer>
                <Image fluid={image.fluid} alt="" />
            </S.ImageContainer>
        )}
        <div>
            <UI.Label>Author</UI.Label>
            {title && <S.Title>{title}</S.Title>}
            {description && <S.Description>{description}</S.Description>}
        </div>
    </S.Container>
);

export default AuthorInfo;

/**
 * @prettier
 * @flow
 */
import * as React from 'react';
import {Link} from 'gatsby';
import {animated, useSpring, useTrail} from 'react-spring/web.cjs';
import BackButton from '../BackButton';
import Hamburger from '../Hamburger';
import SearchInput from '../SearchInput';
import SiteLogo from '../SiteLogo';
import SiteFooter from '../SiteFooter';
import {getLinkProps} from '../../globals/functions';
import * as UI from '../UI/styles';
import * as S from './styles';

type Props = {
    isHamburgerActive: boolean,
    handleHamburgerClick: Function,
    links: Array<Object>,
    location: Object,
    title: string
};

const SiteHeader = ({isHamburgerActive, handleHamburgerClick, links, location, title}: Props) => {
    const LINK_PROPS_TIMEOUT = 700;
    const $linksContainerRef = React.useRef(null);
    const $activeLinkIndicatorRef = React.useRef(null);

    /**
     * WIP work around for targetting correct dimensions of animated links once animations have completed.
     * This is required for the 'active' indicator...
     */
    let animatedLinkCount = 0;

    const siteLogoProps = useSpring({
        config: {mass: 1, tension: 150, friction: 30},
        delay: 300,
        from: {opacity: 0, transform: 'scale(1.1) translateY(30px) rotateX(30deg)'},
        to: {opacity: 1, transform: 'scale(1) translateY(0) rotateX(0)'}
    });
    const titleContainerProps = useSpring({
        config: {mass: 1, tension: 150, friction: 30},
        from: {opacity: 0, height: '0vh', overflow: 'hidden'},
        to: {opacity: 1, height: '100vh', overflow: 'hidden'}
    });
    const titleProps = useSpring({
        config: {mass: 1, tension: 200, friction: 30},
        delay: 200,
        from: {opacity: 0, transform: 'translateY(150%)', width: '100%'},
        to: {opacity: 1, transform: 'translateY(0)', width: '100%'}
    });
    const footerProps = useSpring({
        config: {mass: 1, tension: 150, friction: 30},
        delay: 1500,
        from: {opacity: 0},
        to: {opacity: 1}
    });

    const [activeLinkIndicatorProps, setActiveLinkProps, stopActiveLinkProps] = useSpring(() => ({
        config: {mass: 1, tension: 110, friction: 15},
        from: {opacity: 0, transform: 'scale(0)'}
    }));

    const setActiveLinkIndicator = ($activeLink: HTMLElement) => {
        const {height, y} = $activeLink.getBoundingClientRect();

        setActiveLinkProps({opacity: 1, transform: 'scale(1)', top: y + height / 2 - 2});
    };

    const [linkProps, setLinkProps] = useTrail(links.length, () => ({
        config: {mass: 5, tension: 2000, friction: 200},
        opacity: 0,
        transform: 'scale(1.05) translateY(10px) rotateX(10deg)',
        onRest: () => {
            if (animatedLinkCount < links.length) {
                animatedLinkCount++;

                if (animatedLinkCount === links.length) {
                    /**
                     * This is also a WIP workaround for setting default position of the active link indicator
                     */
                    if ($linksContainerRef.current && $activeLinkIndicatorRef.current) {
                        const $activeLink = $linksContainerRef.current.querySelector(
                            '[data-active="true"]'
                        );

                        if ($activeLink) {
                            const {height, y} = $activeLink.getBoundingClientRect();

                            $activeLinkIndicatorRef.current.style.opacity = 1;
                            $activeLinkIndicatorRef.current.style.top = `${y + height / 2 - 2}px`;
                            $activeLinkIndicatorRef.current.style.transform = 'scale(1)';

                            setActiveLinkIndicator($activeLinkIndicatorRef.current);
                        }
                    }
                }
            }
        }
    }));

    setTimeout(() => {
        setLinkProps({opacity: 1, transform: 'scale(1) translateY(0) rotateX(0)'});
    }, LINK_PROPS_TIMEOUT);

    const handleLinkClick = (event: Event) => {
        const $target = ((event.target: any): HTMLLinkElement);

        if ($target) {
            /**
             * We start the transition, but we need to wait for the link font size transition to finish so we can
             * read the final height/y of the element, so we wait and then stop the first transition and kick off
             * another to take over.
             */
            setActiveLinkIndicator($target);
            setTimeout(() => {
                stopActiveLinkProps();
                setActiveLinkIndicator($target);
            }, 200);
        }
    };

    React.useEffect(() => {
        const handleWindowResize = () => {
            if ($linksContainerRef.current) {
                const $activeLink = $linksContainerRef.current.querySelector(
                    '[data-active="true"]'
                );

                if ($linksContainerRef.current && $activeLink && $activeLinkIndicatorRef.current) {
                    const {height, y} = $activeLink.getBoundingClientRect();

                    $activeLinkIndicatorRef.current.style.top = `${y + height / 2 - 4}px`;

                    setActiveLinkIndicator($activeLinkIndicatorRef.current);
                }
            }
        };

        window.addEventListener('resize', handleWindowResize);
        return () => window.removeEventListener('resize', handleWindowResize);
    }, []);

    return (
        <React.Fragment>
            <animated.div style={titleContainerProps} css={S.titleContainer}>
                <S.Title>
                    <animated.div style={titleProps}>{title}</animated.div>
                </S.Title>
            </animated.div>

            <S.Header isOverlayActive={isHamburgerActive}>
                <div>
                    <animated.div style={siteLogoProps}>
                        <Link to="/" title="Home">
                            <SiteLogo />
                        </Link>
                    </animated.div>

                    <UI.HideAt breakpoint="tablet" css={S.hamburger}>
                        <animated.div style={siteLogoProps}>
                            <Hamburger
                                isActive={isHamburgerActive}
                                handleClick={handleHamburgerClick}
                            />
                        </animated.div>
                    </UI.HideAt>

                    <BackButton location={location} />
                </div>

                <S.LinksContainer ref={$linksContainerRef}>
                    {linkProps.map((props, index) => {
                        if (links[index].label !== 'My Profile') {
                            return (
                                <animated.div key={links[index].to} style={props}>
                                    <Link
                                        to={links[index].to}
                                        css={S.link}
                                        getProps={getLinkProps}
                                        onClick={handleLinkClick}
                                    >
                                        {links[index].label}
                                    </Link>
                                </animated.div>
                            );
                        } else {
                            return (
                                <animated.div key={links[index].to} style={props}>
                                    <UI.FlexLink
                                        href={links[index].to}
                                        target="noopener"
                                        css={S.smallLink}
                                    >
                                        <svg
                                            width="24"
                                            height="24"
                                            xmlns="http://www.w3.org/2000/svg"
                                        >
                                            <g fill="none" fillRule="evenodd">
                                                <circle
                                                    stroke="#000"
                                                    strokeWidth="1.5"
                                                    cx="12"
                                                    cy="12"
                                                    r="11.25"
                                                />
                                                <path
                                                    d="M9.40009 12.26624c-.5973-.65113-.96259-1.51832-.96259-2.46936 0-2.01618 1.64026-3.65625 3.65625-3.65625 2.016 0 3.65625 1.64007 3.65625 3.65625 0 .95104-.3653 1.81823-.96259 2.46936 1.74518.95599 2.93134 2.81048 2.93134 4.93689h-1.875c0-2.06782-1.68219-3.75-3.75-3.75-2.06781 0-3.75 1.68219-3.75 3.75h-1.875c0-2.1264 1.18616-3.9809 2.93134-4.9369zm2.69366-.68811c.98218 0 1.78125-.79908 1.78125-1.78125 0-.98218-.79907-1.78125-1.78125-1.78125s-1.78125.79907-1.78125 1.78125c0 .98217.79907 1.78125 1.78125 1.78125z"
                                                    stroke="#FFF"
                                                    strokeWidth=".4"
                                                    fill="#000"
                                                    fillRule="nonzero"
                                                />
                                            </g>
                                        </svg>{' '}
                                        My Profile
                                    </UI.FlexLink>
                                </animated.div>
                            );
                        }
                    })}
                    <UI.FadeIn delay={1600} style={{marginTop: 48}}>
                        <SearchInput />
                    </UI.FadeIn>
                </S.LinksContainer>

                <animated.div style={footerProps}>
                    <SiteFooter />
                </animated.div>

                <animated.div
                    ref={$activeLinkIndicatorRef}
                    style={activeLinkIndicatorProps}
                    css={S.activeLinkIndicator}
                />
            </S.Header>
            <S.HeaderPlaceholder />
        </React.Fragment>
    );
};

export default SiteHeader;

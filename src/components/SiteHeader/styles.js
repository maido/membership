/**
 * @prettier
 * @flow
 */
import {css, keyframes} from '@emotion/core';
import styled from '@emotion/styled';
import {rem} from 'polished';
import {responsiveSpacing} from '../../globals/functions';
import {breakpoints, colors, fontFamilies, spacing, transitions} from '../../globals/variables';

export const TITLE_CONTAINER_WIDTH = spacing.l;
export const NAV_CONTAINER_WIDTHS = [
    {breakpoint: breakpoints.tablet, width: 300},
    {breakpoint: breakpoints.desktop, width: 330}
];

export const Header = styled.header`
    @media (max-width: ${rem(breakpoints.tablet)}) {
        background-color: ${colors.black};
        box-shadow: 0 3px 30px 0 rgba(0, 0, 0, 0.05);
        align-items: center;
        border-bottom: 1px solid ${colors.greyDark};
        display: flex;
        height: ${rem(60)};
        overflow: hidden;
        padding-left: ${rem(spacing.m)};
        padding-right: ${rem(spacing.m)};
        position: fixed;
        transition: background-color 0.4s ease-in-out;
        width: 100%;
        z-index: 101;

        ${props =>
            props.isOverlayActive &&
            `background-color: ${colors.white}; border: 0; box-shadow: none; path { fill: ${
                colors.black
            }; }`}

        footer {
            display: none;
        }
    }

    @media (min-width: ${rem(breakpoints.tablet)}) {
        background-color: ${colors.white};
        border-right: 1px solid ${colors.greyLight};
        display: flex;
        flex-direction: column;
        height: 100vh;
        left: ${rem(TITLE_CONTAINER_WIDTH)};
        justify-content: space-between;
        padding: ${responsiveSpacing(spacing.m)} ${rem(spacing.l)};
        position: fixed;
        top: 0;
        z-index: 10;
    }

    ${NAV_CONTAINER_WIDTHS.map(
        ({breakpoint, width}) => `
        @media (min-width: ${rem(breakpoint)}) {
            width: ${rem(width)};
        }`
    ).join('')};
`;

export const HeaderPlaceholder = styled.div`
    height: ${rem(TITLE_CONTAINER_WIDTH)};

    @media (min-width: ${rem(breakpoints.tablet)}) {
        display: none;
    }
`;

export const titleContainer = css`
    background-color: ${colors.black};
    color: ${colors.white};
    height: 100vh;
    left: 0;
    pointer-events: none;
    position: fixed;
    top: 0;
    width: ${rem(TITLE_CONTAINER_WIDTH)};

    @media (max-width: ${rem(breakpoints.tablet)}) {
        display: none;
    }
`;

export const Title = styled.span`
    display: none;
    left: ${rem(28)};
    position: fixed;
    transform: rotate(-90deg) translateX(-50%);
    transform-origin: left;
    top: 50vh;
    z-index: 201;
`;

export const logo = css`
    display: block;

    svg {
        height: ${rem(30)};
        vertical-align: middle;
        width: auto;
    }

    @media (max-width: ${rem(breakpoints.tablet)}) {
        path {
            fill: ${colors.white};
        }
    }

    @media (min-width: ${rem(breakpoints.tablet)}) {
        svg {
            height: auto;
            width: 100%;
        }
    }
`;

export const hamburger = css`
    position: absolute;
    right: ${rem(spacing.m)};
    top: 50%;
    transform: translateY(-50%);
`;

export const LinksContainer = styled.div`
    padding-bottom: ${rem(spacing.m)};
    padding-top: ${rem(spacing.m)};
    width: 240px;

    @media (min-height: ${rem(700)}) {
        position: fixed;
        top: 50%;
        transform: translateY(-50%);
    }

    @media (max-width: ${rem(breakpoints.tablet)}) {
        display: none;
    }
`;

export const dotEntranceAnimation = keyframes`
    0% {
            opacity: 0;
            transform: translateY(50%) scale(0);
    }
    20% {
            opacity: 1;
            transform: translateY(50%) scale(1.3);
    }
    60% {
            opacity: 1;
            transform: translateY(50%) scale(0.7);
    }
    80% {
            opacity: 1;
            transform: translateY(50%) scale(1.1);
    }
    100% {
            opacity: 1;
            transform: translateY(50%) scale(1);
    }
`;

export const link = css`
    color: ${colors.black};
    cursor: pointer;
    display: block;
    font-family: ${fontFamilies.bold};
    font-size: ${rem(18)};
    line-height: 1;
    position: relative;
    padding: ${rem(spacing.s)} 0;
    transition: ${transitions.default};
    transition-duration: 0.1s;

    &[data-active='true'],
    &:hover,
    &:active {
        color: ${colors.primary};
    }
    &[data-active='true'] {
        // font-size: ${rem(26)};
    }
`;

export const smallLink = css`
    font-size: ${rem(14)};
    margin-top: ${rem(spacing.s)};
`;

export const activeLinkIndicator = css`
    background-color: ${colors.primary};
    border-radius: 50%;
    content: '';
    height: ${rem(6)};
    opacity: 0;
    position: absolute;
    left: ${rem(spacing.m)};
    width: ${rem(6)};

    @media (max-width: ${rem(breakpoints.tablet)}) {
        display: none;
    }
`;

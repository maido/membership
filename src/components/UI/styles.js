/**
 * @prettier
 * @flow
 */
import {css, keyframes} from '@emotion/core';
import styled from '@emotion/styled';
import {between, rem} from 'polished';
import {responsiveSpacing} from '../../globals/functions';
import {
    breakpoints,
    colors,
    fontFamilies,
    fontSizes,
    spacing,
    themeKeys,
    transitions
} from '../../globals/variables';

export const primaryText = css`
    color: ${colors.primary};
`;

export const greyText = css`
    color: ${colors.grey};
`;

export const blackText = css`
    color: ${colors.black};
`;

export const greyDarkText = css`
    color: ${colors.greyDark};
`;

export const blackLink = css`
    color: ${colors.black};

    &:hover,
    &:focus {
        color: ${colors.primary};
    }
`;

export const regularText = css`
    font-family: ${fontFamilies.default};
`;

export const leadText = css`
    font-size: ${rem(fontSizes.lead * 0.85)};
    line-height: 1.5;

    @media (min-width: ${rem(breakpoints.mobile)}) {
        font-size: ${rem(fontSizes.lead)};
        line-height: 1.6;
    }
`;

export const centerText = css`
    text-align: center;
`;

export const uppercaseText = css`
    text-transform: uppercase;
`;

export const marginBottomS = css`
    margin-bottom: ${rem(spacing.s)};
`;

export const flexAtMobile = css`
    @media (min-width: ${rem(breakpoints.mobile)}) {
        display: flex;
    }
`;

export const responsiveHorizontalPadding = css`
    padding-left: ${responsiveSpacing(spacing.m)};
    padding-right: ${responsiveSpacing(spacing.m)};
`;

//

export const Badge = styled.span`
    background-color: ${colors.greyLight};
    border-radius: ${rem(4)};
    color: ${colors.black};
    display: inline-block;
    font-size: ${rem(12)};
    font-family: ${fontFamilies.bold};
    letter-spacing: ${rem(0.5)};
    line-height: 1.2;
    padding: ${rem(4)} ${rem(spacing.xs)} ${rem(6)};
    position: relative;
    text-transform: uppercase;
    vertical-align: top;
`;

export const MemberTypeBadge = styled(Badge)`
    background-color: ${colors.greyLight};
`;

export const Divider = styled.hr`
    background-color: ${colors.greyLight};
    border: 0;
    height: 1px;
    display: block;
`;

//

export const ContentSection = styled.section`
    ${props => `
        padding-bottom: ${rem(props.size ? spacing[props.size] : spacing.m)};
        padding-top: ${rem(props.size ? spacing[props.size] : spacing.m)};
    `}

    & + & {
        padding-top: 0;
    }
`;

//

export const Flex = styled.div`
    display: flex;
`;

export const FlexWrap = styled.div`
    display: flex;
    flex-wrap: wrap;

    * {
        align-items: center;
        display: flex;
        flex-grow: 1;
    }

    * + * {
        margin-left: ${rem(spacing.m)};
    }
`;

//

const fadeInAnimation = keyframes`
    from { opacity: 0; }
    to { opacity: 1; }
`;

export const FadeIn = styled.div`
    animation: ${fadeInAnimation} 0.6s;
    animation-delay: ${props => (props.delay ? `${props.delay / 1000}s` : 0)};
    animation-fill-mode: forwards;
    opacity: 0;
    position: relative;
    transition: ${transitions.bezier};
    will-change: opacity;
`;

const fadeInUpAnimation = keyframes`
    from {
        opacity: 0;
        transform: translateY(${rem(spacing.s)});
    }
    to {
        opacity: 1;
        transform: translateY(0);
    }
`;

export const FadeInUp = styled.div`
    animation: ${fadeInUpAnimation} 0.6s;
    animation-delay: ${props => (props.delay ? `${props.delay / 1000}s` : 0)};
    animation-fill-mode: forwards;
    opacity: 0;
    position: relative;
    transform: translateY(${rem(spacing.m)});
    transition: ${transitions.bezier};
    will-change: transform;
`;

//

export const FlexLink = styled.a`
    align-items: center;
    color: ${colors.black};
    font-family: ${fontFamilies.bold};
    font-size: ${rem(fontSizes.default)};
    display: flex;

    img,
    svg {
        margin-right: ${rem(spacing.s)};

        &,
        * {
            transition: ${transitions.default};
        }
    }

    &:hover,
    &:focus {
        color: ${colors.primary};

        img,
        svg {
            transform: scale(1.1);
        }

        path {
            fill: ${colors.primary};
        }
        circle {
            stroke: ${colors.primary};
        }
    }
`;

//

export const GreyBox = styled.div`
    background-color: ${colors.greyLightest};
    padding: ${rem(spacing.m)};
`;

//

export const HorizontalList = styled.ul`
    list-style: none;
    margin: 0;
    padding: 0;

    > li {
        display: inline-block;
    }

    > li + li {
        margin-left: ${rem(spacing.s)};
    }

    ${props =>
        props.blockAtMobile &&
        `
        @media (max-width: ${rem(breakpoints.mobile)}) {
            > li:nth-child(1) {
                display: block;
            }
            > li:nth-child(2) {
                margin-left: 0;
            }
        }
    `}

    ${props =>
        props.flex &&
        `
        &, > li {
            align-items:center;
            display: flex;
        }
    `}
`;

//

const Heading = css`
    font-family: ${fontFamilies.heading};
    font-weight: 300;
    line-height: 1.1;
    margin-bottom: ${rem(spacing.m)};
    margin-top: 0;
`;

export const Heading1 = styled.h1`
    ${Heading};
    font-size: ${rem(fontSizes.h1 * 0.7)};

    @media (min-width: ${props => rem(breakpoints.tablet)}) {
        font-size: ${rem(fontSizes.h1)};
        line-height: 1.05;
    }
`;

export const Heading2 = styled.h2`
    ${Heading};
    font-size: ${rem(fontSizes.h2 * 0.75)};

    @media (min-width: ${props => rem(breakpoints.mobile)}) {
        font-size: ${rem(fontSizes.h2)};
    }

    @media (min-width: ${props => rem(breakpoints.desktop)}) {
        font-size: 6vmin;
    }
`;

export const Heading3 = styled.h3`
    ${Heading};
    font-size: ${rem(fontSizes.h3)};
`;

export const Heading4 = styled.h4`
    ${Heading};
    font-size: ${rem(fontSizes.h4)};

    ${props => props.primary && `color: ${colors.primary};`}
`;

export const Heading5 = styled.h5`
    ${Heading};
    font-family: ${fontFamilies.bold};
    font-size: ${rem(fontSizes.h5)};
    font-weight: 800;
`;

export const Heading6 = styled.h6`
    ${Heading};
    font-family: ${fontFamilies.bold};
    font-size: ${rem(fontSizes.h6)};
    font-weight: 800;
`;

//

export const label = css`
    color: ${colors.grey};
    font-family: ${fontFamilies.bold};
    font-size: ${rem(13)};
    letter-spacing: ${rem(0.75)};
    text-transform: uppercase;
`;

export const Label = styled.span`
    ${label}
`;

//

export const LayoutContainer = styled.div`
    display: block;
    margin: 0;
    padding: 0;
    list-style: none;
    margin-left: ${rem(spacing.m * -1)};

    ${props =>
        props.type === 'matrix' &&
        `> div {
            margin-bottom: ${rem(spacing.l)} !important;
        }

        @media (min-width: ${rem(breakpoints.mobile)}) {
            > div {
                margin-bottom: 6vmax !important;
            }

            > div:nth-child(-n+3) {
                margin-bottom: 3vmax;
            }

            > div:nth-child(3n+2) {
                transform: translateY(4vmax);
            }

            > div:nth-child(3n+3) {
                transform: translateY(2vmax);
            }
        }
        `};

    ${props =>
        props.size &&
        `@media (min-width: ${rem(breakpoints.mobile)}) {
            margin-left: ${rem(spacing[props.size] * -1)};

            > div {
                padding-left: ${rem(spacing[props.size])};
            }
        }
        `};

    ${props =>
        props.flush &&
        `margin-left: 0;

            > div {
                padding-left: 0;
            }
        `};

    ${props =>
        props.stack &&
        `@media (max-width: ${rem(breakpoints.mobile)}) {
            > div + div {
                margin-top: ${rem(spacing.m)};
            }
        }
        `};
`;

const layoutWidth = size => (size * 100).toFixed(2);

export const LayoutItem = styled.div`
    box-sizing: border-box;
    display: inline-block;
    vertical-align: top;
    padding-left: ${rem(spacing.m)};
    width: ${props => (props.size ? `${layoutWidth(props.size)}%` : '100%')};

    ${props =>
        props.sizeAtMobileSmall &&
        `@media (min-width: ${rem(breakpoints.mobileSmall)}) {
        width: ${layoutWidth(props.sizeAtMobileSmall)}%;
    }`}

    ${props =>
        props.sizeAtMobile &&
        `@media (min-width: ${rem(breakpoints.mobile)}) {
        width: ${layoutWidth(props.sizeAtMobile)}%;
    }`}

    ${props =>
        props.sizeAtTablet &&
        `@media (min-width: ${rem(breakpoints.tablet)}) {
        width: ${layoutWidth(props.sizeAtTablet)}%;
    }`}

    ${props =>
        props.sizeAtDesktop &&
        `@media (min-width: ${rem(breakpoints.desktop)}) {
        width: ${layoutWidth(props.sizeAtDesktop)}%;
    }`}

    ${props =>
        props.sizeAtDesktopLarge &&
        `@media (min-width: ${rem(breakpoints.desktopLarge)}) {
        width: ${layoutWidth(props.sizeAtDesktopLarge)}%;
    }`}

    ${props =>
        props.isBlank &&
        `@media (max-width: ${rem(breakpoints.tablet)}) {
        display: none;
    }`}
`;

//

export const rteContent = props => css`
    // > p:first-of-type {
    //     font-size: ${rem(18)};
    //     letter-spacing: ${rem(-0.05)};
    //     line-height: 1.6;
    // }

    ol,
    ul {
        padding-left: ${rem(spacing.m)};
    }

    blockquote {
        border-left: ${rem(3)} solid ${colors.greyLight};
        padding-left: ${responsiveSpacing(spacing.m)};
        font-family: ${fontFamilies.bold};
        font-size: ${rem(fontSizes.h4)};
        line-height: 1.6;
        margin: ${responsiveSpacing(spacing.l)} auto;
        position: relative;

        &::before {
            background-image: url("data:image/svg+xml;charset=UTF-8,%3csvg width='19' height='15' xmlns='http://www.w3.org/2000/svg'%3e%3cpath d='M8.66022 11.1032C8.66022 13.18505 7.03315 15 4.5663 15 1.78453 15 0 12.70463 0 9.87544 0 2.72242 4.82873.32028 8.13536 0v3.25623c-1.99448.32028-4.1989 1.97509-4.30387 4.4306.26243-.16014.62984-.2669 1.10221-.2669 2.36188 0 3.72652 1.44128 3.72652 3.68327zm10.33978 0C19 13.18505 17.37293 15 14.90608 15c-2.78177 0-4.5663-2.29537-4.5663-5.12456 0-7.15302 4.82873-9.55516 8.13536-9.87544v3.25623c-1.99448.32028-4.1989 1.97509-4.30387 4.4306.26243-.16014.62983-.2669 1.10221-.2669C17.63536 7.41993 19 8.8612 19 11.1032z' fill='%23FF534B' fill-rule='nonzero'/%3e%3c/svg%3e");
            background-size: 19px 15px;
            background-repeat: no-repeat;
            content: '';
            display: block;
            height: 15px;
            margin-bottom: ${rem(spacing.m)};
            width: 19px;
        }
    }

    @media (min-width: ${rem(breakpoints.mobile)}) {
ol,
    ul {
        padding-left: ${rem(spacing.l)};
    }
    }
`;

//

export const Spacer = styled.div`
    height: ${props => rem(spacing[props.size ? props.size : 'm'])};
    width: ${props => rem(spacing[props.size ? props.size : 'm'])};

    ${props =>
        props.sizeAtMobile &&
        `
        @media (min-width: ${rem(breakpoints.mobile)}) {
            height: ${rem(spacing[props.sizeAtMobile])};
            width: ${rem(spacing[props.sizeAtMobile])};
        }
        `}
`;

export const ResponsiveSpacer = styled.div`
    height: ${props => responsiveSpacing(spacing[props.size ? props.size : 'm'])};
    width: ${props => responsiveSpacing(spacing[props.size ? props.size : 'm'])};
`;

//

export const StickyAtMobile = styled.div`
    @media (min-width: ${rem(breakpoints.mobile)}) {
        position: sticky;
        top: ${rem(spacing.m)};
    }
`;

//

export const ThemeKey = styled.div`
    background-color: ${colors.greyLight};
    border-radius: 100%;
    display: inline-block;
    height: ${rem(12)};
    margin-left: ${rem(spacing.xs)};
    overflow: hidden;
    width: ${rem(12)};

    ${props => props.themeKey && `background-color: ${themeKeys[props.themeKey]};`}
`;

//

export const HideAt = styled.div`
    @media (min-width: ${props => rem(breakpoints[props.breakpoint])}) {
        display: none;
    }
`;

export const ShowAt = styled.div`
    @media (max-width: ${props => rem(breakpoints[props.breakpoint])}) {
        display: none;
    }
`;

export const VisuallyHidden = styled.div`
    border: 0 !important;
    clip: rect(0 0 0 0) !important;
    clip-path: inset(50%) !important;
    height: 1px !important;
    margin: -1px !important;
    overflow: hidden !important;
    padding: 0 !important;
    position: absolute !important;
    white-space: nowrap !important;
    width: 1px !important;
`;

/**
 * @prettier
 * @flow
 */
import {css} from '@emotion/core';
import styled from '@emotion/styled';
import {rem} from 'polished';
import {breakpoints, fontFamilies, spacing} from '../../globals/variables';

export const heading = css`
    font-family: ${fontFamilies.bold};
    text-transform: uppercase;
`;

export const ImagesList = styled.ul`
    align-items: center;
    display: flex;
    flex-wrap: wrap;
    list-style: none;
    margin: 0;
    margin-bottom: ${rem(spacing.m * -1)};
    padding: 0;
`;

export const ImageContainer = styled.li`
    display: inline-block;
    margin-bottom: ${rem(spacing.m)};
    max-width: 33.3333%;
    padding-left: ${rem(spacing.s)};
    padding-right: ${rem(spacing.s)};
    width: 100%;

    @media (min-width: ${rem(breakpoints.mobile)}) {
        max-width: ${rem(180)};
    }
`;

export const image = css`
    width: 100%;
`;

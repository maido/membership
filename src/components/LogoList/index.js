/**
 * @prettier
 * @flow
 */
import React, {Fragment} from 'react';
import Image from 'gatsby-image';
import * as UI from '../UI/styles';
import * as S from './styles';

type Props = {
    images: Array<Object>,
    title?: string
};

const LogoList = ({images = [], title}: Props) => {
    return (
        <Fragment>
            {title && <UI.Heading4 css={S.heading}>{title}</UI.Heading4>}

            <S.ImagesList>
                {images.map(image => (
                    <S.ImageContainer key={image.fluid.src}>
                        <Image fluid={image.fluid} alt="" css={S.image} />
                    </S.ImageContainer>
                ))}
            </S.ImagesList>
        </Fragment>
    );
};

export default LogoList;

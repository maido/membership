/**
 * @prettier
 */
import {keyframes} from '@emotion/core';
import styled from '@emotion/styled';
import {rem} from 'polished';
import {spacing, transitions} from '../../globals/variables';

const fadeInUpSkewAnimation = keyframes`
    from {
        opacity: 0;
        transform: translateY(${rem(spacing.s)});
    } 
    to {
        opacity: 1;
        transform: translateY(0);
    }
`;

export const FadeInUpSkew = styled.span`
    animation: ${fadeInUpSkewAnimation} 0.6s;
    animation-delay: ${props => (props.delay ? `${props.delay / 1000}s` : 0)};
    animation-fill-mode: forwards;
    opacity: 0;
    position: relative;
    transform: translateY(${rem(spacing.m)});
    transition: ${transitions.bezier};
    will-change: transform;
`;

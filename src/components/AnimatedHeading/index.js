/**
 * @prettier
 * @flow
 */
import React from 'react';
import * as UI from '../UI/styles';
import * as S from './styles';

type Props = {title: string};

const AnimatedHeading = ({title}: Props) => (
    <UI.Heading1 title={title} ariaHidden="true">
        {title.split('').map((c, i) => (
            <S.FadeInUpSkew
                style={{
                    display: 'inline-block',
                    visibility: c === ' ' ? 'hidden' : 'visible'
                }}
                key={`${c}-${i}`}
                delay={i * 30}
            >
                {c === ' ' ? '.' : c}
            </S.FadeInUpSkew>
        ))}
    </UI.Heading1>
);

export default AnimatedHeading;

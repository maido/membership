// @flow
import {css} from '@emotion/core';
import styled from '@emotion/styled';
import {rem} from 'polished';
import {colors, transitions} from '../../globals/variables';

export const VideoContainer = styled.div`
    background: ${colors.black};
    height: 0;
    max-width: 100%;
    overflow: hidden;
    padding-bottom: 56.25%;
    position: relative;
    width: 100%;
`;

const fill = css`
    border: 0;
    height: 100%;
    left: 0;
    position: absolute;
    top: 0;
    width: 100%;
`;

export const IframeContainer = styled.div`
    iframe,
    iframe object,
    iframe embed {
        ${fill};
    }
`;

export const PlayIcon = styled.svg`
    background-color: ${colors.white};
    border: 2px solid #ff0000;
    border-radius: 100%;
    height: auto;
    max-width: 100px;
    overflow: hidden;
    transition: ${transitions.default};
    width: 30vmin;

    path {
        fill: #ff0000;
    }
`;

export const Thumbnail = styled.button`
    ${fill};
    align-items: center;
    background-image: url('${props => `${props.image}`}');
    background-position: center center;
    background-repeat: no-repeat;
    background-size:cover;
    display: flex;
    justify-content: center;
    transition: ${transitions.default};

    &:hover ${PlayIcon},
    &:focus ${PlayIcon} {
        transform:scale(1.1);
    }
`;

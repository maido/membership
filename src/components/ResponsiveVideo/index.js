// @flow
import React, {useState} from 'react';
import {getYouTubeIdFromUrl} from '../../globals/functions';
import * as S from './styles';

type Props = {
    autoplay?: boolean,
    url: string
};

const ResponsiveVideo = ({autoplay = false, url}: Props) => {
    const [isEmbedded, setIsEmbedded] = useState(autoplay);
    const videoId = getYouTubeIdFromUrl(url);

    return (
        <S.VideoContainer>
            {!isEmbedded && (
                <S.Thumbnail
                    onClick={() => setIsEmbedded(true)}
                    image={`https://img.youtube.com/vi/${videoId}/maxresdefault.jpg`}
                >
                    <S.PlayIcon
                        height="20"
                        width="20"
                        xmlns="http://www.w3.org/2000/svg"
                        viewBox="0 0 20 20"
                    >
                        <path
                            d="M10 0C4.5 0 0 4.5 0 10s4.5 10 10 10 10-4.5 10-10S15.5 0 10 0zM8 14.5v-9l6 4.5-6 4.5z"
                            fill="#000"
                            fillRule="evenodd"
                        />
                    </S.PlayIcon>
                </S.Thumbnail>
            )}
            {isEmbedded && (
                <S.IframeContainer>
                    <iframe
                        width="100%"
                        height="auto"
                        src={`https://www.youtube-nocookie.com/embed/${videoId}?autoplay=1&rel=0`}
                        frameborder="0"
                        allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                        allowfullscreen="true"
                    />
                </S.IframeContainer>
            )}
        </S.VideoContainer>
    );
};

export default ResponsiveVideo;

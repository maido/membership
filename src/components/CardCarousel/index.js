/**
 * @prettier
 * @flow
 */
import * as React from 'react';
import {Link} from 'gatsby';
import * as UI from '../UI/styles';
import * as S from './styles';

const Flickity = typeof window !== 'undefined' ? require('react-flickity-component') : () => null;

type Props = {
    children: React.Node,
    label?: string,
    showNav?: boolean,
    viewAll?: string
};

const CardCarousel = ({children, label, showNav = true, viewAll}: Props) => {
    const flickityOptions = {
        cellAlign: 'left',
        draggable: false,
        pageDots: false,
        prevNextButtons: showNav
    };

    return (
        <S.Container>
            <S.Header>
                {viewAll && (
                    <Link to={viewAll}>
                        <UI.Heading4 as="h2" css={S.heading}>
                            {label}
                        </UI.Heading4>
                    </Link>
                )}
                {!viewAll && (
                    <UI.Heading4 as="h2" css={S.heading}>
                        {label}
                    </UI.Heading4>
                )}

                {viewAll && (
                    <Link to={viewAll} css={[S.buttonLabel, !showNav && S.buttonLabelFlush]}>
                        <UI.Label>View all</UI.Label>
                    </Link>
                )}
            </S.Header>

            {Flickity && <Flickity options={flickityOptions}>{children}</Flickity>}
        </S.Container>
    );
};

export default CardCarousel;

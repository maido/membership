/**
 * @prettier
 * @flow
 */
import {css} from '@emotion/core';
import styled from '@emotion/styled';
import {rem} from 'polished';
import {Label as UILabel} from '../UI/styles';
import {responsiveSpacing} from '../../globals/functions';
import {breakpoints, colors, spacing, transitions} from '../../globals/variables';

export const Container = styled.div`
     min-height: ${rem(200)};
    // position: relative;
    // max-width: 100%;

    .flickity-enabled {
        position: relative;
    }

    .flickity-enabled:focus {
        outline: none;
    }

    .flickity-viewport {
        overflow: hidden;
        position: relative;
        height: 100%;
    }

    .flickity-slider {
        position: absolute;
        width: 100%;
        height: 100%;
    }

    /* draggable */

    .flickity-enabled.is-draggable {
        user-select: none;
    }

    .flickity-enabled.is-draggable .flickity-viewport {
        cursor: move;
        cursor: grab;
    }

    .flickity-enabled.is-draggable .flickity-viewport.is-pointer-down {
        cursor: grabbing;
    }

    .flickity-button {
        position: absolute;
        background: hsla(0, 0%, 100%, 0.75);
        border: none;
        color: #333;
    }

    .flickity-button:active {
        opacity: 0.6;
    }

    .flickity-button:disabled {
        opacity: 0.15;
        cursor: auto;
        /* prevent disabled button from capturing pointer up event. #716 */
        pointer-events: none;
    }

    .flickity-button-icon {
        fill: currentColor;
    }

    /* ---- previous/next buttons ---- */

    .flickity-prev-next-button.previous {
        right: ${rem(spacing.l)};

        &:hover,
        &:focus {
            transform: scale(1.1);
        }
    }

    .flickity-prev-next-button.next {
        right: 0;
        transform: rotate(180deg);

        &:hover,
        &:focus {
            transform: scale(1.1) rotate(180deg);
        }
    }

    .flickity-button-icon {
        display: none;
    }

    .flickity-prev-next-button {
        background-color: transparent;
        background-repeat: no-repeat;
        height: ${rem(40)};
        top: ${rem(-56)};
        transition: ${transitions.default};
        width: ${rem(40)};
        z-index: 10;
    }

    @media (min-width: ${rem(breakpoints.mobile)}) {
        min-height: ${rem(400)};

        .flickity-prev-next-button.previous {
            right: ${rem(spacing.l + spacing.xs)};
        }
    }

    @media (max-width: ${rem(breakpoints.mobile)}) {
        .flickity-prev-next-button {
            // top: ${rem(spacing.l * 1.225 * -1)};
        }

        .flickity-viewport,
        .flickity-slider {
            width: calc(100% + ${rem(spacing.m)});
        }
    }

    .flickity-prev-next-button {
        background-image: url("data:image/svg+xml,%3Csvg width='40' height='40' viewBox='0 0 40 40' xmlns='http://www.w3.org/2000/svg' %3E%3Cg transform='translate(.50943 1)' fill='none' fill-rule='evenodd'%3E%3Cellipse stroke='%23000' transform='matrix(-1 0 0 1 38.49057 0)' cx='19.24528' cy='19' rx='19.24528' ry='19' /%3E%3Cpath d='M16.4807 14.39287c.18571-.1905.4948-.1905.68701 0 .18572.18407.18572.4904 0 .67404l-3.48356 3.45256h11.49527c.26797.00042.48096.21152.48096.4771 0 .26558-.21299.48354-.48096.48354H13.68415l3.48356 3.44612c.18572.1905.18572.49726 0 .6809-.1922.1905-.50173.1905-.68701 0l-4.30824-4.2699c-.19221-.18406-.19221-.4904 0-.67403l4.30824-4.27033z' fill='%23000' fill-rule='nonzero' /%3E%3C/g%3E%3C/svg%3E");
    }
`;

export const Header = styled.div`
    margin-bottom: ${rem(spacing.m)};

    @media (max-width: ${rem(breakpoints.mobile)}) {
        h2 {
            max-width: calc(100% - ${rem(100)}) !important;
        }
    }

    @media (min-width: ${rem(breakpoints.mobile)}) {
        align-items: center;
        display: flex;
    }
`;

export const heading = css`
    color: ${colors.black};
    margin: 0;
`;

export const Nav = styled.div`
    align-items: center;
    display: flex;
    flex-grow: 1;
    justify-content: flex-end;
    position: relative;
    text-align: right;
    z-index: 10;
`;

export const buttonLabel = css`
    margin-top: ${rem(spacing.s)};

    > ${UILabel} {
        color: ${colors.primary};
        transition: ${transitions.default};

        &:hover,
        &:focus {
            color: ${colors.black};
        }
    }

    @media (min-width: ${rem(breakpoints.mobile)}) {
        margin-top: 0;
        position: absolute;
        right: ${rem(80 + spacing.s * 6)};
    }
`;

export const buttonLabelFlush = css`
    right: ${responsiveSpacing(spacing.m)} !important;
`;

export const Icon = styled.svg`
    height: ${rem(36)};
    width: ${rem(36)};
`;

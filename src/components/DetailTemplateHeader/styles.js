/**
 * @prettier
 * @flow
 */
import {css} from '@emotion/core';
import styled from '@emotion/styled';
import {rem} from 'polished';
import {responsiveSpacing} from '../../globals/functions';
import {
    breakpoints,
    colors,
    fontFamilies,
    fontSizes,
    spacing,
    transitions
} from '../../globals/variables';

export const container = css`
    line-height: 1.2;
    width: 100%;

    @media (min-width: ${rem(breakpoints.tablet)}) {
        align-items: center;
        display: flex;
    }
`;

export const image = css`
    max-width: 100%;
`;

export const Title = styled.span`
    color: ${colors.black};
    display: block;
    flex-grow: 1;
    font-size: ${rem(fontSizes.h5)};

    ${props => props.size === 'medium' && `font-size: ${rem(fontSizes.h4)};`};
`;

export const Subtitle = styled.span`
    color: ${colors.grey};
    display: block;
    font-family: ${fontFamilies.bold};
    font-size: ${rem(fontSizes.h6)};
    letter-spacing: ${rem(0.5)};
    margin-top: ${rem(spacing.s)};
    text-transform: uppercase;
`;

export const Content = styled.div`
    flex-grow: 1;
`;

export const Description = styled.p`
    color: ${colors.greyDark};
    line-height: 1.4;
    margin-top: ${rem(spacing.s)};
`;

export const Link = styled.a`
    color: ${colors.grey};

    &:hover,
    &:focus {
        color: ${colors.black};
    }
`;

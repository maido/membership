/**
 * @prettier
 * @flow
 */
import * as React from 'react';
import Image from 'gatsby-image';
import {useSpring, animated} from 'react-spring/web.cjs';
import IconLinkList from '../IconLinkList';
import {urlLabel} from '../../globals/functions';
import * as UI from '../UI/styles';
import * as S from './styles';

type Props = {
    badge?: string,
    bio?: string,
    children?: React.Node,
    city?: string,
    image?: Object,
    memberType?: string,
    name?: string,
    websiteUrl?: string,
    facebookUrl?: string,
    twitterUrl?: string,
    instagramUrl?: string,
    youtubeUrl?: string
};

const DetailTemplateHeader = ({
    badge,
    bio,
    children,
    city,
    image,
    memberType,
    name,
    websiteUrl,
    facebookUrl,
    twitterUrl,
    instagramUrl,
    youtubeUrl
}: Props) => {
    const imageProps = useSpring({
        config: {mass: 1, tension: 150, friction: 30},
        from: {opacity: 0, transform: 'scale(1.1) translateY(30px) rotateX(30deg)'},
        to: {opacity: 1, transform: 'scale(1) translateY(0) rotateX(0)'}
    });
    const nameProps = useSpring({
        config: {mass: 1, tension: 150, friction: 30},
        delay: 300,
        from: {opacity: 0, transform: 'scale(1.1) translateY(30px) rotateX(30deg)'},
        to: {opacity: 1, transform: 'scale(1) translateY(0) rotateX(0)'}
    });
    const contentProps = useSpring({
        config: {mass: 1, tension: 150, friction: 30},
        delay: 600,
        from: {opacity: 0, transform: 'translateY(20px)'},
        to: {opacity: 1, transform: 'translateY(0)'}
    });
    const socialListProps = useSpring({
        config: {mass: 1, tension: 150, friction: 30},
        delay: 800,
        from: {opacity: 0, transform: 'translateY(20px)'},
        to: {opacity: 1, transform: 'translateY(0)'}
    });

    return (
        <UI.LayoutContainer css={S.container} size="l" stack>
            {image && (
                <UI.LayoutItem sizeAtMobile={6 / 12}>
                    <animated.div style={imageProps}>
                        <Image fluid={image} css={S.image} />
                    </animated.div>
                </UI.LayoutItem>
            )}
            <UI.LayoutItem sizeAtMobile={image ? 6 / 12 : 1 / 1}>
                <animated.div style={nameProps}>
                    <UI.Heading1>{name}</UI.Heading1>

                    {(badge || memberType) && (
                        <UI.HorizontalList flex={true}>
                            {memberType && (
                                <li>
                                    <UI.MemberTypeBadge>{memberType}</UI.MemberTypeBadge>
                                </li>
                            )}
                            {badge && (
                                <li>
                                    <UI.Badge>{badge}</UI.Badge>
                                </li>
                            )}
                        </UI.HorizontalList>
                    )}
                </animated.div>

                {(city || websiteUrl) && (
                    <animated.div style={contentProps}>
                        <UI.Spacer size="s" />
                        <UI.Label>
                            {city}

                            {city && websiteUrl && <span>&nbsp;|&nbsp;</span>}

                            {websiteUrl && (
                                <S.Link href={websiteUrl} target="noopener">
                                    {urlLabel(websiteUrl)}
                                </S.Link>
                            )}
                        </UI.Label>
                    </animated.div>
                )}

                {(facebookUrl || instagramUrl || twitterUrl || youtubeUrl) && (
                    <React.Fragment>
                        <UI.Spacer />

                        <animated.div style={socialListProps}>
                            <IconLinkList
                                facebookUrl={facebookUrl}
                                instagramUrl={instagramUrl}
                                twitterUrl={twitterUrl}
                                youtubeUrl={youtubeUrl}
                            />
                        </animated.div>
                    </React.Fragment>
                )}

                {children && (
                    <React.Fragment>
                        <UI.Divider style={{margin: '24px 0'}} />
                        {children}
                    </React.Fragment>
                )}
            </UI.LayoutItem>
        </UI.LayoutContainer>
    );
};

export default DetailTemplateHeader;

/**
 * @prettier
 */
import styled from '@emotion/styled';
import {rem} from 'polished';
import {button} from '../CTAButton/styles';
import {breakpoints, colors, fontFamilies, spacing} from '../../globals/variables';

export const Container = styled.div`
    ul {
        list-style: none;
        padding: 0;
        text-align: center;
    }

    li {
        display: inline-block;
    }
    li + li {
        margin-left: ${rem(spacing.xs)};
    }
    li.active a {
        background-color: ${colors.primary};
        color: ${colors.white};
        pointer-events: none;
    }
    li.disabled a {
        pointer-events: none;
        opacity: 0.3;
    }

    a {
        ${button}
        background-color: transparent;
        color: ${colors.black};
        font-weight: ${fontFamilies.bold};
        padding: ${rem(10)} ${rem(16)};
    }
    a:hover,
    a:focus {
        background-color: ${colors.greyLight};
    }

    .simple a {
        background-color: transparent;
        color: ${colors.black};
        padding: ${rem(10)} ${rem(6)};
    }

    @media (min-width: ${rem(breakpoints.mobile)}) {
        ul {
            text-align: center;
        }
    }
`;

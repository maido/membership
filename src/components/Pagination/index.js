/**
 * @prettier
 * @flow
 */
import React from 'react';
import ReactPagination from 'react-js-pagination';
import * as S from './styles';
import * as UI from '../UI/styles';

type Props = {
    activePage: number,
    currentItems: number,
    itemsCountPerPage: number,
    onChange: Function,
    pageRangeDisplayed: number,
    skip?: number,
    totalItemsCount: number,
    type?: string
};

const Pagination = ({
    activePage,
    currentItems,
    itemsCountPerPage,
    onChange,
    pageRangeDisplayed,
    skip = 0,
    totalItemsCount,
    type = 'posts'
}: Props) => (
    <S.Container>
        <UI.FadeInUp style={{margin: '24px 0 12px', textAlign: 'center'}}>
            <UI.Label>
                Showing {skip + 1}-{skip + currentItems} of {totalItemsCount} {type}
            </UI.Label>
        </UI.FadeInUp>

        {totalItemsCount > itemsCountPerPage && (
            <ReactPagination
                pageRangeDisplayed={pageRangeDisplayed}
                activePage={activePage}
                itemsCountPerPage={itemsCountPerPage}
                totalItemsCount={totalItemsCount}
                onChange={onChange}
                itemClassFirst="simple"
                itemClassLast="simple"
                itemClassNext="simple"
                itemClassPrev="simple"
            />
        )}
    </S.Container>
);

export default Pagination;

/**
 * @prettier
 * @flow
 */
import React, {useEffect, useRef} from 'react';
import smoothScrollIntoView from 'smooth-scroll-into-view-if-needed';
import * as S from './styles';

type Props = {
    activeMonth?: number,
    handleClick: Function,
    items: Array<string>
};

const MonthSelector = ({activeMonth = 0, handleClick, items = []}: Props) => {
    const $containerRef = useRef(null);
    const $allButtonRef = useRef(null);

    const setActiveMonthPosition = ($element: HTMLElement) => {
        if ($element) {
            smoothScrollIntoView($element, {behavior: 'smooth', inline: 'center'});
        }
    };

    /**
     * Used for when 'reset' is used in parent component
     */
    if (activeMonth === 0 && $allButtonRef && $allButtonRef.current) {
        setActiveMonthPosition($allButtonRef.current);
    }

    const getPositionType = (itemIndex, activeIndex) => {
        if (activeIndex === itemIndex) {
            return 'primary';
        } else if (activeIndex - 1 === itemIndex || activeIndex + 1 === itemIndex) {
            return 'secondary';
        } else if (activeIndex - 2 === itemIndex || activeIndex + 2 === itemIndex) {
            return 'tertiary';
        }
    };

    const handleMonthClick = (event: UIEvent, month) => {
        const $target = ((event.target: any): HTMLButtonElement);

        if ($target) {
            handleClick(month);
            setActiveMonthPosition($target);
        }
    };

    useEffect(() => {
        /**
         * Needs some work...
         */
        if ($containerRef.current) {
            const $defaultMonth = $containerRef.current.querySelector(
                `[data-month-index="${activeMonth}"]`
            );

            if ($defaultMonth) {
                $defaultMonth.click();
            }
        }
    }, []);

    return (
        <S.Container ref={$containerRef}>
            <S.HiddenScrollContainer>
                <S.ScrollContainer>
                    {items.map((month, index) => (
                        <S.Button
                            type="button"
                            position={getPositionType(activeMonth, index)}
                            key={month}
                            data-month-index={index}
                            ref={month === 'All' ? $allButtonRef : null}
                            onClick={event => handleMonthClick(event, month)}
                        >
                            <S.TextWrapper>{month}</S.TextWrapper>
                        </S.Button>
                    ))}
                </S.ScrollContainer>
            </S.HiddenScrollContainer>
        </S.Container>
    );
};

export default MonthSelector;

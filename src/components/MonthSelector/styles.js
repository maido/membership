/**
 * @prettier
 * @flow
 */
import styled from '@emotion/styled';
import {rem} from 'polished';
import {breakpoints, colors, fontFamilies, spacing, transitions} from '../../globals/variables';
import {responsiveSpacing} from '../../globals/functions';
import {FIXED_HEADER_HEIGHT} from '../FixedHeader/styles';

export const Container = styled.div`
    height: ${rem(FIXED_HEADER_HEIGHT.medium - 20)};
    max-width: 100%;
    position: relative;
    z-index: 99;

    @media (max-width: ${rem(breakpoints.tablet)}) {
        height: ${rem(FIXED_HEADER_HEIGHT.medium - 20 * 0.8)};
    }
`;

export const HiddenScrollContainer = styled.div`
    height: ${rem(FIXED_HEADER_HEIGHT.medium - 20)};
    overflow-x: scroll;
    overflow-y: hidden;
    position: relative;
    -webkit-overflow-scrolling: touch;
    width: auto;
`;

export const ScrollContainer = styled.div`
    align-items: center;
    background-color: ${colors.white};
    display: flex;
    height: ${rem(FIXED_HEADER_HEIGHT.medium - 20)};
    width: 100%;

    @media (max-width: ${rem(breakpoints.tablet)}) {
        height: ${rem(FIXED_HEADER_HEIGHT.medium - 20 * 0.8)};
    }
`;

export const TextWrapper = styled.span`
    align-self: center;
    display: flex;
`;

export const Button = styled.button`
    align-self: center;
    color: ${colors.greyLight};
    display: flex;
    font-family: ${fontFamilies.bold};
    font-size: ${rem(22)};
    height: ${rem(FIXED_HEADER_HEIGHT.medium)};
    justify-content: center;
    padding: 0 ${responsiveSpacing(spacing.m)};
    position: relative;
    transition: ${transitions.default};
    vertical-align: middle;

    &::after {
        background-color: ${colors.primary};
        border-radius: 50%;
        top: ${rem(FIXED_HEADER_HEIGHT.medium * 0.65)};
        content: '';
        height: ${rem(4)};
        opacity: 0;
        position: absolute;
        left: 50%;
        transform: translateY(${rem(spacing.m)}) scale(0);
        transition: ${transitions.default};
        transition-duration: .4s;
        width: ${rem(4)};
    }

    &:hover,
    &:focus {
        color: ${colors.greyDark};
    }

    @media (max-width: ${rem(breakpoints.tablet)}) {
        &::after {
            top: ${rem(FIXED_HEADER_HEIGHT.medium * 0.625)};
        }
    }

    @media (min-width: ${rem(breakpoints.mobile)}) {
        &::after {
            bottom: ${rem(2)};
        }
    }

    ${props =>
        props.position &&
        props.position === 'primary' &&
        `
        color: ${colors.black};
        pointer-events: none;
        transform: scale(1.6);

        &::after {
            opacity: 1;
            transform: translateY(0) scale(1);
            transition-delay: .1s;
        }
    `}

    ${props =>
        props.position &&
        props.position === 'secondary' &&
        `
        color: ${colors.greyDark};
        transition-delay: .03s;
    `}

    ${props =>
        props.position &&
        props.position === 'tertiary' &&
        `
        color: ${colors.grey};
        transition-delay: .05s;
    `}
`;

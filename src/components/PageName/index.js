/**
 * @prettier
 * @flow
 */
import * as React from 'react';
import {Link} from 'gatsby';
import kebabCase from 'lodash/kebabCase';
import * as UI from '../UI/styles';

type Props = {title?: string};

const PageName = ({title = ''}: Props) => (
    <UI.HideAt breakpoint="mobile" css={UI.centerText}>
        <UI.Spacer />
        <Link to={`/${kebabCase(title.replace('&', 'and'))}/`}>
            <UI.Label css={UI.primaryText}>{title}</UI.Label>
        </Link>
    </UI.HideAt>
);

export default PageName;

/**
 * @prettier
 * @flow
 */
import * as React from 'react';
import Layout from '../../layouts';

type Props = {children: React.Node};

const LayoutContainer = ({children}: Props) => {
    if (process.env.NODE_ENV === 'development') {
        return <Layout>{children}</Layout>;
    } else {
        return children;
    }
};

export default LayoutContainer;

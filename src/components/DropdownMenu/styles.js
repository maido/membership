/**
 * @prettier
 * @flow
 */
import {css, keyframes} from '@emotion/core';
import styled from '@emotion/styled';
import {rem} from 'polished';
import {breakpoints, colors, fontFamilies, spacing, transitions} from '../../globals/variables';

const menuListEntranceAnimation = keyframes`
    0% {
        opacity: 0;
        transform: translateY(-20px) scale(0.85);
    }
    100% {
        opacity: 1;
        transform: translateY(0) scale(1);
    }
`;

export const menuButton = css`
    align-items: center;
    display: flex;
    font-family: ${fontFamilies.bold};
    font-size: ${rem(24)};
    line-height: 1.2;
    position: relative;
    text-align: left;
    z-index: 10000;

    svg {
        margin-left: ${rem(spacing.s)};
        transition: ${transitions.default};
        vertical-align: middle;
    }

    &:hover svg,
    &:focus svg {
        transform: scale(1.3);
    }

    &[aria-expanded='true'] svg {
        transform: rotate(-180deg);
    }

    @media (min-width: ${rem(breakpoints.tablet)}) {
        font-size: ${rem(30)};
    }
`;

export const menuList = css`
    animation: ${menuListEntranceAnimation} 0.5s;
    border: 1px solid ${colors.greyLight};
    box-shadow: 0 4px 40px 1px rgba(0, 0, 0, 0.1);
    margin-top: 20px;
    padding: 0;
    position: absolute;
    z-index: 10000;

    [data-reach-menu-item] {
        cursor: pointer;
        font-family: ${fontFamilies.bold};
        font-size: ${rem(18)};
        padding: ${rem(spacing.xs)} ${rem(spacing.m)};
    }

    [data-reach-menu-item][data-selected] {
        background-color: ${colors.primary};
    }
`;

export const TextOuterWrapper = styled.span`
    @media (max-width: ${rem(breakpoints.mobile)}) {
        display: block;
        overflow: hidden;
        text-overflow: ellipsis;
        width: calc(100% - 160px);
    }
`;

export const TextInnerWrapper = styled.span`
    @media (max-width: ${rem(breakpoints.mobile)}) {
        display: block;
        width: 100vw;
    }
`;

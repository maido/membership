/**
 * @prettier
 * @flow
 */
import React from 'react';
import {Menu, MenuButton, MenuList, MenuItem} from '@reach/menu-button';
import '@reach/menu-button/styles.css';
import * as S from './styles';

type Props = {
    activeItem: string,
    handleClick: Function,
    items: Array<{
        label: string,
        value: number | string
    }>
};

const DropdownMenu = ({activeItem, handleClick, items}: Props) => (
    <Menu>
        <MenuButton css={S.menuButton}>
            <S.TextOuterWrapper>
                <S.TextInnerWrapper>{activeItem}</S.TextInnerWrapper>
            </S.TextOuterWrapper>
            <svg
                width="16"
                height="10"
                viewBox="0 0 16 10"
                xmlns="http://www.w3.org/2000/svg"
                aria-hidden
            >
                <path
                    d="M14.85676 1.15459c-.19296-.20612-.50652-.20612-.69948 0L8.00677 7.73752 1.84419 1.15459c-.19296-.20612-.50652-.20612-.69947 0-.19296.20612-.19296.54106 0 .74718l6.50025 6.94364C7.74145 8.94847 7.86205 9 7.99471 9c.1206 0 .25325-.05153.34973-.15459l6.50026-6.94364c.20501-.20612.20501-.54106.01206-.74718z"
                    fill="#000"
                    fillRule="nonzero"
                    stroke="#000"
                    strokeWidth=".5"
                />
            </svg>
        </MenuButton>
        <MenuList css={S.menuList}>
            {items.map(item => (
                <MenuItem key={item.value} onSelect={() => handleClick(item.value)}>
                    {item.label}
                </MenuItem>
            ))}
        </MenuList>
    </Menu>
);

export default DropdownMenu;

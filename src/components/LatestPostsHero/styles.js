/**
 * @prettier
 * @flow
 */
import {css} from '@emotion/core';
import styled from '@emotion/styled';
import {rem} from 'polished';
import {
    breakpoints,
    colors,
    fontFamilies,
    fontSizes,
    spacing,
    transitions
} from '../../globals/variables';
import dotPattern from '../../images/dot-pattern.png';

export const Container = styled.header`
    margin-bottom: ${rem(spacing.m)};
    position: relative;
`;

export const imageContainer = css`
    max-height: 440px;
    overflow: hidden;
    position: relative;
    z-index: 1;
`;

export const image = css`
    background-position: center center;
    background-repeat: no-repeat;
    background-size: cover;
    height: auto;
    width: 100%;
`;

export const IntroText = styled.p`
    font-family: ${fontFamilies.heading};
    font-size: ${rem(23)};
    line-height: 1.5;
    margin-top: ${rem(spacing.m)};
`;

export const titleContainer = css`
    position: relative;
    z-index: 4;
`;

export const TitleImage = styled.img`
    bottom: ${rem(spacing.m * -1)};
    height: auto;
    left: 50%;
    margin-left: -25%;
    position: absolute;
    width: 50%;
    z-index: 4;

    @media (min-width: ${rem(breakpoints.mobile)}) {
        bottom: ${rem(spacing.l * -1)};
    }
`;

export const pattern = css`
    background-image: url('${dotPattern}');
    background-size: contain;
    background-position: center center;
    background-repeat: repeat;
    bottom: 0%;
    left: 50%;
    padding-bottom: 25%;
    margin-left: -30%;
    position: absolute;
    width: 60%;
    z-index: 3;
`;

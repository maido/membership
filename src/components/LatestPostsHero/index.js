/**
 * @prettier
 * @flow
 */
import React from 'react';
import Image from 'gatsby-image';
import {useSpring, animated} from 'react-spring/web.cjs';
import * as S from './styles';

type Props = {
    backgroundImage: {
        fluid: Object
    },
    text: string,
    title: string,
    titleImage: string
};

const LatestPostsHero = ({backgroundImage, text, title, titleImage}: Props) => {
    const imageProps = useSpring({
        config: {mass: 1, tension: 100, friction: 30},
        from: {opacity: 0},
        to: {opacity: 1}
    });
    const titleProps = useSpring({
        config: {mass: 1, tension: 150, friction: 30},
        delay: 800,
        from: {opacity: 0, transform: 'scale(1.1) translateY(30px) rotateX(30deg)'},
        to: {opacity: 1, transform: 'scale(1) translateY(0) rotateX(0)'}
    });
    const patternProps = useSpring({
        config: {mass: 1, tension: 150, friction: 30},
        delay: 1000,
        from: {opacity: 0, transform: 'scale(1.1) translateY(30px) rotateX(30deg)'},
        to: {opacity: 1, transform: 'scale(1) translateY(0) rotateX(0)'}
    });

    return (
        <S.Container>
            {backgroundImage && (
                <animated.div style={imageProps} css={S.imageContainer}>
                    <Image fluid={backgroundImage.fluid} css={S.image} />
                </animated.div>
            )}
            {1 == 2 && <animated.div style={patternProps} css={S.pattern} />}

            {titleImage && (
                <animated.div style={titleProps} css={S.titleContainer}>
                    <S.TitleImage src={titleImage ? `https:${titleImage}` : ''} alt={title} />
                </animated.div>
            )}

            {text && (
                <animated.div style={imageProps}>
                    <S.IntroText>{text}</S.IntroText>
                </animated.div>
            )}
        </S.Container>
    );
};

export default LatestPostsHero;

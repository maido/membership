/**
 * @prettier
 * @flow
 */
import styled from '@emotion/styled';
import {between, rem} from 'polished';
import {breakpoints, colors, fontFamilies, spacing, transitions} from '../../globals/variables';

export const Container = styled.form`
    align-items: center;
    border: 2px solid ${colors.greyLight};
    border-radius: ${rem(spacing.m)};
    display: flex;
    padding: ${rem(spacing.xs)} ${rem(spacing.s)};
    transition: ${transitions.default};

    ${props => props.isActive && `border-color: ${colors.red}`}
`;

export const SearchIcon = styled.svg`
    flex-shrink: 0;
    height: auto;
    margin-right: ${rem(spacing.s)};
    width: ${rem(18)};
`;

export const TextInput = styled.input`
    border: 0;
    font-family: ${fontFamilies.default};
    font-size: ${rem(16)};
    line-height: 1;
    letter-spacing: ${rem(0.25)};
    transition: ${transitions.default};
    width: 100%;

    &::placeholder {
        color: ${colors.grey};
    }

    &:focus {
        outline: none;
    }
`;

export const ArrowIcon = styled.svg`
    fill: ${colors.red};
    flex-shrink: 0;
    height: auto;
    transition: ${transitions.default};
    width: ${rem(18)};
`;

export const SubmitButton = styled.button`
    -webkit-appearance: none;
    margin-left: ${rem(spacing.s)};
    vertical-align: middle;

    &:hover,
    &:focus {
        ${ArrowIcon} {
            transform: translateX(1px);
        }
    }
`;

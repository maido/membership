/**
 * @prettier
 * @flow
 */
import React, {useState} from 'react';
import {navigate} from '@reach/router';
import * as UI from '../UI/styles';
import * as S from './styles';

const SearchInput = () => {
    const [isActive, setIsActive] = useState(false);
    const [value, setValue] = useState('');

    const handleSubmit = event => {
        event.preventDefault();

        if (value) {
            navigate(`/search?query=${encodeURIComponent(value)}`);
        }
    };

    return (
        <S.Container isActive={isActive} onSubmit={handleSubmit}>
            <S.SearchIcon viewBox="0 0 26 26" xmlns="http://www.w3.org/2000/svg">
                <path
                    d="M14.94629 1C9.40332 1 4.89355 5.50977 4.89355 11.05322c0 2.50226.9248 4.78852 2.44281 6.54987l-6.11663 6.11664c-.29297.29297-.29297.76757 0 1.06054C1.3662 24.92676 1.55762 25 1.75 25s.38379-.07324.53027-.21973l6.11658-6.11657c1.76123 1.518 4.04742 2.44275 6.54944 2.44275C20.49023 21.10645 25 16.59668 25 11.05322S20.49023 1 14.94629 1zm0 18.60645c-4.71582 0-8.55274-3.83692-8.55274-8.55323S10.23047 2.5 14.9463 2.5C19.66309 2.5 23.5 6.33691 23.5 11.05322s-3.83691 8.55323-8.55371 8.55323z"
                    fill="#1D1D1B"
                />
            </S.SearchIcon>
            <S.TextInput
                onFocus={() => setIsActive(true)}
                onBlur={() => setIsActive(false)}
                onChange={event => setValue(event.target.value)}
                value={value}
                placeholder="Search..."
            />
            {value && (
                <S.SubmitButton type="submit">
                    <UI.FadeIn>
                        <S.ArrowIcon xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32 32">
                            <path
                                clip-rule="evenodd"
                                d="M32 16.009c0-.267-.11-.522-.293-.714l-9.899-9.999c-.391-.395-1.024-.394-1.414 0-.391.394-.391 1.034 0 1.428l8.193 8.275H1c-.552 0-1 .452-1 1.01s.448 1.01 1 1.01h27.586l-8.192 8.275c-.391.394-.39 1.034 0 1.428.391.394 1.024.394 1.414 0l9.899-9.999c.187-.189.29-.449.293-.714z"
                                fill="#121313"
                                fillRule="evenodd"
                            />
                        </S.ArrowIcon>
                    </UI.FadeIn>{' '}
                </S.SubmitButton>
            )}
        </S.Container>
    );
};

export default SearchInput;

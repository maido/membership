/**
 * @prettier
 * @flow
 */
import styled from '@emotion/styled';
import {rem} from 'polished';
import {CARD_SIZES, Content, ImageContainer, Label, MemberTypeBadge, Tag} from '../ListCard/styles';
import {breakpoints, colors, spacing} from '../../globals/variables';

export const Container = styled.div`
    width: ${rem(CARD_SIZES.mobileLarge)};

    .c-container {
        background-color: transparent !important;
        border: none !important;
        padding: 0 !important;
    }

    .gatsby-image-wrapper {
        vertical-align: top;
    }

    .c-container,
    ${Content} {
        display: block;
    }

    ${ImageContainer} {
        margin-bottom: ${rem(spacing.m)};
        width: 100%;
    }

    ${Tag} {
        margin-left: 0;
        margin-top: ${rem(spacing.s)};
    }

    ${MemberTypeBadge} {
        background-color: ${colors.white};
        border: 1px solid rgba(0, 0, 0, 0.25);
        left: ${rem(spacing.xs)};
        margin-top: 0;
        position: absolute;
        top: ${rem(spacing.xs)};
    }

    ${Label} {
        color: ${colors.primary} !important;
    }

    footer {
        justify-content: flex-start;
    }

    @media (max-width: ${rem(breakpoints.mobileSmall)}) {
        .gatsby-image-wrapper,
        .gatsby-image-wrapper img {
            height: ${rem(CARD_SIZES.mobileMedium)} !important;
            width: 100% !important;
        }
    }

    @media (min-width: ${rem(breakpoints.mobileSmall)}) {
        width: ${rem(CARD_SIZES.desktopLarge)};

        & + & {
            margin-left: ${rem(spacing.m)};
        }
    }
`;

export const FeaturedLabel = styled.span`
    background-color: ${colors.red};
    border-radius: ${rem(2)};
    color: ${colors.white};
    font-size: ${rem(11)};
    left: ${rem(spacing.xs)};
    letter-spacing: ${rem(0.5)};
    line-height: 1;
    padding: ${rem(2)} ${rem(spacing.xs)} ${rem(4)};
    position: absolute;
    text-transform: uppercase;
    top: ${rem(spacing.xs)};
    z-index: 100;
`;

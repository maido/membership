/**
 * @prettier
 * @flow
 */
import React from 'react';
import ListCard from '../ListCard';
import * as UI from '../UI/styles';
import * as S from './styles';

type Props = {
    description?: string,
    featuredLabel?: string,
    image?: Object,
    isFeatured?: boolean,
    label?: string,
    link?: string,
    memberType?: string,
    subtitle?: string,
    tag?: string,
    tagType?: string,
    title?: string,
    to?: string
};

const CarouselListCard = ({
    description,
    featuredLabel,
    image,
    isFeatured,
    label,
    link,
    memberType,
    subtitle,
    tag,
    tagType,
    title,
    to
}: Props) => (
    <S.Container>
        {isFeatured && (
            <S.FeaturedLabel>{featuredLabel ? featuredLabel : 'Featured'}</S.FeaturedLabel>
        )}
        <ListCard
            {...{
                description,
                image: image ? image.fixed : null,
                imageHasPlaceholder: true,
                imageType: 'fixed',
                label,
                link,
                memberType,
                subtitle,
                tag,
                tagType,
                title,
                to
            }}
        />
    </S.Container>
);

export default CarouselListCard;

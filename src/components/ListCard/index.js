/**
 * @prettier
 * @flow
 */
import React, {type Node} from 'react';
import {Link} from 'gatsby';
import Image from 'gatsby-image';
import {urlLabel} from '../../globals/functions';
import * as UI from '../UI/styles';
import * as S from './styles';

type Props = {
    badge?: string,
    children?: Node,
    description?: string,
    handleClick?: Function,
    isFeatured?: boolean,
    image?: Object,
    imageHasPlaceholder?: boolean,
    imageLogo?: boolean,
    imageType?: string,
    label?: string,
    link?: string,
    memberType?: string,
    size?: 'small' | 'medium' | 'large',
    subtitle?: string,
    tag?: string,
    tagType?: string,
    themeKey?: string,
    title?: string,
    to?: string
};

const ListCard = ({
    badge,
    children,
    description,
    handleClick = () => {},
    isFeatured,
    image,
    imageHasPlaceholder = false,
    imageLogo = false,
    imageType = 'fixed',
    label,
    link = '',
    memberType,
    size = 'small',
    subtitle,
    tag,
    tagType,
    themeKey,
    title,
    to
}: Props) => (
    <Link to={to} css={S.container({isFeatured})} onClick={handleClick} className="c-container">
        {(image || imageHasPlaceholder) && (
            <S.ImageContainer type={imageType} size={isFeatured ? 'large' : size}>
                {image && imageType === 'fixed' && (
                    <Image fixed={image} css={[S.image, imageLogo ? S.logo : null]} />
                )}
                {image && imageType === 'fluid' && (
                    <Image
                        fluid={image}
                        css={[S.image, imageLogo ? S.logo : null, S.imageFluid({isFeatured})]}
                    />
                )}
                {image && imageType === 'direct' && (
                    <img
                        src={image}
                        css={S.image}
                        style={{height: 100, marginRight: 24, width: 100}}
                    />
                )}
                {!image && imageHasPlaceholder && <S.ImagePlaceholder />}
            </S.ImageContainer>
        )}
        <S.Content>
            <S.TextContainer>
                {title && <S.Title size={size}>{title}</S.Title>}

                {(subtitle || link) && (
                    <S.Subtitle size={size}>
                        {subtitle} {subtitle && link && <span>| {urlLabel(link)}</span>}
                    </S.Subtitle>
                )}
                {description && <S.Description>{description}</S.Description>}
                {children && <S.Description>{children}</S.Description>}
            </S.TextContainer>
            <UI.Flex as="footer" css={S.footer({isFeatured})}>
                {(badge || memberType) && (
                    <S.BadgeContainer>
                        {memberType && <S.MemberTypeBadge>{memberType}</S.MemberTypeBadge>}
                        {badge && <S.Badge>{badge}</S.Badge>}
                    </S.BadgeContainer>
                )}
                {tag && (
                    <S.Tag>
                        {tagType && tagType === 'date' && (
                            <S.TagIcon
                                width="16"
                                height="17"
                                viewBox="0 0 16 17"
                                xmlns="http://www.w3.org/2000/svg"
                            >
                                <g fill="#999" fillRule="evenodd">
                                    <path d="M13.67742 2.69875H1.85135c-.23516 0-.4258-.17839-.4258-.39844 0-.22005.19064-.39843.4258-.39844h11.82607c.23517 0 .4258.1784.4258.39844 0 .22005-.19063.39844-.4258.39844z" />
                                    <path d="M14.14865 16.86719H1.85135c-1.01905 0-1.84516-.77301-1.84516-1.72657V3.62844c0-.95356.82611-1.72656 1.84516-1.72656.23517 0 .42581.17838.42581.39843s-.19064.39844-.4258.39844c-.54873 0-.99355.41624-.99355.92969v11.51218c0 .51346.44482.9297.99354.9297h12.2973c.54872 0 .99354-.41624.99354-.9297V3.62844c0-.51345-.44482-.92969-.99354-.92969h-.47123c-.23517 0-.4258-.17839-.4258-.39844 0-.22005.19063-.39844.4258-.39844h.47123c1.01905 0 1.84516.77301 1.84516 1.72657v11.51218c0 .95356-.82611 1.72657-1.84516 1.72657z" />
                                    <path d="M15.568 7.12937H.432c-.23517 0-.4258-.17838-.4258-.39843S.19682 6.3325.432 6.3325h15.136c.23517 0 .4258.17839.4258.39844 0 .22005-.19063.39843-.4258.39843zM12.25806 4.02688c-.23516 0-.4258-.1784-.4258-.39844V.53125c0-.22005.19064-.39844.4258-.39844.23517 0 .42581.17839.42581.39844v3.09719c0 .22005-.19064.39844-.4258.39844zM3.74194 4.02688c-.23517 0-.42581-.1784-.42581-.39844V.53125c0-.22005.19064-.39844.4258-.39844.23517 0 .42581.17839.42581.39844v3.09719c0 .22005-.19064.39844-.4258.39844z" />
                                </g>
                            </S.TagIcon>
                        )}
                        <span dangerouslySetInnerHTML={{__html: tag}} />
                    </S.Tag>
                )}
                {themeKey && <UI.ThemeKey themeKey={themeKey} />}
            </UI.Flex>
        </S.Content>
    </Link>
);

export default ListCard;

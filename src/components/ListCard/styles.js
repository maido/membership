/**
 * @prettier
 * @flow
 */
import {css} from '@emotion/core';
import styled from '@emotion/styled';
import {rem} from 'polished';
import {
    Label as UILabel,
    Badge as UIBadge,
    MemberTypeBadge as UIMemberTypeBadge
} from '../UI/styles';
import {responsiveSpacing} from '../../globals/functions';
import {
    breakpoints,
    colors,
    fontFamilies,
    fontSizes,
    spacing,
    transitions
} from '../../globals/variables';

export const CARD_SIZES = {
    mobile: 130,
    mobileMedium: 180,
    mobileLarge: 260,
    tablet: 100,
    desktopLarge: 380
};

/**
 * Container is at the bottom of this file..
 */
const fixedImageContainer = (props: Object) => `
    align-self: flex-start;
    flex: 0 0 ${rem(CARD_SIZES.mobile)};
    width: ${rem(CARD_SIZES.mobile)};

    ${
        props.size === 'medium'
            ? `flex: 0 0 ${rem(CARD_SIZES.mobileMedium)}; width: ${rem(CARD_SIZES.mobileMedium)};`
            : ''
    };
    ${
        props.size === 'large'
            ? `flex: 0 0 ${rem(CARD_SIZES.mobileLarge)}; width: ${rem(CARD_SIZES.mobileLarge)};`
            : ''
    };

    @media (max-width: ${rem(breakpoints.mobileSmall)}) {
        flex: 0 0 ${rem(CARD_SIZES.tablet)};
        width: ${rem(CARD_SIZES.tablet)};
    }

    @media (max-width: ${rem(breakpoints.mobile)}) {
        flex: 0 0 35%;
        width: 35%;
    }

    @media (min-width: ${rem(breakpoints.desktopLarge)}) {
        ${
            props.size === 'large'
                ? `flex: 0 0 ${rem(CARD_SIZES.desktopLarge)}; width: ${rem(
                      CARD_SIZES.desktopLarge
                  )};`
                : ''
        };
    }
    `;

export const ImageContainer = styled.div`
    flex-shrink: 0;
    ${props => (props.type === 'fixed' ? `${fixedImageContainer(props)}` : '')};
`;

export const image = css`
    &,
    img {
        border: 1px solid ${colors.greyLight};
        max-width: 100%;
        vertical-align: top;
    }

    & {
        background-color: ${colors.greyLight};
        transition: ${transitions.default};
    }

    a:hover & {
        opacity: 0.8;
    }

    @media (max-width: ${rem(breakpoints.mobileSmall)}) {
        &,
        img {
            height: ${rem(CARD_SIZES.tablet)} !important;
            width: ${rem(CARD_SIZES.tablet)} !important;
        }
    }
`;

export const logo = css`
    &,
    img {
        border-color: transparent;
        max-height: 100px !important;
        object-fit: contain !important;
    }

    & {
        background-color: transparent;
    }
`;

export const imageFluid = props => css`
    &,
    img {
        height: auto !important;
        margin-right: ${rem(spacing.m)};
        width: ${rem(CARD_SIZES.mobile)} !important;
    }
`;

export const ImagePlaceholder = styled.div`
    background-color: ${colors.greyLight};
    height: 0;
    padding-bottom: 70%;
    width: 100%;
`;

export const Content = styled.div`
    @media (min-width: ${rem(breakpoints.tabletSmall)}) {
        align-items: center;
        display: flex;
        flex-direction: row;
        flex-grow: 1;
        white-space: break-spaces;
    }
`;

export const TextContainer = styled.div`
    flex-grow: 1;

    @media (max-width: ${rem(breakpoints.tabletSmall)}) {
        margin-bottom: ${rem(spacing.s)};
    }
`;

export const Label = styled(UILabel)`
    display: block;
    flex-grow: 1;
    margin-bottom: ${rem(spacing.s)};
`;

export const Title = styled.span`
    color: ${colors.black};
    display: block;
    flex-grow: 1;
    font-size: ${rem(fontSizes.h5)};
    line-height: 1.4;
    white-space: normal;

    ${props => props.size === 'medium' && `font-size: ${rem(fontSizes.h4)};`};
`;

export const BadgeContainer = styled.div`
    align-items: center;
    display: flex;
    flex-shrink: 0;
    margin-right: ${rem(spacing.s)};

    * + * {
        margin-left: ${rem(spacing.s)};
    }

    @media (min-width: ${rem(breakpoints.tabletSmall)}) {
        margin-left: ${rem(spacing.s)};
    }
`;

export const Badge = styled(UIBadge)``;

export const MemberTypeBadge = styled(UIMemberTypeBadge)``;

export const Subtitle = styled.span`
    color: ${colors.grey};
    display: block;
    font-family: ${fontFamilies.bold};
    font-size: ${rem(fontSizes.h6)};
    letter-spacing: ${rem(0.5)};
    margin-top: ${rem(spacing.s)};
    text-transform: uppercase;
`;

export const Description = styled.p`
    color: ${colors.greyDark};
    line-height: 1.4;
    margin-top: ${rem(spacing.s)};
`;

export const footer = props => css`
    @media (min-width: ${rem(breakpoints.tabletSmall)}) {
        min-width: ${rem(CARD_SIZES.mobile)};
        ${!props.isFeatured && `justify-content: flex-end;`}
    }
`;

export const Tag = styled.span`
    align-items: center;
    color: ${colors.grey};
    display: flex;
    flex-shrink: 0;
    font-size: ${rem(fontSizes.h6)};
    text-align: right;
    text-transform: uppercase;
    transition: ${transitions.default};
    word-wrap: nowrap;

    a:hover &,
    a:active & {
        color: ${colors.black};
    }

    @media (min-width: ${rem(breakpoints.mobile)}) {
        margin-left: ${rem(spacing.s)};
    }
`;

export const TagIcon = styled.svg`
    margin-right: ${rem(spacing.s)};

    path {
        fill: ${colors.grey};
        transition: ${transitions.default};
    }
    a:hover & path,
    a:focus & path {
        fill: ${colors.black};
    }
`;

export const Link = styled.a`
    color: ${colors.grey};

    &:hover,
    &:focus {
        color: ${colors.black};
    }
`;

export const featuredContainer = css`
    background-color: ${colors.black};
    border-color: ${colors.black};
    color: ${colors.white};

    &:hover,
    &:active {
        background-color: ${colors.primary};
        border-color: ${colors.primary};
    }

    &,
    & * {
        transition: ${transitions.default};
    }

    ${Content} {
        align-items: flex-start;
        flex-direction: column;
        justify-content: center;
        margin-top: ${rem(spacing.m)};
        padding-bottom: ${rem(spacing.s)};
    }

    ${Title} {
        color: ${colors.white};
        font-size: ${rem(fontSizes.h3)};
    }

    ${Badge} {
        vertical-align: middle;
    }

    ${Label} {
        color: ${colors.primary};
    }
    &:hover ${Label},
    &:focus ${Label} {
        color: ${colors.white};
    }

    ${Description} {
        color: ${colors.grey};
    }
    &:hover ${Description},
    &:focus ${Description} {
        color: ${colors.white};
    }

    .gatsby-image-wrapper,
    .gatsby-image-wrapper img {
        border-color: ${colors.black} !important;
    }

    ${BadgeContainer},
    ${Tag},
    &:hover ${Tag},
    &:active ${Tag} {
        color: ${colors.white};
        font-size: ${rem(16)};
        margin-left: 0;
        margin-top: ${rem(spacing.m)};

        path {
            fill: ${colors.white};
        }
    }

    @media (min-width: ${rem(breakpoints.tabletSmall)}) {
        ${BadgeContainer} {
            margin-right: ${rem(spacing.m)};
        }
    }

    @media (max-width: ${rem(breakpoints.mobileSmall)}) {
        flex-direction: column;

        ${ImageContainer},
        ${ImageContainer} * {
            flex: 0 0 100%;
            height: auto;
            margin-right: 0;
            width: 100%;
        }

        .gatsby-image-wrapper,
        .gatsby-image-wrapper img {
            height: 250px !important;
            overflow: auto !important;
            width: 100% !important;
        }
    }

    @media (min-width: ${rem(breakpoints.mobileSmall)}) {
         ${Content} {
            margin-top: 0;
            padding-bottom: 0;
            padding-left: ${responsiveSpacing(spacing.m)};
        }
     }
`;

export const container = props => css`
    align-items: center;
    background-color: ${colors.white};
    border: 1px solid ${colors.greyLight};
    border-left: none;
    border-top: none;
    display: flex;
    line-height: 1.2;
    padding: ${responsiveSpacing(spacing.s * 1.5)} ${responsiveSpacing(spacing.m)};
    width: 100%;

    &:hover,
    &:active {
        background-color: ${colors.greyLight};
    }

    &[href='/#'] {
        pointer-events: none;
    }

    ${props.isFeatured ? featuredContainer : ''}

    @media (min-width: ${rem(breakpoints.tabletSmall)}) {
        padding: ${responsiveSpacing(spacing.s)} ${responsiveSpacing(spacing.m)};
    }
`;

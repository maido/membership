/**
 * @prettier
 * @flow
 */
import styled from '@emotion/styled';
import {rem} from 'polished';
import {colors, fontFamilies, spacing, transitions} from '../../globals/variables';

export const IconList = styled.div`
    align-items: center;
    display: flex;
`;

export const IconLink = styled.a`
    font-family: ${fontFamilies.bold};
    display: inline-block;

    & + & {
        margin-left: ${rem(spacing.s)};
    }

    svg {
        height: ${rem(16)};
        transition: ${transitions.bezier};
        width: auto;
    }
    &:hover svg,
    &:focus svg {
        transform: scale(1.2);
    }

    path {
        fill: ${colors.black};
        transition: ${transitions.default};
    }
    &:hover path,
    &:focus path {
        fill: ${colors.primary};
    }

    ${props => props.noFill && `path { fill: none !important; }`}
    ${props => props.size && `svg{height: ${rem(spacing[props.size])}`}
`;

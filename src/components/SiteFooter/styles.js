/**
 * @prettier
 */
import styled from '@emotion/styled';
import {rem} from 'polished';
import {breakpoints, colors, spacing} from '../../globals/variables';

export const Footer = styled.footer`
    margin-top: ${rem(spacing.m)};

    a {
        color: ${colors.black};
        font-size: ${rem(14)};
    }

    @media (max-width: ${rem(breakpoints.tablet)}) {
        flex-direction: row;
        margin-top: ${rem(spacing.l)};

        &,
        & * {
            justify-content: center;
            width: auto;
        }
    }
`;

export const LogoContainer = styled.div`
    display: flex;
    margin-bottom: ${rem(spacing.m)};

    * {
        max-height: ${rem(70)};
    }

    * + * {
        margin-left: ${rem(spacing.m)};
    }
`;

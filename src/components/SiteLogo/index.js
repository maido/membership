/**
 * @prettier
 * @flow
 */
import React from 'react';
import * as UI from '../UI/styles';
import logo from '../../images/logo-4.png';
import logoWhite from '../../images/logo-4-white.png';
import logo2x from '../../images/logo-4@2x.png';
import logoWhite2x from '../../images/logo-4-white@2x.png';

const SiteLogo = () => (
    <>
        <UI.ShowAt breakpoint="mobile">
            <img
                src={logo}
                srcSet={`${logo2x} 2x`}
                alt="British Fashion Council - Membership"
                style={{willChange: 'transform'}}
            />
        </UI.ShowAt>
        <UI.HideAt breakpoint="mobile">
            <img
                src={logoWhite}
                srcSet={`${logoWhite2x} 2x`}
                alt="British Fashion Council - Membership"
                style={{width: 150, marginTop: 8, willChange: 'transform'}}
            />
        </UI.HideAt>
    </>
);

export default SiteLogo;

/**
 * @prettier
 * @flow
 */
import React from 'react';
import * as S from './styles';

type Props = {
    activeTab?: string,
    handleTabClick: Function,
    items: Array<string>
};

const Tabs = ({activeTab, handleTabClick, items = []}: Props) => (
    <S.Tabs>
        {items.map(item => (
            <S.Tab
                key={`tab-${item}`}
                isActive={item === activeTab}
                onClick={() => handleTabClick(item)}
            >
                {item}
            </S.Tab>
        ))}
    </S.Tabs>
);

export default Tabs;

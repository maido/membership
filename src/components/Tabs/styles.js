/**
 * @prettier
 * @flow
 */
import styled from '@emotion/styled';
import {rem} from 'polished';
import {
    breakpoints,
    colors,
    fontFamilies,
    fontSizes,
    spacing,
    transitions
} from '../../globals/variables';
import {responsiveSpacing} from '../../globals/functions';

export const Tabs = styled.nav`
    * + * {
        margin-left: ${rem(spacing.m * 1.5)};
    }
`;

export const Tab = styled.button`
    display: inline-block;
    color: ${colors.grey};
    font-family: ${fontFamilies.bold};
    font-size: ${rem(18)};
    line-height: 1;
    padding-bottom: ${responsiveSpacing(spacing.s)};
    padding-top: ${responsiveSpacing(spacing.s)};
    position: relative;
    transition: ${transitions.default};

    &::after {
        background-color: ${colors.primary};
        bottom: 0;
        display: block;
        content: '';
        height: ${rem(4)};
        left: 0;
        position: absolute;
        transition: ${transitions.bezier};
        transition-duration: 0.4s;
        width: 0;
    }

    &:hover,
    &:focus {
        color: ${colors.black};
    }

    @media (min-width: ${rem(breakpoints.tablet)}) {
        font-size: ${rem(20)};
    }

    ${props =>
        props.isActive &&
        `
        color: ${colors.black};

        &::after {
            width: 100%;
        }
        `};
`;

/**
 * @prettier
 * @flow
 */
import * as React from 'react';
import VisibilitySensor from 'react-visibility-sensor';
import {useSpring, animated} from 'react-spring/web.cjs';
import {spacing} from '../../globals/variables';

type Props = {
    children: React.Node,
    delay?: number,
    config?: Object,
    offset?: Object,
    watch?: boolean
};

const AnimateWhenVisible = ({
    children,
    delay = 0,
    config,
    offset = {top: -400},
    watch = true
}: Props) => {
    const defaultConfig = {
        config: {
            tension: 70,
            mass: 1,
            friction: 16
        },
        delay,
        from: {transform: `translateY(${spacing.l}px)`, opacity: 0},
        to: {transform: `translateY(0)`, opacity: 1}
    };
    const springConfig = {
        ...defaultConfig,
        ...config
    };

    if (springConfig.delay > 500) {
        springConfig.delay = 500;
    }

    const [styleProps, setStyleProps] = useSpring(() => ({
        config: springConfig.config,
        delay: springConfig.delay,
        from: springConfig.from
    }));

    const [hasBeenVisible, setHasBeenVisible] = React.useState(false);

    if (delay <= 0 || !watch) {
        setTimeout(() => setStyleProps(springConfig.to), delay);

        return <animated.div style={styleProps}>{children}</animated.div>;
    } else {
        const handleVisibilityChange = (isVisible: boolean) => {
            if (hasBeenVisible === false && isVisible === true) {
                setHasBeenVisible(true);
                setTimeout(() => setStyleProps(springConfig.to), springConfig.delay);
            }
        };

        handleVisibilityChange(true);

        return (
            <VisibilitySensor
                delayedCall={true}
                onChange={handleVisibilityChange}
                active={hasBeenVisible === false}
                partialVisibility={true}
                offset={offset}
            >
                <animated.div style={styleProps}>{children}</animated.div>
            </VisibilitySensor>
        );
    }
};

export default AnimateWhenVisible;

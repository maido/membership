/**
 * @prettier
 * @flow
 */
import styled from '@emotion/styled';
import map from 'lodash/map';
import {rem} from 'polished';
import {breakpoints} from '../../globals/variables';

const columnSizes = {
    '1': {
        mobile: 1
    },
    '2': {},
    '3': {
        tablet: 2
    }
};

export const Item = styled.div`
    ${props => `width: ${100 / props.defaultColumns}%;`}

    ${props =>
        map(
            columnSizes[props.columns],
            (size, breakpoint) =>
                `@media (min-width: ${rem(breakpoints[breakpoint])}) { width: ${100 / size}%; }`
        ).join('')}
`;

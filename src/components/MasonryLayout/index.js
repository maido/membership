/**
 * @prettier
 * @flow
 */
import * as React from 'react';
import Masonry from 'react-masonry-component';
import AnimateWhenVisible from '../AnimateWhenVisible';
import * as S from './styles';

type Props = {
    columns: number,
    defaultColumns?: number,
    items: Array<{
        title: string,
        component: React.Node
    }>
};

const MasonryLayout = ({columns = 2, defaultColumns = 1, items = []}: Props) => (
    <Masonry>
        {items.map((item, index) => (
            <S.Item columns={columns} defaultColumns={defaultColumns} key={item.title}>
                <AnimateWhenVisible delay={index * 100}>{item.component}</AnimateWhenVisible>
            </S.Item>
        ))}
    </Masonry>
);

export default MasonryLayout;

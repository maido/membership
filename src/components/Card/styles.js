/**
 * @prettier
 * @flow
 */
import {css} from '@emotion/core';
import styled from '@emotion/styled';
import {rem} from 'polished';
import {MemberTypeBadge as UIMemberTypeBadge} from '../UI/styles';
import {breakpoints, colors, fontSizes, spacing, transitions} from '../../globals/variables';
import {responsiveSpacing} from '../../globals/functions';

export const Title = styled.span`
    color: ${colors.black};
    display: block;
    font-size: ${rem(fontSizes.h5)};
    line-height: 1.3;

    @media (min-width: ${rem(breakpoints.mobile)}) {
        font-size: ${rem(fontSizes.h4)};
    }
`;

export const ImageContainer = styled.div`
    margin-bottom: ${responsiveSpacing(spacing.s)};
    transition: ${transitions.bezier};
    transition-duration: 0.4s;

    a:hover &,
    a:active & {
        transform: scale(1.0015);
    }
`;

export const image = css`
    max-width: 100%;
`;

export const Tag = styled.span`
    align-items: center;
    color: ${colors.grey};
    display: flex;
    font-size: ${rem(fontSizes.h6)};
    letter-spacing: ${rem(0.5)};
    margin-top: ${rem(spacing.s)};
    text-align: right;
    text-transform: uppercase;
    transition: ${transitions.default};
    word-wrap: nowrap;

    a:hover &,
    a:active & {
        color: ${colors.black};
    }
`;

export const TagIcon = styled.svg`
    margin-right: ${rem(spacing.s)};

    path {
        fill: ${colors.grey};
        transition: ${transitions.default};
    }
    a:hover & path,
    a:focus & path {
        fill: ${colors.black};
    }
`;

export const introduction = css`
    color: ${colors.greyDark};
    line-height: 1.5;
    margin-top: ${rem(spacing.s)};

    @media (max-width: ${rem(breakpoints.mobile)}) {
        display: none;
    }
`;

const inGridContainer = css`
    border-left: 0;
    border-top: 0;
`;

const featuredContainer = css`
    background-color: ${colors.black};
    border-color: ${colors.black};
    color: ${colors.white};

    &:hover,
    &:active {
        background-color: ${colors.primary};
        border-color: ${colors.primary};
    }

    &,
    & * {
        color: ${colors.greyLight};
        transition: ${transitions.default};
    }

    ${Title} {
        color: ${colors.white};
        font-size: ${rem(fontSizes.h4)};
    }

    @media (min-width: ${rem(breakpoints.mobile)}) {
        ${Title} {
            font-size: ${rem(fontSizes.h3)};
        }
    }
`;

export const container = props => css`
    border: 1px solid ${colors.greyLight};
    display: block;
    padding: ${responsiveSpacing(spacing.m)};
    position:relative;
    transition: ${transitions.bezier};

    &:hover,
    &:active {
        background-color: ${colors.greyLight};
    }

    ${props.inGrid ? inGridContainer : ''}
    ${props.isFeatured && props.isFeatured === true ? featuredContainer : ''}

    ${UIMemberTypeBadge} {
        background-color:${colors.white};
        border: 1px solid rgba(0,0,0,.25);
        color:${colors.black};
        left: calc(${responsiveSpacing(spacing.m)} + ${rem(spacing.xs)});
        position: absolute;
        top: calc(${responsiveSpacing(spacing.m)} + ${rem(spacing.xs)});
        z-index: 5;
    }

    @media (min-width: ${rem(breakpoints.desktopLarge)}) {
        padding: ${rem(spacing.l)};

        ${UIMemberTypeBadge} {
            left: ${rem(spacing.l + spacing.xs)};
            position: absolute;
            top: ${rem(spacing.l + spacing.xs)};
        }
    }
`;

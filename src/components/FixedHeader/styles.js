/**
 * @prettier
 * @flow
 */
import styled from '@emotion/styled';
import {rem} from 'polished';
import {breakpoints, colors, spacing} from '../../globals/variables';
import {responsiveSpacing} from '../../globals/functions';
import {TITLE_CONTAINER_WIDTH, NAV_CONTAINER_WIDTHS} from '../SiteHeader/styles';

export const FIXED_HEADER_HEIGHT = {
    default: 150,
    medium: 110,
    small: 69
};

export const Container = styled.div`
    background-color: ${colors.white};
    box-shadow: 0 3px 30px 0 rgba(0, 0, 0, 0.05);
    border-bottom: 1px solid ${colors.greyLight};
    display: flex;
    flex-direction: column;
    overflow: hidden;
    position: fixed;
    z-index: 2;

    ${NAV_CONTAINER_WIDTHS.map(
        ({breakpoint, width}) => `
        @media (min-width: ${rem(breakpoint)}) {
            width: calc(100% - ${rem(TITLE_CONTAINER_WIDTH)} - ${rem(width)});
        }`
    ).join('')};

    ${props => `height: ${rem(FIXED_HEADER_HEIGHT[props.size])};`}

    @media (max-width: ${rem(breakpoints.tablet)}) {
        width: 100%;

        ${props => `height: ${rem(FIXED_HEADER_HEIGHT[props.size] * 0.8)};`}
    }
`;

export const Placeholder = styled.div`
    ${props => `height: ${rem(FIXED_HEADER_HEIGHT[props.size])};`}

    @media (max-width: ${rem(breakpoints.tablet)}) {
        ${props => `height: ${rem(FIXED_HEADER_HEIGHT[props.size] * 0.8)};`}
    }
`;

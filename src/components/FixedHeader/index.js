/**
 * @prettier
 * @flow
 */
import * as React from 'react';
import * as S from './styles';

type Props = {
    children: React.Node,
    size?: string,
    styles?: Object
};

const FixedHeader = ({children, size = 'default', styles}: Props) => (
    <React.Fragment>
        <S.Container size={size} css={styles}>{children}</S.Container>
        <S.Placeholder size={size} css={styles} />
    </React.Fragment>
);

export default FixedHeader;

/**
 * @prettier
 * @flow
 */
import {css} from '@emotion/core';
import styled from '@emotion/styled';
import {rem} from 'polished';
import {responsiveSpacing} from '../../globals/functions';
import {breakpoints, spacing} from '../../globals/variables';

export const CONTAINER_WIDTH = 1150;

export const containerCSS = props => css`
    box-sizing: border-box;
    display: block;
    max-width: ${rem(CONTAINER_WIDTH)};
    padding: ${responsiveSpacing(spacing.m)};
    position: relative;
    width: 100%;

    ${props.padding === false && `padding: 0;`}
`;

export const Container = styled.div`
    ${containerCSS};
`;

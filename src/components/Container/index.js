/**
 * @prettier
 * @flow
 */
import * as React from 'react';
import * as S from './styles';

type Props = {
    as?: string,
    children: React.Node,
    padding?: boolean,
    style?: Object
};

const Container = ({as = 'div', children, padding = true, style = {}}: Props) => (
    <S.Container as={as} padding={padding} style={style}>
        {children}
    </S.Container>
);

export default Container;

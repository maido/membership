/**
 * @prettier
 * @flow
 */
import React, {Fragment} from 'react';
// import * as S from './styles';

type Props = {
    address: ContentfulAddress
};

const Address = ({address}: Props) => (
    <Fragment>
        {address.name && (
            <div>
                <strong>{address.name}</strong>
            </div>
        )}
        {address.street && <div>{address.street}</div>}
        {(address.city || address.country) && (
            <div>
                {address.city}
                {address.country && <span>, {address.country}</span>}
            </div>
        )}
        {address.postcode && <div>{address.postcode}</div>}
    </Fragment>
);

export default Address;

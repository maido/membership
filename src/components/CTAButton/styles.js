/**
 * @prettier
 * @flow
 */
import {css} from '@emotion/core';
import styled from '@emotion/styled';
import {rem, shade} from 'polished';
import {
    breakpoints,
    colors,
    fontFamilies,
    fontSizes,
    spacing,
    themes,
    transitions
} from '../../globals/variables';

export const button = css`
    border-radius: 0;
    border: 2px solid transparent;
    cursor: pointer;
    display: inline-block;
    font-family: ${fontFamilies.bold};
    font-size: ${rem(fontSizes.default)};
    line-height: 1.3;
    overflow: hidden;
    opacity: 1;
    padding: ${rem(15)} ${rem(30)};
    position: relative;
    text-align: center;
    transition: ${transitions.default};
    vertical-align: middle;

    @media (max-width: ${rem(breakpoints.mobile)}) {
        display: block;

        & + & {
            margin-top: ${rem(spacing.xs)};
        }
    }

    @media (min-width: ${rem(breakpoints.mobile)}) {
        & + & {
            margin-left: ${rem(spacing.xs)};
        }
    }
`;

export const buttonTheme = props => css`
    background-color: ${themes[props.theme].background};
    border-color: ${themes[props.theme].background};
    color: ${themes[props.theme].text};

    &:hover,
    &:focus {
        background-color: ${shade(0.05, themes[props.theme].background)};
        border-color: ${shade(0.05, themes[props.theme].background)};
        color: ${themes[props.theme].text};
    }

    ${props.border && `border: 2px solid;`}

    ${props.block && `width: 100%;`}

    ${props.ghost &&
        `&:not(:hover):not(:active) {
        background: transparent;
        border: 2px solid ${themes[props.theme].background};
        color: ${themes[props.theme].background};
    }`}
`;

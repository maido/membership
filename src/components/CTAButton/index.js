/**
 * @prettier
 * @flow
 */
import * as React from 'react';
import Link from '../Link';
import * as S from './styles';

type Props = {
    block?: boolean,
    border?: boolean,
    children: React.Node,
    ghost?: boolean,
    to?: string,
    theme?: string,
    type?: string
};

const CTAButton = ({
    block = false,
    border = false,
    children,
    to = '',
    type = 'button',
    ghost = false,
    theme = 'primary',
    ...props
}: Props) => {
    return (
        <Link
            to={to}
            css={[S.button, S.buttonTheme({block, border, ghost, theme})]}
            type={type}
            {...props}
        >
            {children}
        </Link>
    );
};

export default CTAButton;

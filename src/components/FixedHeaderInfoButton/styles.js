/**
 * @prettier
 * @flow
 */
import {css} from '@emotion/core';
import styled from '@emotion/styled';
import {rem} from 'polished';
import {Label as UILabel} from '../UI/styles';
import {breakpoints, colors, spacing, transitions} from '../../globals/variables';

export const Icon = styled.svg`
    color: ${colors.greyDark};
    height: ${rem(16)};
    margin-right: ${rem(spacing.s)};
    transitions: ${transitions.default};
    width: ${rem(16)};
`;

export const button = css`
    align-items: center;
    background-color: ${colors.greyLightest};
    border-color: ${colors.greyLightest};
    bottom: 0;
    display: flex;
    font-size: ${rem(12)};
    padding: ${rem(spacing.s * 0.75)} ${rem(spacing.s * 1.5)};
    position: absolute;
    right: 0;
    transitions: ${transitions.default};
    z-index: 10001;

    ${UILabel} {
        color: ${colors.greyDark};
        letter-spacing: ${rem(0.5)};
    }

    &:hover,
    &:focus {
        background-color: ${colors.greyLight};
        border-color: ${colors.greyLight};
    }

    @media (max-width: ${rem(breakpoints.mobile)}) {
        padding: ${rem(spacing.s * 0.75)} ${rem(spacing.s)};

        ${UILabel} {
            display: none;
        }

        ${Icon} {
            margin-right: 0;
        }
    }
`;

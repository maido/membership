/**
 * @prettier
 * @flow
 */
import {css} from '@emotion/core';
import styled from '@emotion/styled';
import {rem} from 'polished';
import {colors, spacing, transitions} from '../../globals/variables';

export const container = css`
    padding-bottom: ${rem(spacing.s)};
`;

export const layout = css`
    > div {
        margin-bottom: ${rem(spacing.m)};
    }
`;

export const TextWrapper = styled.div`
    display: flex;
    flex-direction: row;
    line-height: 1.5;
`;

export const key = css`
    flex: 0 0 ${rem(12)};
    margin-left: 0;
    margin-right: ${rem(spacing.s)};
    margin-top: ${rem(8)};
`;

/**
 * @prettier
 * @flow
 */
import * as React from 'react';
import * as UI from '../UI/styles';
import * as S from './styles';

type Props = {
    stages: Array<ContentfulBusinessStage>
};

const BusinessStages = ({description, title, stages = []}: Props) => (
    <UI.LayoutContainer css={S.layout}>
        {stages.map(stage => (
            <UI.LayoutItem key={stage.title} sizeAtMobileSmall={6 / 12}>
                <S.TextWrapper>
                    <UI.ThemeKey themeKey={stage.title} css={S.key} />

                    <span>
                        <UI.Label css={[UI.blackText]}>{stage.title}</UI.Label>
                        <br />
                        {stage.description}
                    </span>
                </S.TextWrapper>
            </UI.LayoutItem>
        ))}
    </UI.LayoutContainer>
);

export default BusinessStages;

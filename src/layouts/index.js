/**
 * @prettier
 * @flow
 */
import * as React from 'react';
import {StaticQuery, graphql} from 'gatsby';
import {Location} from '@reach/router';
import OverlayNavigation from '../components/OverlayNavigation';
import SiteFooter from '../components/SiteFooter';
import SiteHeader from '../components/SiteHeader';
import * as UI from '../components/UI/styles';
import * as S from './styles';

type Props = {
    children: React.Node
};

const Layout = ({children}: Props) => {
    const [isOverlayNavActive, setOverlayNavActive] = React.useState(false);

    const handleHamburgerClick = () => setOverlayNavActive(!isOverlayNavActive);

    const handleOverylayLinkClick = () => setOverlayNavActive(false);

    return (
        <StaticQuery
            query={graphql`
                query HeadingQuery {
                    site {
                        siteMetadata {
                            title
                            navigation {
                                main {
                                    to
                                    label
                                }
                            }
                        }
                    }
                }
            `}
            render={data => (
                <S.PageWrapper>
                    <Location>
                        {({location}) => (
                            <SiteHeader
                                isHamburgerActive={isOverlayNavActive}
                                handleHamburgerClick={handleHamburgerClick}
                                location={location}
                                links={data.site.siteMetadata.navigation.main}
                                title={data.site.siteMetadata.title}
                            />
                        )}
                    </Location>

                    <S.Main>{children}</S.Main>

                    <OverlayNavigation
                        isActive={isOverlayNavActive}
                        handleClick={handleOverylayLinkClick}
                        links={data.site.siteMetadata.navigation.main}
                    />

                    <UI.HideAt breakpoint="tablet">
                        <SiteFooter />
                        <UI.Spacer />
                    </UI.HideAt>
                </S.PageWrapper>
            )}
        />
    );
};

export default Layout;

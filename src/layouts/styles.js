/**
 * @prettier
 * @flow
 */
import styled from '@emotion/styled';
import {rem} from 'polished';
import {breakpoints} from '../globals/variables';
import {TITLE_CONTAINER_WIDTH, NAV_CONTAINER_WIDTHS} from '../components/SiteHeader/styles';

export const PageWrapper = styled.div`
    @media (min-width: ${rem(breakpoints.tablet)}) {
        display: flex;
        flex-direction: column;
        min-height: 100vh;
    }

    ${NAV_CONTAINER_WIDTHS.map(
        ({breakpoint, width}) => `
        @media (min-width: ${rem(breakpoint)}) {
            margin-left: ${rem(TITLE_CONTAINER_WIDTH + width)};
        }`
    ).join('')};
`;

export const Main = styled.main`
    flex-grow: 1;
    overflow: hidden;
`;

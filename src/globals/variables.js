/**
 * @prettier
 * @flow
 */
export const breakpoints = {
    mobileSmall: 540,
    mobile: 741,
    tabletSmall: 769,
    tablet: 1025,
    desktop: 1300,
    desktopLarge: 1800
};

export const colors = {
    white: '#fff',
    black: '#000',
    grey: '#9B9B9B',
    greyLight: '#E5E6E8',
    greyLightest: '#F8F8F8',
    greyDark: '#3D4451',
    red: '#FF534B',

    primary: '#FF534B',
    secondary: '#9B9B9B',
    tertiary: '#979797'
};

export const themes = {
    primary: {
        background: colors.red,
        text: colors.white
    },
    secondary: {
        background: colors.greyDark,
        text: colors.white
    },
    light: {
        accent: colors.greyLight,
        background: colors.white,
        text: colors.black
    },
    greyLightest: {
        accent: colors.greyLightest,
        background: colors.greyLightest,
        text: colors.black
    }
};

export const fontFamilies = {
    default: "'NeuzeitGro - Regular', Arial",
    bold: "'NeuzeitGro - Bold', Arial",
    italic: "'NeuzeitGro - Regular', Arial",
    heading: "'NeuzeitGro - Regular', Arial"
};

export const fontSizes = {
    default: 18,
    lead: 22,
    h1: 52,
    h2: 44,
    h3: 33,
    h4: 24,
    h5: 18,
    h6: 13
};

export const radius = 0;

export const spacing = {
    xs: 6,
    s: 12,
    m: 24,
    l: 48,
    xl: 72
};

export const transitions = {
    default: 'all .2s ease-in-out',
    slow: 'all .3s ease-out',
    bezier: 'all 2s cubic-bezier(.23,1,.32,1)'
};

export const themeKeys = {
    Entrepreneur: '#308DFF',
    Explorer: '#F90076',
    'Future Brand': '#00E5D3',
    'Start up': '#CDFF17'
};

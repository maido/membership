/**
 * @prettier
 * @/flow
 */
import find from 'lodash/find';
import fromPairs from 'lodash/fromPairs';
import get from 'lodash/get';
import last from 'lodash/last';
import map from 'lodash/map';
import truncate from 'lodash/truncate';
import dayjs from 'dayjs';

const getRootElementFontSize = () => {
    let size = 16;

    if (typeof window !== 'undefined' && document.documentElement) {
        size = parseFloat(getComputedStyle(document.documentElement).fontSize);
    }

    return size;
};

export const rem = (value: number) => `${(value / getRootElementFontSize()).toFixed(4)}rem`;

export const scrollToTop = () => {
    if (typeof window !== 'undefined') {
        window.scroll({
            top: 0,
            left: 0,
            behavior: 'smooth'
        });
    }
};

export const urlLabel = (url: string = '') => {
    url = url
        .replace('https://', '')
        .replace('http://', '')
        .replace('www.', '');

    if (url.charAt(url.length - 1) === '/') {
        url = url.substr(0, url.length - 1);
    }

    return url;
};

export const photoURL = (filename: string = '') =>
    filename.includes('http') ? filename : `/static/images/${filename}`;

export const getURLFromType = () => {};

export const getStraplineFromMarkdown = (content: string = '') => {
    let formattedContent;

    formattedContent = content.replace(new RegExp('__', 'g'), '');
    formattedContent = formattedContent.replace(new RegExp(/\*/, 'g'), '');
    formattedContent = formattedContent.split('\n');

    return formattedContent[0];
};

export const formatContentForMarkdown = (rawMarkdown: string = '') => {
    /**
     * We need to replace all missing protocols for Contentful links.
     * Some links might have it, others not. Is this the best way to do this?
     */
    let markdown = rawMarkdown.replace('(assets.ctfassets', '(https://assets.ctfassets');
    markdown = markdown.replace('(images.ctfassets', '(https://images.ctfassets');
    markdown = markdown.replace('(downloads.ctfassets', '(https://downloads.ctfassets');

    return markdown;
};

export const isLinkActive = (props: Object) => {
    if (props.isCurrent || props.isPartiallyCurrent) {
        if (props.href === '/' && !props.isCurrent) {
            return false;
        }

        return true;
    } else {
        return false;
    }
};

export const getLinkProps = (props: Object) => {
    return {
        'data-active': isLinkActive(props)
    };
};

export const responsiveSpacing = (unit: number = 0, calculation: string = '+') =>
    `calc(${rem(unit)} ${calculation} var(--responsive-spacing))`;

export const getKeyValues = (resources: Array<Object> = []) => {
    return fromPairs(
        map(resources, r => {
            const key = last(get(r, 'key', '').split('.'));
            const value = get(r, 'value.value');

            return [key, value];
        })
    );
};

export const getResources = (data: Object, title: string, property: string = 'resources') => {
    const resources = find(get(data, property), {title});

    if (resources) {
        return getKeyValues(get(resources, 'references'));
    }
};

export const bytesToSize = (bytes, seperator = '') => {
    const sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB'];

    if (bytes !== 0) {
        const i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)), 10);

        if (i === 0) {
            return `${bytes}${seperator}${sizes[i]}`;
        } else {
            return `${(bytes / 1024 ** i).toFixed(1)}${seperator}${sizes[i]}`;
        }
    }
};

export const getPersonsTagline = (data: Object) => {
    if (data.tagline) {
        return data.tagline;
    } else if (data.content && data.content.content) {
        return truncate(data.content.content, {
            length: 70,
            separator: ' '
        });
    } else {
        return '';
    }
};

export const hasDatePassed = (date: string) => {
    return dayjs().isAfter(dayjs(date));
};

export const getYouTubeIdFromUrl = (url: string = '') => {
    const regExp = /^.*((youtu.be\/)|(v\/)|(\/u\/\w\/)|(embed\/)|(watch\?))\??v?=?([^#\&\?]*).*/;
    const match = url.match(regExp);

    return match && match[7].length == 11 ? match[7] : null;
};

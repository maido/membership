/**
 * @prettier
 * @flow
 */
import {css} from '@emotion/core';
import {rem} from './functions';
import {breakpoints, colors, fontFamilies, spacing, transitions} from './variables';
import * as fonts from '../fonts';

export const styles = css`
    :root {
        --responsive-spacing: 0.1px;
        --reach-menu-button: 1;
    }

    @media (min-width: ${rem(breakpoints.mobile)}) {
        :root {
            --responsive-spacing: ${rem(spacing.xs)};
        }
    }

    @media (min-width: ${rem(breakpoints.desktop)}) {
        :root {
            --responsive-spacing: ${rem(spacing.s)};
        }
    }

    *,
    *::before,
    *::after {
        box-sizing: inherit;
    }

    html {
        background-color: ${colors.white};
        box-sizing: border-box;
        color: ${colors.black};
        font-family: ${fontFamilies.default};
        overflow-x: hidden;
        -moz-osx-font-smoothing: grayscale;
        -webkit-font-smoothing: antialiased;
        -webkit-text-size-adjust: 100%;
    }

    body {
        font-size: 1em;
        line-height: 1.6;
        margin: 0;
        max-width: 100%;
    }

    @media (max-width: ${rem(breakpoints.mobile)}) {
        body {
            font-size: 0.95em;
        }
    }

    button {
        background: none;
        border: 0;
        cursor: pointer;
        outline: none;
        padding: 0;
    }

    i,
    em,
    b,
    strong {
        font-family: ${fontFamilies.default};
    }

    img {
        max-width: 100%;
    }

    ::selection {
        background-color: ${colors.primary};
        color: ${colors.white};
    }

    a,
    button {
        color: inherit;
        text-decoration: none;
        transition: ${transitions.default};
    }

    a {
        color: ${colors.primary};
    }
    a:hover,
    a:focus {
        color: ${colors.greyDark};
    }

    p {
        margin: 0;
    }

    p + * {
        margin-top: ${rem(spacing.m)};
    }

    @media (prefers-reduced-motion: reduce) {
        * {
            animation: none !important;
        }
    }

    @font-face {
        font-family: 'NeuzeitGro - Regular';
        src: local('NeuzeitGro - Regular'),
            url('${fonts.neuzeitGroRegularWoff2}') format('woff2'),
            url('${fonts.neuzeitGroRegularWoff}') format('woff');
        font-weight: normal;
        font-style: normal;
    }
    @font-face {
        font-family: 'NeuzeitGro - Bold';
        src: local('NeuzeitGro - Bold'),
            url('${fonts.neuzeitGroBoldWoff2}') format('woff2'),
            url('${fonts.neuzeitGroBoldWoff}') format('woff');
        font-weight: normal;
        font-style: normal;
    }
`;

/**
 * @prettier
 */
import React from 'react';
import {Global} from '@emotion/core';
import {styles} from './globals/styles';

const HTML = ({
    htmlAttributes,
    headComponents,
    bodyAttributes,
    preBodyComponents,
    body,
    postBodyComponents
}) => (
    <html {...htmlAttributes}>
        <head>
            <meta charSet="utf-8" />
            <meta httpEquiv="x-ua-compatible" content="ie=edge" />
            <meta name="viewport" content="width=device-width, initial-scale=1" />
            <link href="https://images.ctfassets.net" rel="preconnect" />
            <script
                crossOrigin="anonymous"
                src="https://cdn.polyfill.io/v3/polyfill.min.js?flags=gated&features=default%2CIntersectionObserverEntry%2CArray.prototype.includes%2CString.prototype.includes%2Cfetch%2CIntersectionObserver%2CHTMLPictureElement"
            />
            {headComponents}
            <Global styles={styles} />
        </head>
        <body {...bodyAttributes}>
            {preBodyComponents}
            <div key={`body`} id="___gatsby" dangerouslySetInnerHTML={{__html: body}} />
            {postBodyComponents}
        </body>
    </html>
);

export default HTML;
